package com.gradulab.seguridad.services;

import com.gradulab.seguridad.entity.Usuario;

public interface IUsuarioService {

    public Usuario findByUsername(String username);
}
