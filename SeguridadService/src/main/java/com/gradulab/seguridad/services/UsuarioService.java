package com.gradulab.seguridad.services;

import com.gradulab.seguridad.entity.Usuario;
import com.gradulab.seguridad.repository.UsusarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UsuarioService implements IUsuarioService, UserDetailsService {

    @Autowired
    private UsusarioRepository ususarioRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        var usuario = ususarioRepository.findByLogin(username);

        if (usuario == null){
            throw new UsernameNotFoundException("Error en el login, no existe el usuario");
        }

        List<GrantedAuthority> authorities = usuario.getRoles()
                .stream()
                .map(role -> new SimpleGrantedAuthority(role.getNombre()))
                .collect(Collectors.toList());
        return new User(usuario.getLogin(),usuario.getContrasena(), true, true, true,true, authorities);
    }

    @Override
    public Usuario findByUsername(String username) {
        return ususarioRepository.findByLogin(username);
    }
}
