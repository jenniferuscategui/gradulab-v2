package com.gradulab.seguridad;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@EnableEurekaClient
@SpringBootApplication
public class SeguridadServiceApplication implements CommandLineRunner {

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;


	public static void main(String[] args) {
		SpringApplication.run(SeguridadServiceApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		String pasword = "1234567";

		for (int i = 0; i<4; i++){
			String pass = passwordEncoder.encode(pasword);
			System.out.println(pass);
		}
	}
}
