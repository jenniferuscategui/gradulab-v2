package com.gradulab.seguridad.dto;

import java.util.Date;

public class UsuarioDTO extends DTO{

    private Long id;

    private String login;

    private String contrasena;

    private String habilitado;

    private Date fechaIngreso;

    private PersonaDTO personaDto;

    private Date fechaIngresoReg;

    private Date fechaModificaReg;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getHabilitado() {
        return habilitado;
    }

    public void setHabilitado(String habilitado) {
        this.habilitado = habilitado;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public PersonaDTO getPersonaDto() {
        return personaDto;
    }

    public void setPersonaDto(PersonaDTO personaDto) {
        this.personaDto = personaDto;
    }

    public Date getFechaIngresoReg() {
        return fechaIngresoReg;
    }

    public void setFechaIngresoReg(Date fechaIngresoReg) {
        this.fechaIngresoReg = fechaIngresoReg;
    }

    public Date getFechaModificaReg() {
        return fechaModificaReg;
    }

    public void setFechaModificaReg(Date fechaModificaReg) {
        this.fechaModificaReg = fechaModificaReg;
    }
}
