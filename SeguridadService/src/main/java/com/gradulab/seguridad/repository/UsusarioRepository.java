package com.gradulab.seguridad.repository;


import com.gradulab.seguridad.entity.Persona;
import com.gradulab.seguridad.entity.Usuario;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsusarioRepository extends CrudRepository<Usuario,Long> {
    Usuario findByPersona(Persona persona);

    Usuario findByLogin(String login);
}
