# GRADULAB :
## Seguimiento a  graduados del programa de ingenieria de sistemas de la UFPS


## Requeiemintos

- Java 16
- Spring Boot 2
- Maven
- Mysql
- Docker
- Nodejs
- Postman
- Angular 11

## Configuracion del IDE default intelij

Desabilitar el importart dependencias con  *
[![auto import][imagen_01t]]

si no se detecta maven y no le importa la dependencias configurae el lanzador del servicio
[![imagen_02t]]

## Backend
### Orden de ejecucion de los servicios

### ConfigService
http://localhost:9800

para probar las configuracion de un servicio en postman envie una peticion GET

http://localhost:9800/AdministradorService/default

### EurekaService 

http://localhost:9854/

### GatewayService

### Microservicio Administrador
peticiones en Postman

GET
     http://localhost:9801/persona/list
POST

## Orden opcional
### Microservicio Graduado
GET

    

POST

### Microservicio Ofertas Laborales
GET

POST

### Microservicio Reportes
GET

POST
### Microservicio Encuestas

GET

POST




[imagen_01t]: Documentacion/imagenes/ide_01.jpg
[imagen_02t]: Documentacion/imagenes/configServicio.PNG
