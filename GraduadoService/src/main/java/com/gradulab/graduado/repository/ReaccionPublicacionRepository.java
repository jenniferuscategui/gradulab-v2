package com.gradulab.graduado.repository;

import com.gradulab.graduado.entity.ReaccionPublicacion;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public interface ReaccionPublicacionRepository extends CrudRepository<ReaccionPublicacion, Long> {
    Optional<ReaccionPublicacion> findByPublicacionAndReaccion();
}
