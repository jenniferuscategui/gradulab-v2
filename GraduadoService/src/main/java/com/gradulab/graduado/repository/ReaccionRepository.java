package com.gradulab.graduado.repository;

import com.gradulab.graduado.entity.Reaccion;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public interface ReaccionRepository extends CrudRepository<Reaccion, Long> {
}
