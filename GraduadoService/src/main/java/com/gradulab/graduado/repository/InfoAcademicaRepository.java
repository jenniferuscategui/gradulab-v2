package com.gradulab.graduado.repository;

import com.gradulab.graduado.entity.Graduado;
import com.gradulab.graduado.entity.InfoAcademica;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface InfoAcademicaRepository  extends CrudRepository<InfoAcademica, Long> {
    Optional<InfoAcademica> findByGraduado(Graduado graduado);
}
