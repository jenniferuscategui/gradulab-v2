package com.gradulab.graduado.repository;

import com.gradulab.graduado.entity.Graduado;
import com.gradulab.graduado.entity.InfoLaboral;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface InfoLaboralRepository  extends CrudRepository<InfoLaboral,Long> {

    Optional<InfoLaboral> findByGraduado(Graduado graduado);
}
