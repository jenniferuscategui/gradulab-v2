package com.gradulab.graduado.repository;

import com.gradulab.graduado.entity.Persona;
import com.gradulab.graduado.entity.Usuario;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsusarioRepository  extends CrudRepository<Usuario,Long> {
    Usuario findByPersona(Persona persona);
}
