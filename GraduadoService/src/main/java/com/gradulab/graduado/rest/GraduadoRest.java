
package com.gradulab.graduado.rest;

import com.gradulab.graduado.dto.*;
import com.gradulab.graduado.general.constants.ResponseCodes;
import com.gradulab.graduado.general.exception.GradulabExceptionHandler;
import com.gradulab.graduado.general.utils.ResponseObject;
import com.gradulab.graduado.servicios.GraduadoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


@RestController
@RequestMapping("/gradudado")
public class GraduadoRest {

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private GraduadoService graduadoService;

    private static final String EXCEPTION_MESSAGE = "Exception.message";

    private static final Logger logger = LoggerFactory.getLogger(GraduadoRest.class);

    @GetMapping(value = "/gra", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<ResgistroPersonaDTO> registrarGraduado (){
        try{
            ResgistroPersonaDTO resgistroPersonaDTO = new ResgistroPersonaDTO();
            PersonaDTO personaDTO = new PersonaDTO();
            personaDTO.setNombre("javier");
            personaDTO.setApellidos("numa");
            personaDTO.setCelular("4545454");
            resgistroPersonaDTO.setPersonaDto(personaDTO);
            return new ResponseObject<>(resgistroPersonaDTO);
        } catch (Exception e){
            return new ResponseObject<>( ResponseCodes.REQUEST_ERROR_CODE,
                    messageSource.getMessage(EXCEPTION_MESSAGE, null, LocaleContextHolder.getLocale()));
        }
    }

    @PostMapping(value = "/registrarGraduado", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<GraduadoDTO> registrarGraduado (@RequestBody ResgistroPersonaDTO resgistroPersonaDto){
        try{
            return new ResponseObject<>(graduadoService.registrarGraduado(resgistroPersonaDto));
        } catch (GradulabExceptionHandler gradulabExceptionHandler){
            logger.error(gradulabExceptionHandler.getMessage());
            return new ResponseObject<>( ResponseCodes.EXCEPTION_CODE,
                    messageSource.getMessage(gradulabExceptionHandler.getMessage(), null, LocaleContextHolder.getLocale()));
        }
        catch (Exception e){
            logger.error(e.getMessage());
            return new ResponseObject<>( ResponseCodes.REQUEST_ERROR_CODE,
                    messageSource.getMessage(EXCEPTION_MESSAGE, null, LocaleContextHolder.getLocale()));
        }
    }

    @PostMapping(value = "/infoLaboral", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<InfoLaboralDTO> ingresarInfoLaboral(@RequestBody InfoLaboralDTO infoLaboralDTO){
        try{
            return new ResponseObject<>(graduadoService.ingresarInfoLaborar(infoLaboralDTO));
        } catch ( Exception exception) {
            logger.error(exception.getMessage());
            return new ResponseObject<>( ResponseCodes.REQUEST_ERROR_CODE,
                    messageSource.getMessage(EXCEPTION_MESSAGE, null, LocaleContextHolder.getLocale()));
        }
    }

    @PostMapping(value = "/infoAcademica", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<InfoAcademicaDTO> ingresarInfoAcademica(@RequestBody InfoAcademicaDTO infoAcademicaDTO){
        try{
            return new ResponseObject<>(graduadoService.ingresarInfoAcademica(infoAcademicaDTO));
        } catch ( Exception exception) {
            logger.error(exception.getMessage());
            return new ResponseObject<>( ResponseCodes.REQUEST_ERROR_CODE,
                    messageSource.getMessage(EXCEPTION_MESSAGE, null, LocaleContextHolder.getLocale()));
        }
    }

    @PostMapping(value = "/crearPublicacion", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<PublicacionDTO> crearPublicacion(@RequestBody PublicacionDTO publicacionDTO, @RequestParam("file") MultipartFile file) {
        try{
            return new ResponseObject<>(graduadoService.crearPublicacion(publicacionDTO, file));
        } catch ( Exception exception) {
            logger.error(exception.getMessage());
            return new ResponseObject<>( ResponseCodes.REQUEST_ERROR_CODE,
                    messageSource.getMessage(EXCEPTION_MESSAGE, null, LocaleContextHolder.getLocale()));
        }
    }

    @PostMapping(value = "/comentarPublicacion", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<ComentarioDTO> comentarPublicacion(@RequestBody ComentarioDTO comentarioDTO) {
        try{
            return new ResponseObject<>(graduadoService.comentarPublicacion(comentarioDTO));
        } catch ( Exception exception) {
            logger.error(exception.getMessage());
            return new ResponseObject<>( ResponseCodes.REQUEST_ERROR_CODE,
                    messageSource.getMessage(EXCEPTION_MESSAGE, null, LocaleContextHolder.getLocale()));
        }
    }

}
