package com.gradulab.graduado.dto;

import java.util.Date;

public class GraduadoDTO extends DTO{

    private Long id;

    private Date fechaGrado;

    private String codigo;

    private String estado;

    private String modalidad;

    private String nombreTrabajo;

    private String folio;

    private String acta;

    private PersonaDTO personaDto;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Date getFechaGrado() {
        return fechaGrado;
    }

    public void setFechaGrado(Date fechaGrado) {
        this.fechaGrado = fechaGrado;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getModalidad() {
        return modalidad;
    }

    public void setModalidad(String modalidad) {
        this.modalidad = modalidad;
    }

    public String getNombreTrabajo() {
        return nombreTrabajo;
    }

    public void setNombreTrabajo(String nombreTrabajo) {
        this.nombreTrabajo = nombreTrabajo;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getActa() {
        return acta;
    }

    public void setActa(String acta) {
        this.acta = acta;
    }

    public PersonaDTO getPersonaDto() {
        return personaDto;
    }

    public void setPersonaDto(PersonaDTO personaDto) {
        this.personaDto = personaDto;
    }
}
