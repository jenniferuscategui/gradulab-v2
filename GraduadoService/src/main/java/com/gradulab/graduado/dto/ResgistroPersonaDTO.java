package com.gradulab.graduado.dto;

public class ResgistroPersonaDTO extends DTO{

    private PersonaDTO personaDto;
    private EmpresaDTO empresaDto;

    public PersonaDTO getPersonaDto() {
        return personaDto;
    }

    public void setPersonaDto(PersonaDTO personaDto) {
        this.personaDto = personaDto;
    }

    public EmpresaDTO getEmpresaDto() {
        return empresaDto;
    }

    public void setEmpresaDto(EmpresaDTO empresaDto) {
        this.empresaDto = empresaDto;
    }
}
