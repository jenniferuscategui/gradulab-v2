package com.gradulab.graduado.dto;

import com.gradulab.graduado.entity.Usuario;

public class PublicacionDTO extends DTO{

    private String contenido;

    private String multimedia;

    private Usuario usuario;

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public String getMultimedia() {
        return multimedia;
    }

    public void setMultimedia(String multimedia) {
        this.multimedia = multimedia;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
}
