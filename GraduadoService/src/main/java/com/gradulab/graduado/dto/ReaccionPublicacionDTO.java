package com.gradulab.graduado.dto;

import com.gradulab.graduado.entity.Publicacion;
import com.gradulab.graduado.entity.Reaccion;

public class ReaccionPublicacionDTO extends DTO{

    private Reaccion reaccion;

    private Publicacion publicacion;

    public Reaccion getReaccion() {
        return reaccion;
    }

    public void setReaccion(Reaccion reaccion) {
        this.reaccion = reaccion;
    }

    public Publicacion getPublicacion() {
        return publicacion;
    }

    public void setPublicacion(Publicacion publicacion) {
        this.publicacion = publicacion;
    }
}
