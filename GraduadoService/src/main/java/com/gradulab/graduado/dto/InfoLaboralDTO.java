package com.gradulab.graduado.dto;

import java.util.Date;

public class InfoLaboralDTO extends DTO{

    private Long id;

    private String empresa;

    private Date fechaInicio;

    private Date fechaFin ;

    private String cargo;

    private String area;

    private GraduadoDTO graduadoDto;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public GraduadoDTO getGraduadoDto() {
        return graduadoDto;
    }

    public void setGraduadoDto(GraduadoDTO graduadoDto) {
        this.graduadoDto = graduadoDto;
    }
}
