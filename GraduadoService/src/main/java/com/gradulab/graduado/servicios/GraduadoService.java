package com.gradulab.graduado.servicios;


import com.gradulab.graduado.dto.ComentarioDTO;
import com.gradulab.graduado.dto.GraduadoDTO;
import com.gradulab.graduado.dto.InfoAcademicaDTO;
import com.gradulab.graduado.dto.InfoLaboralDTO;
import com.gradulab.graduado.dto.PublicacionDTO;
import com.gradulab.graduado.dto.ReaccionPublicacionDTO;
import com.gradulab.graduado.dto.ResgistroPersonaDTO;
import com.gradulab.graduado.entity.Comentario;
import com.gradulab.graduado.entity.Graduado;
import com.gradulab.graduado.entity.InfoAcademica;
import com.gradulab.graduado.entity.InfoLaboral;
import com.gradulab.graduado.entity.Publicacion;
import com.gradulab.graduado.entity.ReaccionPublicacion;
import com.gradulab.graduado.general.enums.EnumEstadoGraduado;
import com.gradulab.graduado.general.exception.GradulabExceptionHandler;
import com.gradulab.graduado.repository.ComentarioRepository;
import com.gradulab.graduado.repository.GraduadoRepository;
import com.gradulab.graduado.repository.InfoAcademicaRepository;
import com.gradulab.graduado.repository.InfoLaboralRepository;
import com.gradulab.graduado.repository.PersonaRepository;
import com.gradulab.graduado.repository.PublicacionRepository;
import com.gradulab.graduado.repository.ReaccionPublicacionRepository;
import com.gradulab.graduado.repository.ReaccionRepository;
import com.gradulab.graduado.repository.UsusarioRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.Optional;

@Service
public class GraduadoService {

    @Autowired
    private PersonaRepository personaRepository;

    @Autowired
    private InfoLaboralRepository infoLaboralRepository;

    @Autowired
    private InfoAcademicaRepository infoAcademicaRepository;

    @Autowired
    private GraduadoRepository graduadoRepository;

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private PersonaService personaService;

    @Autowired
    private PublicacionRepository publicacionRepository;

    @Autowired
    private UsusarioRepository ususarioRepository;

    @Autowired
    private ComentarioRepository comentarioRepository;

    @Autowired
    private ReaccionPublicacionRepository reaccionPublicacionRepository;

    @Autowired
    private ReaccionRepository reaccionRepository;


    /**
     * guarda la informacion que se recibe del graduado
     *
     * @param resgistroPersonaDTO el objecto que tiene la informacion basica del graduado y el codigo
     * @return GraduadoDTO retorna el objeto graduado que se guardo.
     */
    public GraduadoDTO registrarGraduado(ResgistroPersonaDTO resgistroPersonaDTO) throws GradulabExceptionHandler {
        var graduado = new Graduado();
        if (usuarioService.existeUsuarioDePersona(resgistroPersonaDTO.getPersonaDto())) {
            throw new GradulabExceptionHandler("usuario ya existe");
        }
        graduado.setPersona(personaRepository.save(personaService.guardarPersona(resgistroPersonaDTO.getPersonaDto())));
        graduado.setCodigo(resgistroPersonaDTO.getPersonaDto().getCodigoGraduado());
        graduado.setEstado(EnumEstadoGraduado.GRADUADO.getNombre());
        graduado.setFechaIngresoReg(new Date());
        graduado = graduadoRepository.save(graduado);
        return graduado.getDto();
    }


    /**
     * Se registra la informacion laboral del graduado
     * si ya tiene informacion se actualiza.
     *
     * @param infoLaboralDTO el objeto con la informacion laboral.
     * @return InfoLaboralDTO la informacion que se actualizo
     */
    public InfoLaboralDTO ingresarInfoLaborar(InfoLaboralDTO infoLaboralDTO) {
        var graduado = graduadoRepository.findById(infoLaboralDTO.getGraduadoDto().getId());
        if (graduado.isPresent()) {
            var infoLaboralOptional = infoLaboralRepository.findByGraduado(graduado.get());
            if (infoLaboralOptional.isPresent()) {
                return guardarInfoLaboralGraduado(infoLaboralOptional.get(), infoLaboralDTO, graduado.get());
            } else {
                var infoLaboral = new InfoLaboral();
                return guardarInfoLaboralGraduado(infoLaboral, infoLaboralDTO, graduado.get());
            }
        }
        return null;
    }

    private InfoLaboralDTO guardarInfoLaboralGraduado(InfoLaboral infoLaboral, InfoLaboralDTO infoLaboralDTO, Graduado graduado) {
        BeanUtils.copyProperties(infoLaboralDTO, infoLaboral);
        infoLaboral.setGraduado(graduado);
        return infoLaboralRepository.save(infoLaboral).getDto();
    }

    /**
     * Se registra la informacion laboral del graduado
     * si ya tiene informacion se actualiza.
     *
     * @param infoAcademicaDTO el objeto con la informacion laboral.
     * @return InfoLaboralDTO la informacion que se actualizo
     */
    public InfoAcademicaDTO ingresarInfoAcademica(InfoAcademicaDTO infoAcademicaDTO) {
        var graduado = graduadoRepository.findById(infoAcademicaDTO.getGraduado().getId());
        if (graduado.isPresent()) {
            var infoAcademicaOptional = infoAcademicaRepository.findByGraduado(graduado.get());
            if (infoAcademicaOptional.isPresent()) {
                return guardarInfoAcademicaGraduado(infoAcademicaOptional.get(), infoAcademicaDTO, graduado.get());
            } else {
                var infoAcademica = new InfoAcademica();
                return guardarInfoAcademicaGraduado(infoAcademica, infoAcademicaDTO, graduado.get());
            }
        }
        return null;
    }

    private InfoAcademicaDTO guardarInfoAcademicaGraduado(InfoAcademica infoAcademica, InfoAcademicaDTO infoAcademicaDTO, Graduado graduado) {
        BeanUtils.copyProperties(infoAcademicaDTO, infoAcademica);
        infoAcademica.setGraduado(graduado);
        return infoAcademicaRepository.save(infoAcademica).getDto();
    }


    public PublicacionDTO crearPublicacion(PublicacionDTO publicacionDTO, MultipartFile file) {
        var usuario = ususarioRepository.findById(publicacionDTO.getUsuario().getId());

        if (usuario.isPresent() && graduadoRepository.findById(usuario.get().getPersona().getId()).isPresent()) {
            Publicacion publicacion = new Publicacion();
            BeanUtils.copyProperties(publicacionDTO, publicacion);
            publicacion.setFechaIngresoReg(new Date());
            publicacion.setFechaModificareg(new Date());
            publicacion.setFechaPu(new Date());
            return publicacionRepository.save(publicacion).getDto();
        }
        return null;
    }

    public ComentarioDTO comentarPublicacion(ComentarioDTO comentarioDTO) {
        var usuario = ususarioRepository.findById(comentarioDTO.getUsuario().getId());

        if (usuario.isPresent() && graduadoRepository.findById(usuario.get().getPersona().getId()).isPresent()) {
            var publicacion = publicacionRepository.findById(comentarioDTO.getPublicacion().getId());
            if (publicacion.isPresent()){
                Comentario comentario = new Comentario();
                BeanUtils.copyProperties(comentarioDTO, comentario);
                comentario.setFechaModificaReg(new Date());
                comentario.setFechaModificaReg(new Date());
                return comentarioRepository.save(comentario).getDto();
            }
        }
        return null;
    }

    public ComentarioDTO editarPublicacion(ComentarioDTO comentarioDTO) {
        var comentario = comentarioRepository.findById(comentarioDTO.getId());

        if (comentario.isPresent()) {
            comentario.get().setDescripcion(comentarioDTO.getDescripcion());
            comentario.get().setFechaModificaReg(new Date());
            return comentarioRepository.save(comentario.get()).getDto();
        }
        return null;
    }

    public ReaccionPublicacionDTO reaccionarPublicacion(ReaccionPublicacionDTO reaccionPublicacionDTO) {

        var publicacion = publicacionRepository.findById(reaccionPublicacionDTO.getPublicacion().getId());
        var reaccion = reaccionRepository.findById(reaccionPublicacionDTO.getReaccion().getId());
        //buscar el usuario
        var reaccionPublicacionObj = new ReaccionPublicacion();

        if (publicacion.isPresent() && reaccion.isPresent()) {

            Optional<ReaccionPublicacion> reaccionPublicacion = reaccionPublicacionRepository.findByPublicacionAndReaccion();

            if(reaccionPublicacion.isPresent()){
                reaccionPublicacionObj = reaccionPublicacion.get();
                reaccionPublicacionObj.setCantidad(reaccionPublicacionObj.getCantidad() + 1L);
            }else{
                reaccionPublicacionObj.setCantidad(1L);
            }
            reaccionPublicacionObj.setPublicacion(publicacion.get());
            reaccionPublicacionObj.setReaccion(reaccion.get());
            return reaccionPublicacionRepository.save(reaccionPublicacionObj).getDto();
        }
        return null;
    }
}
