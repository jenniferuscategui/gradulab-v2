package com.gradulab.graduado.servicios;

import com.gradulab.graduado.dto.PersonaDTO;
import com.gradulab.graduado.entity.Persona;
import com.gradulab.graduado.repository.GraduadoRepository;
import com.gradulab.graduado.repository.PersonaRepository;
import com.gradulab.graduado.repository.UsusarioRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsuarioService {

    @Autowired
    private PersonaRepository personaRepository;

    @Autowired
    private GraduadoRepository graduadoRepository;

    @Autowired
    private UsusarioRepository ususarioRepository;

    private static final Logger logger = LoggerFactory.getLogger(UsuarioService.class);


    public Boolean existeUsuarioDePersona(PersonaDTO personaDTO){
        logger.info("Se busca el usuario de la persona solicitada");
        var persona = new Persona();
        persona.setId(personaDTO.getId());
        return ususarioRepository.findByPersona(persona) != null;
    }


}
