package com.gradulab.graduado.entity;

import com.gradulab.graduado.dto.ComentarioDTO;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "GDL_Comentario")
public class Comentario extends Model<ComentarioDTO>{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "come_id")
    private Long id;

    @Column(name = "come_descripcion")
    private String descripcion;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "come_publ_id")
    private Publicacion publicacion;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "come_usua_id")
    private Usuario usuario;

    @Column(name = "come_fecha_ingreso_reg")
    private Date fechaIngresoReg;

    @Column(name = "come_fecha_modifica_reg")
    private Date fechaModificaReg;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Publicacion getPublicacion() {
        return publicacion;
    }

    public void setPublicacion(Publicacion publicacion) {
        this.publicacion = publicacion;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Date getFechaIngresoReg() {
        return fechaIngresoReg;
    }

    public void setFechaIngresoReg(Date fechaIngresoReg) {
        this.fechaIngresoReg = fechaIngresoReg;
    }

    public Date getFechaModificaReg() {
        return fechaModificaReg;
    }

    public void setFechaModificaReg(Date fechaModificaReg) {
        this.fechaModificaReg = fechaModificaReg;
    }

    @Override
    public ComentarioDTO getDto() {
        return super.getDto();
    }

    @Override
    public Class<ComentarioDTO> getDtoClass() {
        return null;
    }
}
