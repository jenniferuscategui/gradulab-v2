package com.gradulab.graduado.entity;

import com.gradulab.graduado.dto.InfoLaboralDTO;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "GDL_Info_Laboral")
public class InfoLaboral extends Model<InfoLaboralDTO>{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "inla_id")
    private Long id;

    @Column(name = "inla_empresa")
    private String empresa;

    @Column(name = "inla_fecha_inicio")
    private Date fechaInicio;

    @Column(name = "inla_fecha_final")
    private Date fechaFin ;

    @Column(name = "inla_cargo")
    private String cargo;

    @Column(name = "inla_area")
    private String area;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "inla_grad_id")
    private Graduado graduado;

    @Column(name = "inla_fecha_ingreso_reg")
    @Temporal(TemporalType.DATE)
    private Date fechaIngresoRegistro;

    @Column(name = "inla_fecha_modifica_reg")
    private Date fechaModificaRegistro;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public Graduado getGraduado() {
        return graduado;
    }

    public void setGraduado(Graduado graduado) {
        this.graduado = graduado;
    }

    public Date getFechaIngresoRegistro() {
        return fechaIngresoRegistro;
    }

    public void setFechaIngresoRegistro(Date fechaIngresoRegistro) {
        this.fechaIngresoRegistro = fechaIngresoRegistro;
    }

    public Date getFechaModificaRegistro() {
        return fechaModificaRegistro;
    }

    public void setFechaModificaRegistro(Date fechaModificaRegistro) {
        this.fechaModificaRegistro = fechaModificaRegistro;
    }

    @Override
    public InfoLaboralDTO getDto(){
        InfoLaboralDTO infoLaboralDto = super.getDto();
        if (graduado != null){
            infoLaboralDto.setGraduadoDto(this.graduado.getDto());
        }
        return super.getDto();
    }

    @Override
    public Class<InfoLaboralDTO> getDtoClass(){
        return InfoLaboralDTO.class;
    }
}
