package com.gradulab.graduado.entity;

import com.gradulab.graduado.dto.ReaccionPublicacionDTO;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "GDL_Reaccion_Publicacion")
public class ReaccionPublicacion extends Model<ReaccionPublicacionDTO> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "repu_id")
    private Long id;

    @Column(name = "repu_cantidad")
    private Long cantidad;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "repu_reac_id")
    private Reaccion reaccion;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "repu_publ_id")
    private Publicacion publicacion;

    @Column(name = "repu_fecha_ingreso_reg")
    private Date fechaIngresoReg;

    @Column(name = "repu_fecha_modifica_reg")
    private Date fechaModificaReg;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCantidad() {
        return cantidad;
    }

    public void setCantidad(Long cantidad) {
        this.cantidad = cantidad;
    }

    public Reaccion getReaccion() {
        return reaccion;
    }

    public void setReaccion(Reaccion reaccion) {
        this.reaccion = reaccion;
    }

    public Publicacion getPublicacion() {
        return publicacion;
    }

    public void setPublicacion(Publicacion publicacion) {
        this.publicacion = publicacion;
    }

    public Date getFechaIngresoReg() {
        return fechaIngresoReg;
    }

    public void setFechaIngresoReg(Date fechaIngresoReg) {
        this.fechaIngresoReg = fechaIngresoReg;
    }

    public Date getFechaModificaReg() {
        return fechaModificaReg;
    }

    public void setFechaModificaReg(Date fechaModificaReg) {
        this.fechaModificaReg = fechaModificaReg;
    }

    @Override
    public ReaccionPublicacionDTO getDto() {
        return super.getDto();
    }

    @Override
    public Class<ReaccionPublicacionDTO> getDtoClass() {
        return null;
    }
}
