package com.gradulab.graduado;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class GraduadoServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(GraduadoServiceApplication.class, args);
	}

}
