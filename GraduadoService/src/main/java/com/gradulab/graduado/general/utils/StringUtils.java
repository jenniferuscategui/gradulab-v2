package com.gradulab.graduado.general.utils;

public class StringUtils {

    /**
     * Indica si el valr esta núlo o vacio
     * @param value el valor que quiero comparar
     * @return un booleano falso si no esta vacio o nulo y vertadero si lo esta
     */
    public static boolean isEmpty(String value) { return value == null || "".equals(value); }
}
