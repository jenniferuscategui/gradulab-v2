package com.gradulab.graduado.general.utils;


import com.gradulab.graduado.general.constants.ResponseCodes;

/**
 * Clae de respuesta para los
 * servicios REST
 *
 * @param <T> Clase del objeto respuesta
 */
public class ResponseObject <T>{

    private String code;

    private String message;

    private T body;


    public ResponseObject(T body){
        this.body = body;
        this.code = ResponseCodes.REQUEST_SUCCESS_CODE;
        this.message = ResponseCodes.REQUEST_SUCCESS_MESSAGE;
    }

    public ResponseObject(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public ResponseObject(String code, String message, T body) {
        this.code = code;
        this.message = message;
        this.body = body;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getBody() {
        return body;
    }

    public void setBody(T body) {
        this.body = body;
    }
}
