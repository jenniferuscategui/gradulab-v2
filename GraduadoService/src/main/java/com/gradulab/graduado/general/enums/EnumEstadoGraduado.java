package com.gradulab.graduado.general.enums;

public enum EnumEstadoGraduado {
    NO_GRADUADO( 1, "No Graduado"),
    GRADUADO( 2, "Graduado");

    private int id;
    private String nombre;

    EnumEstadoGraduado(int id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public int getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }
}
