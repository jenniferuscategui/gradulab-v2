package com.gradulab.graduado.general.constants;

/***
 * Cotiene los codigos y mensajes de respuesta para los controladores REST
 */
public class ResponseCodes {

    public static final String REQUEST_SUCCESS_CODE = "000";
    public static final String REQUEST_SUCCESS_MESSAGE = "Operacion Exitosa";
    public static final String REQUEST_ERROR_CODE = "001";
    public static final String EXCEPTION_CODE = "002";


}
