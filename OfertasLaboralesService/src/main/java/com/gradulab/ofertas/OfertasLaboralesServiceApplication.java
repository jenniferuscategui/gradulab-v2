package com.gradulab.ofertas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class OfertasLaboralesServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(OfertasLaboralesServiceApplication.class, args);
	}

}
