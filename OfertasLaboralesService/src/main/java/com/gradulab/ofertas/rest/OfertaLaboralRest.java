package com.gradulab.ofertas.rest;

import com.gradulab.ofertas.dto.EmpresaDTO;
import com.gradulab.ofertas.dto.OfertaLaboralDTO;
import com.gradulab.ofertas.entity.OfertaLaboral;
import com.gradulab.ofertas.general.constants.ResponseCodes;
import com.gradulab.ofertas.general.exception.GradulabExceptionHandler;
import com.gradulab.ofertas.general.utils.ResponseObject;
import com.gradulab.ofertas.servicios.EmpresaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/oferta")
public class OfertaLaboralRest {

    @Autowired
    private EmpresaService empresaService;

    @Autowired
    private MessageSource messageSource;

    private static final String exceptionMessage = "Exception.message";

    @PostMapping(value = "/crearOfertaLaboral", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<OfertaLaboral> crearOfertaLaboral(@RequestBody OfertaLaboralDTO ofertaLaboralDTO) {

        try{
            var ofertaLaboral = empresaService.crearOfertaLaboral(ofertaLaboralDTO);
            return new ResponseObject<>(ofertaLaboral);
        }catch (GradulabExceptionHandler ge){
            return new ResponseObject<>(ResponseCodes.REQUEST_ERROR_CODE, messageSource.getMessage(
                    ge.getMessage(), null, LocaleContextHolder.getLocale()));
        }catch (Exception e){
            return new ResponseObject<>(ResponseCodes.REQUEST_ERROR_CODE, messageSource.getMessage(
                    exceptionMessage, null, LocaleContextHolder.getLocale()));
        }
    }

    @GetMapping(value = "/listarOfertasLaboralesEmpresa", produces = MediaType.APPLICATION_JSON_VALUE)
    public  ResponseObject<List<OfertaLaboral>> listarOfertasLaboralesEmpresa (@RequestBody EmpresaDTO empresaDTO){

        try{
            List<OfertaLaboral> ofertaLaboralList= empresaService.listarOfertasEmpresa(empresaDTO);
            return new ResponseObject<>(ofertaLaboralList);
        }catch (GradulabExceptionHandler ge){
            return new ResponseObject<>(ResponseCodes.EXCEPTION_CODE, messageSource.getMessage(
                    ge.getMessage(), null, LocaleContextHolder.getLocale()));
        }catch (Exception e){
            return new ResponseObject<>(ResponseCodes.REQUEST_ERROR_CODE, messageSource.getMessage(
                    exceptionMessage, null, LocaleContextHolder.getLocale()));
        }
    }
}
