package com.gradulab.ofertas.general.enums;

public enum EnumEstadoOfertaLaboral {
    ABIERTA( 1, "Abierta"),
    CERRADO( 2, "Cerrada");

    private int id;
    private String nombre;

    EnumEstadoOfertaLaboral(int id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public int getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }
}

