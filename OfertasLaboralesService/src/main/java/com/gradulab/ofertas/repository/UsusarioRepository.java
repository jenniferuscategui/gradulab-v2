package com.gradulab.ofertas.repository;

import com.gradulab.ofertas.entity.Persona;
import com.gradulab.ofertas.entity.Usuario;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsusarioRepository  extends CrudRepository<Usuario,Long> {
    Usuario findByPersona(Persona persona);
}
