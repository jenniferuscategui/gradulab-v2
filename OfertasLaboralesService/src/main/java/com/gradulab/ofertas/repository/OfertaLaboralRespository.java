package com.gradulab.ofertas.repository;

import com.gradulab.ofertas.entity.Empresa;
import com.gradulab.ofertas.entity.OfertaLaboral;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OfertaLaboralRespository extends CrudRepository<OfertaLaboral, Long> {
    List<OfertaLaboral> findAllByEmpresa(Empresa empresaOptional);
}
