package com.gradulab.ofertas.repository;

import com.gradulab.ofertas.entity.Empresa;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmpresaRespository extends PagingAndSortingRepository<Empresa, Long>, JpaSpecificationExecutor<Empresa> {
}
