package com.gradulab.ofertas.repository;

import com.gradulab.ofertas.entity.ContactoEmpresa;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ConactoEmpresaRepository extends PagingAndSortingRepository<ContactoEmpresa, Long>, JpaSpecificationExecutor<ContactoEmpresa>, CrudRepository<ContactoEmpresa, Long> {

    List<ContactoEmpresa> findAll();
}
