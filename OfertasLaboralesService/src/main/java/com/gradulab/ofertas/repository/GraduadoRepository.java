package com.gradulab.ofertas.repository;

import com.gradulab.ofertas.entity.Graduado;
import com.gradulab.ofertas.entity.Persona;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface GraduadoRepository extends CrudRepository<Graduado, Long> {

    @Query("SELECT g FROM Graduado g JOIN g.persona p where g.persona not in ( select u.persona from Usuario u where u.persona = p)")
    List<Graduado> findAll();

    Optional<Graduado> findByPersona(Persona persona);
}
