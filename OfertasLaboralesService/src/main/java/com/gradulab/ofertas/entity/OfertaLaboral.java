package com.gradulab.ofertas.entity;

import com.gradulab.ofertas.dto.OfertaLaboralDTO;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "GDL_Oferta_Laboral")
public class OfertaLaboral extends Model<OfertaLaboralDTO>{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ofla_id")
    private Long id;

    @Column(name = "ofla_cargo")
    private String estado;

    @Column(name = "ofla_descripcion")
    private String descripcion;

    @Column(name = "ofla_experiencia")
    private String experiencia;

    @Column(name = "ofla_salario")
    private String salario;

    @Column(name = "ofla_tipo_contrato")
    private String tipoContrato;

    @Column(name = "ofla_conocimiento")
    private String conocimiento;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ofla_empr_id")
    private Empresa empresa;

    @Column(name = "ofla_fecha_ingreso_reg")
    @Temporal(TemporalType.DATE)
    private Date fechaIngresoRegistro;

    @Column(name = "ofla_fecha_modifica_reg")
    private Date fechaModificaRegistro;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getExperiencia() {
        return experiencia;
    }

    public void setExperiencia(String experiencia) {
        this.experiencia = experiencia;
    }

    public String getSalario() {
        return salario;
    }

    public void setSalario(String salario) {
        this.salario = salario;
    }

    public String getTipoContrato() {
        return tipoContrato;
    }

    public void setTipoContrato(String tipoContrato) {
        this.tipoContrato = tipoContrato;
    }

    public String getConocimiento() {
        return conocimiento;
    }

    public void setConocimiento(String conocimiento) {
        this.conocimiento = conocimiento;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Date getFechaIngresoRegistro() {
        return fechaIngresoRegistro;
    }

    public void setFechaIngresoRegistro(Date fechaIngresoRegistro) {
        this.fechaIngresoRegistro = fechaIngresoRegistro;
    }

    public Date getFechaModificaRegistro() {
        return fechaModificaRegistro;
    }

    public void setFechaModificaRegistro(Date fechaModificaRegistro) {
        this.fechaModificaRegistro = fechaModificaRegistro;
    }


    @Override
    public OfertaLaboralDTO getDto(){
        OfertaLaboralDTO ofertaLaboralDto = super.getDto();
        if (empresa !=null){
            ofertaLaboralDto.setEmpresaDto(this.empresa.getDto());
        }
        return ofertaLaboralDto;
    }

    @Override
    public Class<OfertaLaboralDTO> getDtoClass(){
        return OfertaLaboralDTO.class;
    }
}
