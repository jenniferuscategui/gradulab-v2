package com.gradulab.ofertas.entity;

import com.gradulab.ofertas.dto.ContactoEmpresaDTO;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "GDL_Contacto")
public class ContactoEmpresa extends Model<ContactoEmpresaDTO>{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cont_id")
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "cont_empr_id")
    private Empresa empresa;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "cont_pers_id")
    private Persona persona;

    @Column(name = "cont_fecha_ingreso_reg")
    @Temporal(TemporalType.DATE)
    private Date fechaIngresoRegistro;

    @Column(name = "cont_fecha_modifica_reg")
    private Date fechaModificaRegistro;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public Date getFechaIngresoRegistro() {
        return fechaIngresoRegistro;
    }

    public void setFechaIngresoRegistro(Date fechaIngresoRegistro) {
        this.fechaIngresoRegistro = fechaIngresoRegistro;
    }

    public Date getFechaModificaRegistro() {
        return fechaModificaRegistro;
    }

    public void setFechaModificaRegistro(Date fechaModificaRegistro) {
        this.fechaModificaRegistro = fechaModificaRegistro;
    }

    @Override
    public ContactoEmpresaDTO getDto(){
        ContactoEmpresaDTO contactoEmpresaDto = super.getDto();
        if (empresa !=null){
            contactoEmpresaDto.setEmpresaDto(this.empresa.getDto());
        }
        if (persona !=null){
            contactoEmpresaDto.setPersonaDto(this.persona.getDto());
        }
        return contactoEmpresaDto;
    }

    @Override
    public Class<ContactoEmpresaDTO> getDtoClass(){
        return ContactoEmpresaDTO.class;
    }
}
