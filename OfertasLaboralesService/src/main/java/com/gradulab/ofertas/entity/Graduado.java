package com.gradulab.ofertas.entity;

import com.gradulab.ofertas.dto.GraduadoDTO;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "GDL_Graduado")
public class Graduado extends Model<GraduadoDTO>{


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "grad_id")
    private Long id;

    @Column(name = "grad_fecha_grado")
    private Date fechaGrado;

    @Column(name = "grad_codigo")
    private String codigo;

    @Column(name = "grad_estado")
    private String estado;

    @Column(name = "grad_modalidad")
    private String modalidad;

    @Column(name = "grad_nombre_trabajo")
    private String nombreTrabajo;

    @Column(name = "grad_folio")
    private String folio;

    @Column(name = "grad_acta")
    private String acta;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "grad_pers_id")
    private Persona persona;

    @Temporal(TemporalType.DATE)
    @Column(name = "grad_fecha_ingreso_reg")
    private Date fechaIngresoReg;

    @Column(name = "grad_fecha_modifica_reg")
    private Date fechaModificaReg;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFechaGrado() {
        return fechaGrado;
    }

    public void setFechaGrado(Date fechaGrado) {
        this.fechaGrado = fechaGrado;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getModalidad() {
        return modalidad;
    }

    public void setModalidad(String modalidad) {
        this.modalidad = modalidad;
    }

    public String getNombreTrabajo() {
        return nombreTrabajo;
    }

    public void setNombreTrabajo(String nombreTrabajo) {
        this.nombreTrabajo = nombreTrabajo;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getActa() {
        return acta;
    }

    public void setActa(String acta) {
        this.acta = acta;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public Date getFechaIngresoReg() {
        return fechaIngresoReg;
    }

    public void setFechaIngresoReg(Date fechaIngresoReg) {
        this.fechaIngresoReg = fechaIngresoReg;
    }

    public Date getFechaModificaReg() {
        return fechaModificaReg;
    }

    public void setFechaModificaReg(Date fechaModificaReg) {
        this.fechaModificaReg = fechaModificaReg;
    }

    @Override
    public  GraduadoDTO getDto(){
        GraduadoDTO graduadoDto = super.getDto();
        if (persona !=null){
            graduadoDto.setPersonaDto(this.persona.getDto());
        }
        return graduadoDto;
    }

    @Override
    public Class<GraduadoDTO> getDtoClass(){
        return GraduadoDTO.class;
    }
}

