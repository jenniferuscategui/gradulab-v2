package com.gradulab.ofertas.servicios;

import com.gradulab.ofertas.dto.PersonaDTO;
import com.gradulab.ofertas.entity.Persona;
import com.gradulab.ofertas.repository.GraduadoRepository;
import com.gradulab.ofertas.repository.PersonaRepository;
import com.gradulab.ofertas.repository.UsusarioRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsuarioService {

    @Autowired
    private PersonaRepository personaRepository;

    @Autowired
    private GraduadoRepository graduadoRepository;

    @Autowired
    private UsusarioRepository ususarioRepository;

    private static final Logger logger = LoggerFactory.getLogger(UsuarioService.class);


    public Boolean existeUsuarioDePersona(PersonaDTO personaDTO){
        logger.info("Se busca el usuario de la persona solicitada");
        var persona = new Persona();
        persona.setId(personaDTO.getId());
        return ususarioRepository.findByPersona(persona) != null;
    }


}
