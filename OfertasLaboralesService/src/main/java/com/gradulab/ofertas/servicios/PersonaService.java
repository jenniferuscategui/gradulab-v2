package com.gradulab.ofertas.servicios;


import com.gradulab.ofertas.dto.PersonaDTO;
import com.gradulab.ofertas.entity.Persona;
import com.gradulab.ofertas.general.utils.DTOList;
import com.gradulab.ofertas.repository.PersonaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class PersonaService {

    @Autowired
    private PersonaRepository personaRepository;

    @Autowired
    private DTOList dtoList;

    private static final Logger logger = LoggerFactory.getLogger(PersonaService.class);

    private static final String SORT_BY_NAME = "nombre";
    private static final String SORT_BY_DOCUMENT = "documento";


    /**
     * lista de personas paginadas filtradas por nombre y documento
     * ordenada decendentemente por documento y como segundo criterio
     * de orden el nombre.
     * @param nombre nombre de la persona que se quiere buscar.
     * @param documento docuemnto de la persona que se quiere buscar.
     * @return Page<PersonaDTO> lista paginada con 10 elementos por pagina.
     */
    public Page<PersonaDTO> buscarPersonas(String nombre, String documento) {
           logger.info("Se obtiene el listado de personas");
           var params = PageRequest.of(0, Integer.parseInt("10"), Sort.by(SORT_BY_DOCUMENT).descending().and(Sort.by(SORT_BY_NAME)));
           Specification<Persona> specification = PersonaSpecification.obtenerPersonaSpecification(nombre, documento);
           Page<Persona> personaPage = personaRepository.findAll(specification, params);
           if (personaPage.getNumberOfElements() > 0) {
               return new PageImpl<>(dtoList.getDtoList(personaPage.getContent()), params, personaPage.getTotalElements());
           }
        return new PageImpl<>(new ArrayList<>());
    }

    /**
     * Guarda los datos de la persona
     * @param personaDTO información de la persona que se recibe
     * @return Person el objeto persona que se guarda.
     */
    public Persona guardarPersona(PersonaDTO personaDTO) {
        var persona = new Persona();
        BeanUtils.copyProperties(personaDTO, persona);
        return personaRepository.save(persona);

    }

    /**
     * busca la persona por documento
     * @param documento documento de identidad de la persona
     * @return
     */
    public PersonaDTO buscarPorDocuemnto( String documento){
        var personaDto = personaRepository.findByDocumento(documento);
        return personaDto.map(Persona::getDto).orElse(null);
    }

}
