package com.gradulab.ofertas.servicios;

import com.gradulab.ofertas.dto.ContactoEmpresaDTO;
import com.gradulab.ofertas.dto.EmpresaDTO;
import com.gradulab.ofertas.dto.OfertaLaboralDTO;
import com.gradulab.ofertas.dto.ResgistroPersonaDTO;
import com.gradulab.ofertas.entity.ContactoEmpresa;
import com.gradulab.ofertas.entity.Empresa;
import com.gradulab.ofertas.entity.OfertaLaboral;
import com.gradulab.ofertas.general.enums.EnumEstadoOfertaLaboral;
import com.gradulab.ofertas.general.exception.GradulabExceptionHandler;
import com.gradulab.ofertas.general.utils.DateUtils;
import com.gradulab.ofertas.repository.ConactoEmpresaRepository;
import com.gradulab.ofertas.repository.EmpresaRespository;
import com.gradulab.ofertas.repository.OfertaLaboralRespository;
import com.gradulab.ofertas.repository.PersonaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmpresaService {

    @Autowired
    private PersonaRepository personaRepository;

    @Autowired
    private EmpresaRespository empresaRespository;

    @Autowired
    private ConactoEmpresaRepository conactoEmpresaRepository;

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private OfertaLaboralRespository ofertaLaboralRespository;

    @Autowired
    private DateUtils dateUtils;

    @Autowired
    private PersonaService personaService;


    private static final Logger logger = LoggerFactory.getLogger(EmpresaService.class);

    /**
     * guarda la informacion del contacto empresa y de la empresa
     * @param resgistroPersonaDTO el objecto que tiene la informacion basica de la
     *                            empresa y el contacto empresa.
     * @return ContatcoEmpresaDTO retorna el objeto graduado que se guardo.
     */
    public ContactoEmpresaDTO registrarEmpresa(ResgistroPersonaDTO resgistroPersonaDTO ) throws GradulabExceptionHandler {
        logger.info("Se regiostra  el contacto empresa ");
        var contactoEmpresa = new ContactoEmpresa();
        Boolean validarPersona = usuarioService.existeUsuarioDePersona(resgistroPersonaDTO.getPersonaDto());
        if (Boolean.TRUE.equals(validarPersona)){
            throw new GradulabExceptionHandler("usuario ya existe");
        }
        contactoEmpresa.setPersona(personaService.guardarPersona(resgistroPersonaDTO.getPersonaDto()));
        contactoEmpresa.setEmpresa(guardarEmpresa(resgistroPersonaDTO.getEmpresaDto()));
        return conactoEmpresaRepository.save(contactoEmpresa).getDto();
    }

    /**
     * Se guarda la empresa.
     * @param empresaDTO informacion de la empresa.
     * @return objeto empresa que se guardo.
     */
    private Empresa guardarEmpresa(EmpresaDTO empresaDTO) {
        logger.info("Se guarda el empresa");
        var empresa = new Empresa();
        BeanUtils.copyProperties(empresaDTO, empresa);
        return empresaRespository.save(empresa);
    }

    /**
     * Se crea oferta labora
     * @param ofertaLaboralDTO la informacionde la oferta laboral
     * @return la informacion de la oferta laboral.
     * @throws GradulabExceptionHandler se maneja la excepcion en caso de que no exista la empresa.
     */
    public OfertaLaboral crearOfertaLaboral(OfertaLaboralDTO ofertaLaboralDTO) throws GradulabExceptionHandler {

        Optional<Empresa> empresaOptional = empresaRespository.findById(ofertaLaboralDTO.getEmpresaDto().getId());

        if (empresaOptional.isPresent()) {
            var ofertaLaboral = new OfertaLaboral();
            ofertaLaboral.setEmpresa(empresaOptional.get());
            ofertaLaboral.setConocimiento(ofertaLaboralDTO.getConocimiento());
            ofertaLaboral.setDescripcion(ofertaLaboralDTO.getDescripcion());
            ofertaLaboral.setExperiencia(ofertaLaboralDTO.getExperiencia());
            ofertaLaboral.setFechaIngresoRegistro(dateUtils.obtenerFechaActual());
            ofertaLaboral.setSalario(ofertaLaboralDTO.getSalario());
            ofertaLaboral.setTipoContrato(ofertaLaboralDTO.getTipoContrato());
            ofertaLaboral.setFechaModificaRegistro(dateUtils.obtenerFechaActual());
            ofertaLaboral.setEstado(EnumEstadoOfertaLaboral.ABIERTA.getNombre());
            ofertaLaboralRespository.save(ofertaLaboral);
            return ofertaLaboral;
        }else{
            throw new GradulabExceptionHandler("La empresa no existe");
        }
    }

    /**
     * se lista las ofertas laborares de la empresa
     * @param empresa
     * @return el listado de la oferta laboral.
     * @throws GradulabExceptionHandler excepcion en caso de que no exita la oferta laboral.
     */
    public List<OfertaLaboral> listarOfertasEmpresa(EmpresaDTO empresa) throws GradulabExceptionHandler {

        Optional<Empresa> empresaOptional = empresaRespository.findById(empresa.getId());

        try {
            if (empresaOptional.isPresent()) {
                return ofertaLaboralRespository.findAllByEmpresa(empresaOptional.get());
            } else {
                throw new GradulabExceptionHandler("La empresa no existe");
            }
        }catch (Exception e){
            logger.error(e.getMessage());
        }

        return null;
    }
}
