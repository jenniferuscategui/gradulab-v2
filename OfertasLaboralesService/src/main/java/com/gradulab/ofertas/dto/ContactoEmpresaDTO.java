package com.gradulab.ofertas.dto;

public class ContactoEmpresaDTO extends DTO {

    private Long id;

    private EmpresaDTO empresaDto;

    private PersonaDTO personaDto;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public EmpresaDTO getEmpresaDto() {
        return empresaDto;
    }

    public void setEmpresaDto(EmpresaDTO empresaDto) {
        this.empresaDto = empresaDto;
    }

    public PersonaDTO getPersonaDto() {
        return personaDto;
    }

    public void setPersonaDto(PersonaDTO personaDto) {
        this.personaDto = personaDto;
    }
}
