import {Persona} from './persona';
import {Empresa} from './empresa';

export  class RegistroUsuario{
  id: number;
  contrasena: string;
  personaDto: Persona = new Persona();
  empresaDto: Empresa = new Empresa();
}
