import {Persona} from './persona';

export class Graduado{
  id:number;
  fechagrado:string;
  codigo:string;
  estado:string;
  modalidad:string;
  nombreTrabajo:string;
  folio:string;
  acta:string;
  persona: Persona = new Persona();


}
