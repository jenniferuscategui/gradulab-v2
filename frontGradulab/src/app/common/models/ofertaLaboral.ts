export class OfertaLaboral{
  id:number;
  estado:string;
  descripcion:string;
  experiencia:string;
  salario:string;
  tipoContrato:string;
  conocimiento:string;
}
