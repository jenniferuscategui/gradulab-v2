export  class Empresa{
  id: number;
  nombre: string;
  nit: string;
  area: string;
  direccion: string;
  telefono: string;
  sector: string;
  email: string;
}
