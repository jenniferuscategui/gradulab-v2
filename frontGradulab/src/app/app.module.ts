import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { RouterModule } from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {InicioComponent} from './pages/inicio.component';
import {IniciosComponent} from './auth/inicios.component';
import {SidebaradmComponent} from './shared/sidebaradm/sidebaradm.component';
import {NoticiasComponent} from './pages/administrador/noticias/noticias.component';
import { AplicarofertaComponent } from './pages/ofertalaboral/aplicaroferta/aplicaroferta.component';
import {DashboardComponent} from './pages/dashboard/dashboard.component';
import {FooterComponent} from './shared/footer/footer.component';
import {HeaderlogeadoComponent} from './shared/headerlogeado/headerlogeado.component';
import {HeaderComponent} from './shared/header/header.component';
import { EditargraduadoComponent } from './pages/administrador/editargraduado/editargraduado.component';
import { PublicacionesComponent } from './pages/graduado/publicaciones/publicaciones.component';
import { RecuperarclaveComponent } from './auth/login/recuperar/recuperarclave.component';
import { SidebargraduadoComponent } from './shared/sidebargraduado/sidebargraduado.component';
import { GraduadoComponent } from './pages/graduado/graduado.component';
import { EditarperfilgradComponent } from './pages/graduado/editarperfilgrad/editarperfilgrad.component';
import { ListaofertasComponent } from './pages/ofertalaboral/listaofertas/listaofertas.component';
import { OferlabempresaComponent } from './pages/empresa/oferlabempresa/oferlabempresa.component';
import { EditarinfoadmComponent } from './pages/administrador/editarinfoadm/editarinfoadm.component';
import { EditarperfilempComponent } from './pages/empresa/editarperfilemp/editarperfilemp.component';
import { ReportesComponent } from './pages/administrador/reportes/reportes.component';
import { RecuperarusuarioComponent } from './auth/login/recuperar/recuperarusuario.component';
import { VerestadopostComponent } from './pages/graduado/verestadopost/verestadopost.component';
import {RegistroempresaComponent} from './auth/registroempresa/registroempresa.component';
import {RegistroempresaComponent2} from './pages/ofertalaboral/registroempresa/registroempresa.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { BackupsadminComponent } from './pages/administrador/backupsadmin/backups.component';
import { IniciopublicoComponent } from './pages/informacion/iniciopublico.component';
import { SidebarempComponent } from './shared/sidebaremp/sidebaremp.component';
import { ListarNoticiasComponent } from './pages/listar-noticias/listar-noticias.component';
import { ListarProxEventosComponent } from './pages/listar-prox-eventos/listar-prox-eventos.component';
import { DetalleNoticiasComponent } from './pages/detalle-noticias/detalle-noticias.component';
import { NotificacionesComponent } from './pages/graduado/notificaciones/notificaciones.component';
import {ScrollingModule} from '@angular/cdk/scrolling';
import {JwtInterceptor} from './servicios/jwt.interceptor';
import { MenuempresaComponent } from './pages/empresa/menuempresa/menuempresa.component';
import { EncuestasComponent } from './pages/administrador/encuestas/encuestas/encuestas.component';
import { PreguntasComponent } from './pages/administrador/encuestas/preguntas/preguntas.component';
import { OpcionesComponent } from './pages/administrador/encuestas/opciones/opciones.component';
import { VisualizarencuestaComponent } from './pages/administrador/encuestas/visualizarencuesta/visualizarencuesta.component';
import { ListarrespuestasComponent } from './pages/administrador/encuestas/listarrespuestas/listarrespuestas.component';
import { EncuestaComponent } from './pages/empresa/encuesta/encuesta.component';
import { EncuestasgraComponent } from './pages/graduado/encuestasgra/encuestasgra.component';

import { NgxSpinnerModule } from 'ngx-spinner';
import { SpinnerComponent } from './common/spinner/spinner.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {JwtErrorInterceptor} from './servicios/jwtError.interceptor';
import {LoginComponent} from './auth/login/login.component';
import {RegistroGraduadoComponent} from './auth/registrograduado/registrograduado.component';
import { GraduadoMenuComponent } from './pages/administrador/graudados/graduado-menu/graduado-menu.component';
import { EmpresasMenuComponent } from './pages/administrador/empresas/empresas-menu/empresas-menu.component';
import { NotificacionGraduadoComponent } from './pages/administrador/graudados/graduado-menu/notificacion-graduado/notificacion-graduado.component';
import { CargarInformacionComponent } from './pages/administrador/graudados/graduado-menu/cargar-informacion/cargar-informacion.component';
import { SolicitudRegistroComponent } from './pages/administrador/graudados/graduado-menu/solicitud-registro/solicitud-registro.component';
import { SolicitarPublicarComponent } from './pages/administrador/empresas/empresas-menu/solicitar-publicar/solicitar-publicar.component';
import { OfertaLaboralMenuComponent } from './pages/administrador/oferta-laboral/oferta-laboral-menu/oferta-laboral-menu.component';
import { CrearOfertaComponent } from './pages/administrador/oferta-laboral/oferta-laboral-menu/crear-oferta/crear-oferta.component';
import { ListarOfertaComponent } from './pages/administrador/oferta-laboral/oferta-laboral-menu/listar-oferta/listar-oferta.component';
import { PublicarNoticiasComponent } from './pages/administrador/noticias/publicar-noticias/publicar-noticias.component';
import { CommonModule } from '@angular/common';
import { GraudadosRoutingModule } from './pages/administrador/graudados/graudados-routing.module';

@NgModule({
  declarations: [
    AppComponent,
    NoticiasComponent,
    InicioComponent,
    IniciosComponent,
    HeaderComponent,
    HeaderlogeadoComponent,
    SidebaradmComponent,
    FooterComponent,
    LoginComponent,
    AplicarofertaComponent,
    DashboardComponent,
    PublicacionesComponent,
    EditargraduadoComponent,
    RecuperarclaveComponent,
    SidebargraduadoComponent,
    GraduadoComponent,
    EditarperfilgradComponent,
    ListaofertasComponent,
    OferlabempresaComponent,
    EditarinfoadmComponent,
    EditarperfilempComponent,
    ReportesComponent,
    RecuperarusuarioComponent,
    VerestadopostComponent,
    RegistroempresaComponent,
    RegistroempresaComponent2,
    BackupsadminComponent,
    IniciopublicoComponent,
    SidebarempComponent,
    ListarNoticiasComponent,
    ListarProxEventosComponent,
    DetalleNoticiasComponent,
    NotificacionesComponent,
    RegistroGraduadoComponent,
    MenuempresaComponent,
    EncuestasComponent,
    PreguntasComponent,
    OpcionesComponent,
    VisualizarencuestaComponent,
    ListarrespuestasComponent,
    EncuestaComponent,
    EncuestasgraComponent,
    SpinnerComponent,
    GraduadoMenuComponent,
    EmpresasMenuComponent,
    NotificacionGraduadoComponent,
    CargarInformacionComponent,
    SolicitudRegistroComponent,
    SolicitarPublicarComponent,
    OfertaLaboralMenuComponent,
    CrearOfertaComponent,
    ListarOfertaComponent,
    PublicarNoticiasComponent,
    ListarNoticiasComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    CommonModule,
    FormsModule,
    HttpClientModule,
    ScrollingModule,
    NgxSpinnerModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    GraudadosRoutingModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: JwtErrorInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
