import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GraudadosRoutingModule } from './graudados-routing.module';



@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    GraudadosRoutingModule
  ]
})
export class GraudadosModule { }
