import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GraduadoMenuComponent } from './graduado-menu.component';

describe('GraduadoMenuComponent', () => {
  let component: GraduadoMenuComponent;
  let fixture: ComponentFixture<GraduadoMenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GraduadoMenuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GraduadoMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
