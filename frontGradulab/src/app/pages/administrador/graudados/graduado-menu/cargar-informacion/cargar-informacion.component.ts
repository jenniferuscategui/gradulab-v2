import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-cargar-informacion',
  templateUrl: './cargar-informacion.component.html',
})
export class CargarInformacionComponent implements OnInit {

  formEnviar: FormGroup;

  constructor() { }

  ngOnInit(): void {

    this.formEnviar = new FormGroup({
      archivo: new FormControl({ value: null, disabled: false }),
    })
  }

  put() {
    let obj = this.formEnviar.value;
    console.log(obj);
  }
}
