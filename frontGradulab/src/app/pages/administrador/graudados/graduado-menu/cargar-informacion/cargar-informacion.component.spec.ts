import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CargarInformacionComponent } from './cargar-informacion.component';

describe('CargarInformacionComponent', () => {
  let component: CargarInformacionComponent;
  let fixture: ComponentFixture<CargarInformacionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CargarInformacionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CargarInformacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
