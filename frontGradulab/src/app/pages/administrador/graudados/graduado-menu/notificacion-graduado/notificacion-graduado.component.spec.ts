import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificacionGraduadoComponent } from './notificacion-graduado.component';

describe('NotificacionGraduadoComponent', () => {
  let component: NotificacionGraduadoComponent;
  let fixture: ComponentFixture<NotificacionGraduadoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NotificacionGraduadoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificacionGraduadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
