import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-notificacion-graduado',
  templateUrl: './notificacion-graduado.component.html',
})
export class NotificacionGraduadoComponent implements OnInit {

  formEnviar: FormGroup;

  constructor() { }

  ngOnInit(): void {
    this.formEnviar = new FormGroup({
      asunto: new FormControl({ value: null, disabled: false }),
      mensaje: new FormControl({ value: null, disabled: false }),
    })
  }

  put() {
    let obj =  this.formEnviar.value;
    console.log(obj);
  }

  reset() {
     this.formEnviar.reset();
  }

}
