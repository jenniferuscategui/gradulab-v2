import { Component, OnDestroy, OnInit } from '@angular/core';
import { Usuario } from 'src/app/common/models/usuario';
import { GraduadoService } from 'src/app/servicios/graduado.service';

@Component({
  selector: 'app-solicitud-registro',
  templateUrl: './solicitud-registro.component.html',
})
export class SolicitudRegistroComponent implements OnInit {


  lista: Usuario[];
  constructor(private graduadoSer: GraduadoService) { }

  ngOnInit(): void {
    this.lista = [];
    this.Listar();
    console.log('aca');

  }

  Listar() {
    this.graduadoSer.listarSolicitudes().subscribe(
      response => {
        this.lista = response.body;
      },
      error => {
      }
    );
  }

}
