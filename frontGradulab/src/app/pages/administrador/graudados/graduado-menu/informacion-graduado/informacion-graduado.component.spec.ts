import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InformacionGraduadoComponent } from './informacion-graduado.component';

describe('InformacionGraduadoComponent', () => {
  let component: InformacionGraduadoComponent;
  let fixture: ComponentFixture<InformacionGraduadoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InformacionGraduadoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InformacionGraduadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
