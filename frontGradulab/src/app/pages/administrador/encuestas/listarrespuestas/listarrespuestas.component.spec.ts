import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarrespuestasComponent } from './listarrespuestas.component';

describe('ListarrespuestasComponent', () => {
  let component: ListarrespuestasComponent;
  let fixture: ComponentFixture<ListarrespuestasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListarrespuestasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarrespuestasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
