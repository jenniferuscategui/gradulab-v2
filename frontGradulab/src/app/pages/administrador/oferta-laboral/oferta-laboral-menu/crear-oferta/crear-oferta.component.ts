import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-crear-oferta',
  templateUrl: './crear-oferta.component.html',
})
export class CrearOfertaComponent implements OnInit {

  formEnviar: FormGroup;

  constructor() { }

  ngOnInit(): void {
    this.formEnviar = new FormGroup({
      nombre: new FormControl({ value: null, disabled: false }),
      empresa: new FormControl({ value: null, disabled: false }),
      descripcion: new FormControl({ value: null, disabled: false }),
      requisitos: new FormControl({ value: null, disabled: false }),
      ubicacion: new FormControl({ value: null, disabled: false })
    })
  }

  postOfertas() {
    let obj = this.formEnviar.value;
    console.log(obj);


  }

}
