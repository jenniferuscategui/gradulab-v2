import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OfertaLaboralMenuComponent } from './oferta-laboral-menu.component';

describe('OfertaLaboralMenuComponent', () => {
  let component: OfertaLaboralMenuComponent;
  let fixture: ComponentFixture<OfertaLaboralMenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OfertaLaboralMenuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OfertaLaboralMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
