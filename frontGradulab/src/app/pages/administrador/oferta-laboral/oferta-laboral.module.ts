import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OfertaLaboralRoutingModule } from './oferta-laboral-routing.module';
import { OfertaLaboralMenuComponent } from './oferta-laboral-menu/oferta-laboral-menu.component';
import { CrearOfertaComponent } from './oferta-laboral-menu/crear-oferta/crear-oferta.component';
import { ListarOfertaComponent } from './oferta-laboral-menu/listar-oferta/listar-oferta.component';


@NgModule({
  declarations: [
    OfertaLaboralMenuComponent,
    CrearOfertaComponent,
    ListarOfertaComponent
  ],
  imports: [
    CommonModule,
    OfertaLaboralRoutingModule
  ]
})
export class OfertaLaboralModule { }
