import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { EditarinfoadminService } from 'src/app/servicios/editarinfoadmin.service';
import {RegistroUsuario} from "../../../common/models/registroUsuario";

@Component({
  selector: 'app-editarinfoadm',
  templateUrl: './editarinfoadm.component.html',
})
export class EditarinfoadmComponent implements OnInit {

  formEnviar: FormGroup;
  formCambiar: FormGroup;
  usuarioRes: RegistroUsuario;

  idUsuario: any;

  constructor(
    private editarinfoadminService: EditarinfoadminService
  ) { }

  ngOnInit(): void {
    this.usuarioRes = new RegistroUsuario();

    this.formEnviar = new FormGroup({
      nombre: new FormControl({ value: null, disabled: false }),
      apellidos: new FormControl({ value: null, disabled: false }),
      correoCorporativo: new FormControl({ value: null, disabled: false }),
      titulo: new FormControl({ value: null, disabled: false }),
      telefono: new FormControl({ value: null, disabled: false }),
      celular: new FormControl({ value: null, disabled: false }),
    }),

      this.formCambiar = new FormGroup({
        password: new FormControl({ value: null, disabled: false }),
        passwordConfig: new FormControl({ value: null, disabled: false }),
      })
    this.getUsuario();

  }

  getUsuario() {
    debugger;
    let id = localStorage.getItem('id');
    this.editarinfoadminService.getUsuario(id).subscribe(
      res => {
        let data = res['body'].personaDto;
        let titulo = res['body'].titulo;
        this.idUsuario = data.id;
        this.viewsUsuario(data, titulo);
      },
      err => {
      }
    )
  }

  viewsUsuario(data, titulo) {
    this.formEnviar.get('nombre').setValue(data ? data.nombre : null);
    this.formEnviar.get('apellidos').setValue(data ? data.apellidos : null);
    this.formEnviar.get('correoCorporativo').setValue(data ? data.correoCorporativo : null);
    this.formEnviar.get('titulo').setValue(titulo ? titulo : null);
    this.formEnviar.get('telefono').setValue(data ? data.telefono : null);
    this.formEnviar.get('celular').setValue(data ? data.celular : null);
  }

  postUsuario() {
    let obj = this.formEnviar.value;
    this.editarinfoadminService.postUsuario(obj).subscribe(
      res => {
        console.log(res);
      },
      err => {
      }
    )
  }

  postSeguridad() {
    let obj = {
      passwordConfig: this.formCambiar.value.passwordConfig,
      idUsuario: this.idUsuario
    }
    this.editarinfoadminService.postPassword(obj).subscribe(
      res => {
        console.log(res);
      },
      err => {
      }
    )
  }

}
