import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicarNoticiasComponent } from './publicar-noticias.component';

describe('PublicarNoticiasComponent', () => {
  let component: PublicarNoticiasComponent;
  let fixture: ComponentFixture<PublicarNoticiasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PublicarNoticiasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicarNoticiasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
