import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SolicitarPublicarComponent } from './solicitar-publicar.component';

describe('SolicitarPublicarComponent', () => {
  let component: SolicitarPublicarComponent;
  let fixture: ComponentFixture<SolicitarPublicarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SolicitarPublicarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SolicitarPublicarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
