import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmpresasMenuComponent } from './empresas-menu.component';

describe('EmpresasMenuComponent', () => {
  let component: EmpresasMenuComponent;
  let fixture: ComponentFixture<EmpresasMenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmpresasMenuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmpresasMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
