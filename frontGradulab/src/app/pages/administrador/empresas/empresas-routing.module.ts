import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmpresasMenuComponent } from './empresas-menu/empresas-menu.component';

const routes: Routes = [
  { path:'', component: EmpresasMenuComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmpresasRoutingModule { }
