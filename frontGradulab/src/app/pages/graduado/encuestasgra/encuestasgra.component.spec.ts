import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EncuestasgraComponent } from './encuestasgra.component';

describe('EncuestasgraComponent', () => {
  let component: EncuestasgraComponent;
  let fixture: ComponentFixture<EncuestasgraComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EncuestasgraComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EncuestasgraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
