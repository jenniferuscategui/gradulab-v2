import {Component, OnDestroy, OnInit} from '@angular/core';
import Swal from 'sweetalert2';
import {AuthService} from '../../../servicios/auth.service';
import {Subscription} from 'rxjs';
import {Persona} from '../../../common/models/persona';
import {Router} from '@angular/router';

@Component({
  selector: 'app-recuperarusuario',
  templateUrl: './recuperarusuario.component.html',
  styleUrls: ['./recuperarusuario.component.css']
})
export class RecuperarusuarioComponent implements OnInit, OnDestroy {
  cedula: string;
  correo: string;
  nombre: string;
  persona: Persona;
  mySlogin: Subscription;
  constructor( private authService: AuthService,
               private router: Router
  ) { }

  ngOnInit(): void {
    this.persona = new Persona();
    this.correo = '';
    this.cedula = '';
    this.nombre = '';

  }

  solicitarCambio(){
    if (this.persona.nombre === undefined || this.persona.correoPersonal === undefined || this.persona.documento === undefined){
      Swal.fire('Error', 'Campos vacios', 'error');
    }else {
      this.mySlogin  = this.authService.cambioDeCorreo(this.persona).subscribe(response => {
        if (response.body) {
          Swal.fire('Alerta', response.body, 'success');
          this.router.navigate(['/informacion']);
        }
      }, error => {
        if (error.code === 400){
          Swal.fire('Datos inavlidos', '', 'error');
        }
      });
    }

  }

  ngOnDestroy() {
    if (this.mySlogin != null) {
      this.mySlogin.unsubscribe();
    }
  }

}
