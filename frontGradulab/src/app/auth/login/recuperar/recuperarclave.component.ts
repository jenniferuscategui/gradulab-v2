import {Component, OnDestroy, OnInit} from '@angular/core';
import {AuthService} from '../../../servicios/auth.service';
import {Subscription} from 'rxjs';
import Swal from "sweetalert2";
import {Router} from "@angular/router";

@Component({
  selector: 'app-recuperarclave',
  templateUrl: './recuperarclave.component.html',
  styleUrls: ['./recuperarclave.component.css']
})
export class RecuperarclaveComponent implements OnInit, OnDestroy {
  cedula: string;
  mySlogin: Subscription;
  constructor(
    private authService: AuthService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.cedula = '';
  }

  solicitarCambio(){
    this.mySlogin  = this.authService.recurarContrasena(this.cedula).subscribe(response => {
      if (response.body) {
      Swal.fire('Alerta', response.body, 'success');
      this.router.navigate(['/informacion']);
      }
      if (response.code === '001' || response.code === '002'){
        Swal.fire('Error', response.message, 'error');
      }
    }, error => {
      if (error.code === 400){
        Swal.fire('Datos inavlidos', '', 'error');
      }
    });
  }

  ngOnDestroy() {
    if (this.mySlogin != null){
      this.mySlogin.unsubscribe();
    }
  }


}
