import {Component, OnDestroy, OnInit} from '@angular/core';
import {Usuario} from '../../common/models/usuario';
import Swal from 'sweetalert2';
import {AuthService} from '../../servicios/auth.service';
import {Router} from '@angular/router';
import {Persona} from '../../common/models/persona';
import { NgxSpinnerService } from 'ngx-spinner';
import {connectableObservableDescriptor} from 'rxjs/internal/observable/ConnectableObservable';
import {FormBuilder, Validators} from '@angular/forms';
import {ConstantesValidaciones} from '../../common/constanstes/validaciones';
import {ContantesMensajes} from "../../common/constanstes/contantesMensajes";
import {Subscription} from "rxjs";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {
  usuario: Usuario;
  logGraduado = true;
  logEmpresa: boolean;
  logDirector: boolean;
  mySlogin: Subscription;
  constructor(
    private authService: AuthService,
    private router: Router,
    private spinner: NgxSpinnerService,
    private fb: FormBuilder,
  ) {
    this.usuario = new Usuario();
  }

  error = {
    message: ContantesMensajes.MGS_ERROR
  };

  public loginFrom = this.fb.group({
      cedula: ['', Validators.required],
      contrasena: ['', Validators.required],
      email: ['', [Validators.required, Validators.pattern(ConstantesValidaciones.REGEX.EMAIL)]],
    },
  );

  ngOnInit(): void {
    this.usuario = new Usuario();
    this.logGraduado = true;
    this.logDirector = false;
    this.logEmpresa = false;
  }

  cambiarLog( boGraduado, boEmpresa, boDirector): void{
    this.loginFrom.get('contrasena').setValue('');
    this.loginFrom.get('cedula').setValue('');
    this.logGraduado = boGraduado;
    this.logDirector = boDirector;
    this.logEmpresa  = boEmpresa;
    if (!this.logEmpresa){
      this.loginFrom.get('cedula').setValue('');
      this.loginFrom.get('email').setValue('example@example.com');
      this.loginFrom.get('email').clearValidators();
    } else {
      this.loginFrom.get('email').setValue('');
      this.loginFrom.get('cedula').setValue('1234565');
    }

  }


  login(): void {
      if (this.logEmpresa){
        this.usuario.login = this.loginFrom.get('email').value;
      }else {
        this.usuario.login = this.loginFrom.get('cedula').value;
      }
      this.usuario.contrasena = this.loginFrom.get('contrasena').value;
      if (this.usuario.login === undefined || this.usuario.contrasena === undefined){
      Swal.fire('Error en el fromulario', 'cedula y contraseña vacias', 'error');
      return;
    }

      this.mySlogin  = this.authService.login(this.usuario).subscribe(response => {

      localStorage.setItem('id', response['id_usuario']);
      this.authService.guardarUsuario(response.access_token);
      this.authService.guardarToken(response.access_token);
      const usuario = this.authService.usuario;
      if (this.authService.hasRol('ROLE_GRADUADO')){
        this.router.navigate(['/graduado/publicaciones']);
      } else if (this.authService.hasRol('ROLE_DIRECTOR_PROGRAMA')){
        this.router.navigate(['/administrador/graduados']);
      } else if (this.authService.hasRol('ROLE_EMPRESA')){
        this.router.navigate(['/empresa/ofertalabempr']);
      }
     //  Swal.fire('Inicio de sesion', `HoHla ${usuario.persona.nombre}, Has iniciado sesion con exito`, 'success');
    }, error => {
      if (error.status === 400){
        Swal.fire('Datos inavlidos', '', 'error');
      }
    });
  }

  ngOnDestroy(): void {
    if (this.mySlogin != null){
      this.mySlogin.unsubscribe();
    }
  }


}
