import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {ContantesMensajes} from '../../common/constanstes/contantesMensajes';
import {ConstantesValidaciones} from '../../common/constanstes/validaciones';
import {PasswordService} from '../../servicios/password.service';
import {GraduadoService} from '../../servicios/graduado.service';
import {RegistroUsuario} from '../../common/models/registroUsuario';
import {Subscription} from 'rxjs';
import Swal from 'sweetalert2';
import {Router} from '@angular/router';
import {Empresa} from "../../common/models/empresa";
import {EmpresaService} from "../../servicios/empresa.service";

@Component({
  selector: 'app-registrograduado',
  templateUrl: './registrograduado.component.html',
  styleUrls: ['./registrograduado.component.css']
})
export class RegistroGraduadoComponent implements OnInit, OnDestroy {
  public formSumited = false;
  public registroUsuario: RegistroUsuario;
  public myRegistro$: Subscription;
  empresa: Empresa;
  vieneEmpresa: boolean;

  public registroGraduadoFrom = this.fb.group({
    nombre : ['', Validators.required],
    apellido : ['', Validators.required],
    cedula : ['', Validators.required],
    codigo : ['', Validators.required],
    celular : ['', [Validators.pattern(ConstantesValidaciones.REGEX.ONLY_NUMBERS), Validators.minLength(7)]],
    pass : ['', Validators.required],
    pass2 : ['', Validators.required],
    email: ['', [Validators.required, Validators.pattern(ConstantesValidaciones.REGEX.EMAIL)]],
    terminos: [true, Validators.required]
    }, {
    validators:  this.passService.passwordIguales('pass', 'pass2')}
  );

  constructor(
    private fb: FormBuilder,
    private passService: PasswordService,
    private graduadoService: GraduadoService,
    private router: Router,
    private empresaService: EmpresaService
  ) {
  }
  error = {
    message: ContantesMensajes.MGS_ERROR
  };

  ngOnInit(): void {
    this.registroUsuario = new RegistroUsuario();
    this.empresa = this.passService.empresa;
    if (this.empresa !== undefined ){
      this.registroUsuario.empresaDto = this.empresa;
      this.vieneEmpresa = true;
      this.registroGraduadoFrom.get('codigo').setValue(' ');
      this.registroGraduadoFrom.get('email').setValue(this.empresa.email);

    }
  }

  registrarGraduado(): void{
    this.formSumited = true;
    if (this.registroGraduadoFrom.valid){
      this.registroUsuario.contrasena = this.registroGraduadoFrom.get('pass2').value;
      this.registroUsuario.personaDto.nombre = this.registroGraduadoFrom.get('nombre').value;
      this.registroUsuario.personaDto.apellidos = this.registroGraduadoFrom.get('apellido').value;
      this.registroUsuario.personaDto.documento = this.registroGraduadoFrom.get('cedula').value;
      this.registroUsuario.personaDto.codigoGraduado = this.registroGraduadoFrom.get('codigo').value;
      this.registroUsuario.personaDto.celular = this.registroGraduadoFrom.get('celular').value;
      this.registroUsuario.personaDto.correoPersonal = this.registroGraduadoFrom.get('email').value;
      this.validarSiEsEmpresa();
    } else {
      Swal.fire('Error en Formulario', '', 'error');
    }
  }

  atras(){
    this.router.navigate(['/registroempresa']);
  }



  validarSiEsEmpresa(){
    if (this.vieneEmpresa){
      this.myRegistro$ = this.empresaService.crearRegistroEmpresa(this.registroUsuario).subscribe(response => {
        if (response.code === '002'){
          Swal.fire('Error', response.message, 'error');
        }else {
          Swal.fire('Solicitud Enviada', '', 'success');
          this.formSumited = false;
          this.router.navigate(['/informacion']);
        }
      });
    }else {
      this.myRegistro$ = this.graduadoService.crearRegistroGraduado(this.registroUsuario).subscribe(response => {
        if (response.code === '002'){
          Swal.fire('Error', response.message, 'error');
        }else {
          Swal.fire('Solicitud Enviada', '', 'success');
          this.formSumited = false;
          this.router.navigate(['/informacion']);
        }
      });
    }
  }

  ngOnDestroy() {
    if (this.myRegistro$ != null){
      this.myRegistro$.unsubscribe();
    }
  }

}
