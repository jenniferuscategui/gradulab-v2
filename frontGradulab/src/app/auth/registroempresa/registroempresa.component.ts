import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {ConstantesValidaciones} from '../../common/constanstes/validaciones';
import {PasswordService} from '../../servicios/password.service';
import {Empresa} from '../../common/models/empresa';
import {ContantesMensajes} from '../../common/constanstes/contantesMensajes';
import {Router} from '@angular/router';

@Component({
  selector: 'app-registroempresa',
  templateUrl: './registroempresa.component.html',
  styleUrls: ['./registroempresa.component.css']
})
export class RegistroempresaComponent implements OnInit {
  public empresa: Empresa;
  public registroEmpresaFrom = this.fb.group({
      nit: ['', Validators.required],
      sector: ['', Validators.required],
    razonsocial: ['', Validators.required],
      ubicacion: ['', Validators.required],
    telefono: ['', [Validators.required, Validators.pattern(ConstantesValidaciones.REGEX.ONLY_NUMBERS), Validators.minLength(7)]],
      email: ['javiernuma@gmail.com', [Validators.required, Validators.pattern(ConstantesValidaciones.REGEX.EMAIL)]],
    },
  );

  error = {
    message: ContantesMensajes.MGS_ERROR
  };

  constructor(
    private fb: FormBuilder,
    private passService: PasswordService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.empresa = new Empresa();
    if (this.passService.empresa !== undefined){
      this.registroEmpresaFrom.get('nit').setValue(this.passService.empresa.nit);
      this.registroEmpresaFrom.get('razonsocial').setValue(this.passService.empresa.nombre);
      this.registroEmpresaFrom.get('email').setValue(this.passService.empresa.email);
      this.registroEmpresaFrom.get('ubicacion').setValue(this.passService.empresa.direccion);
      this.registroEmpresaFrom.get('sector').setValue(this.passService.empresa.area);
      this.registroEmpresaFrom.get('telefono').setValue(this.passService.empresa.telefono);
    }
  }

  siguientePaso(){
    this.empresa.nit = this.registroEmpresaFrom.get('nit').value;
    this.empresa.nombre = this.registroEmpresaFrom.get('razonsocial').value;
    this.empresa.email = this.registroEmpresaFrom.get('email').value;
    this.empresa.direccion = this.registroEmpresaFrom.get('ubicacion').value;
    this.empresa.area = this.registroEmpresaFrom.get('sector').value;
    this.empresa.telefono = this.registroEmpresaFrom.get('telefono').value;
    this.passService.guardarInfoEmpresa(this.empresa);
    console.log(this.registroEmpresaFrom);
    this.router.navigate(['/registrograduado']);
  }

}
