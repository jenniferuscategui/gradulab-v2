import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegistroempresaComponent } from './auth/registroempresa/registroempresa.component';
import { IniciopublicoComponent } from './pages/informacion/iniciopublico.component';
import { PagnotfoundComponent } from './pages/pagnotfound/pagnotfound.component';
import { InicioComponent } from './pages/inicio.component';
import { IniciosComponent } from './auth/inicios.component';
import { NoticiasComponent } from './pages/administrador/noticias/noticias.component';
import { LoginComponent } from './auth/login/login.component';
import { EditargraduadoComponent } from './pages/administrador/editargraduado/editargraduado.component';
import { PublicacionesComponent } from './pages/graduado/publicaciones/publicaciones.component';
import { GraduadoComponent } from './pages/graduado/graduado.component';
import { EditarperfilgradComponent } from './pages/graduado/editarperfilgrad/editarperfilgrad.component';
import { AplicarofertaComponent } from './pages/ofertalaboral/aplicaroferta/aplicaroferta.component';
import { ListaofertasComponent } from './pages/ofertalaboral/listaofertas/listaofertas.component';
import { OferlabempresaComponent } from './pages/empresa/oferlabempresa/oferlabempresa.component';
import { EditarinfoadmComponent } from './pages/administrador/editarinfoadm/editarinfoadm.component';
import { EditarperfilempComponent } from './pages/empresa/editarperfilemp/editarperfilemp.component';
import { ReportesComponent } from './pages/administrador/reportes/reportes.component';
import { BackupsadminComponent } from './pages/administrador/backupsadmin/backups.component';
import { RecuperarclaveComponent } from './auth/login/recuperar/recuperarclave.component';
import { RecuperarusuarioComponent } from './auth/login/recuperar/recuperarusuario.component';
import { DetalleNoticiasComponent } from './pages/detalle-noticias/detalle-noticias.component';
import { VerestadopostComponent } from './pages/graduado/verestadopost/verestadopost.component';
import { NotificacionComponent } from './pages/empresa/notificacion/notificacion.component';
import { MenuempresaComponent } from './pages/empresa/menuempresa/menuempresa.component';
import { NotificacionesComponent } from './pages/graduado/notificaciones/notificaciones.component';
import { EncuestasComponent } from "./pages/administrador/encuestas/encuestas/encuestas.component";
import { PreguntasComponent } from "./pages/administrador/encuestas/preguntas/preguntas.component";
import { OpcionesComponent } from "./pages/administrador/encuestas/opciones/opciones.component";
import { EncuestaComponent } from "./pages/empresa/encuesta/encuesta.component";
import { EncuestasgraComponent } from "./pages/graduado/encuestasgra/encuestasgra.component";
import { AuthGuard } from './guards/auth.guard';
import { RoleGuard } from './guards/role.guard';
import { RegistroGraduadoComponent } from './auth/registrograduado/registrograduado.component';
import { PublicarNoticiasComponent } from './pages/administrador/noticias/publicar-noticias/publicar-noticias.component';
import { ListarNoticiasAdmComponent } from './pages/administrador/noticias/listar-noticias/listar-noticias.component';
import { GraduadoMenuComponent } from './pages/administrador/graudados/graduado-menu/graduado-menu.component';
import { SolicitudRegistroComponent } from './pages/administrador/graudados/graduado-menu/solicitud-registro/solicitud-registro.component';
import { CargarInformacionComponent } from './pages/administrador/graudados/graduado-menu/cargar-informacion/cargar-informacion.component';
import { InformacionGraduadoComponent } from './pages/administrador/graudados/graduado-menu/informacion-graduado/informacion-graduado.component';
import { NotificacionGraduadoComponent } from './pages/administrador/graudados/graduado-menu/notificacion-graduado/notificacion-graduado.component';
import { EmpresasMenuComponent } from './pages/administrador/empresas/empresas-menu/empresas-menu.component';
import { SolicitudRegistroEmpresaComponent } from './pages/administrador/empresas/empresas-menu/solicitud-registro/solicitud-registro.component';
import { SolicitarPublicarComponent } from './pages/administrador/empresas/empresas-menu/solicitar-publicar/solicitar-publicar.component';
import { OfertaLaboralMenuComponent } from './pages/administrador/oferta-laboral/oferta-laboral-menu/oferta-laboral-menu.component';
import { CrearOfertaComponent } from './pages/administrador/oferta-laboral/oferta-laboral-menu/crear-oferta/crear-oferta.component';
import { ListarOfertaComponent } from './pages/administrador/oferta-laboral/oferta-laboral-menu/listar-oferta/listar-oferta.component';






const routes: Routes = [
  {
    path: 'administrador', component: InicioComponent,
    canActivate: [AuthGuard, RoleGuard], data: { role: 'ROLE_DIRECTOR_PROGRAMA' },
    children: [
      { path: 'editarperfil', component: EditarinfoadmComponent },
      { path: 'noticias', component: NoticiasComponent,
      children: [
        { path: '', component: PublicarNoticiasComponent },
        { path: 'publicar-noticias', component: PublicarNoticiasComponent },
        { path: 'listar-noticias', component: ListarNoticiasAdmComponent },
      ]
    },
      { path: 'editargraduados', component: EditargraduadoComponent },
      {
        path: 'graduados', component: GraduadoMenuComponent,
        children: [
          { path: '', component: SolicitudRegistroComponent },
          { path: 'registro', component: SolicitudRegistroComponent },
          { path: 'cargar-informacion', component: CargarInformacionComponent },
          { path: 'informacion-graduado', component: InformacionGraduadoComponent },
          { path: 'notificacion', component: NotificacionGraduadoComponent },
        ]
      },
      {
        path: 'empresas', component: EmpresasMenuComponent,
        children: [
          { path: '', component: SolicitudRegistroEmpresaComponent },
          { path: 'solicitud-registro', component: SolicitudRegistroEmpresaComponent },
          { path: 'solicitar-publicar', component: SolicitarPublicarComponent },
        ]
      },
      { path: 'ofertalaboral', component: OfertaLaboralMenuComponent,
      children: [
        { path: '', component: CrearOfertaComponent },
        { path: 'crear-oferta', component: CrearOfertaComponent },
        { path: 'editar-oferta', component: ListarOfertaComponent },
      ] },
      { path: 'encuestas', component: EncuestasComponent },
      { path: 'reportes', component: ReportesComponent },

      { path: 'backupsadmin', component: BackupsadminComponent },

      { path: 'preguntas', component: PreguntasComponent },
      { path: 'opciones', component: OpcionesComponent },
    ]
  },
  {
    path: 'empresa', component: MenuempresaComponent, canActivate: [AuthGuard, RoleGuard], data: { role: 'ROLE_EMPRESA' },
    children: [
      { path: 'ofertalabempr', component: OferlabempresaComponent }, /*Pertenece a la empresa*/
      { path: 'editarperfil', component: EditarperfilempComponent },
      { path: 'encuesta', component: EncuestaComponent },
      { path: '', redirectTo: '/ofertalabempr', pathMatch: 'full' },
      { path: 'notifcaciones', component: NotificacionComponent },
    ]
  },
  {
    path: 'graduado', component: GraduadoComponent, canActivate: [AuthGuard, RoleGuard], data: { role: 'ROLE_GRADUADO' },
    children: [
      { path: 'publicaciones', component: PublicacionesComponent }, /*pertenece al graduado*/
      { path: 'editarperfil', component: EditarperfilgradComponent }, /*Pertenece a las vistas de graduado*/
      { path: 'aplicaroferta', component: AplicarofertaComponent }, /*Pertenece a las vistas de graduado*/
      { path: 'listaoferta', component: ListaofertasComponent }, /*Pertenece a las vistas de graduado*/
      { path: 'postulacion', component: VerestadopostComponent },
      { path: 'notificaiones', component: NotificacionesComponent },
      { path: 'encuesta', component: EncuestasgraComponent }
    ]
  },
  {
    path: '', component: IniciosComponent,
    children: [
      { path: 'registrograduado', component: RegistroGraduadoComponent },
      { path: 'registroempresa', component: RegistroempresaComponent },
      { path: 'informacion', component: IniciopublicoComponent },
      { path: 'login', component: LoginComponent },
      { path: 'recuperarclave', component: RecuperarclaveComponent },
      { path: 'recuperarusuario', component: RecuperarusuarioComponent },
      { path: '', redirectTo: '/informacion', pathMatch: 'full' }
    ]
  },
  { path: 'listnoticias', component: DetalleNoticiasComponent },
  // { path: '**', component: PagnotfoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
