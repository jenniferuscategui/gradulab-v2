import { Injectable } from '@angular/core';
import {RegistroUsuario} from '../common/models/registroUsuario';
import {ReponseObject} from '../common/models/reponseObject';
import {environment} from '../../environments/environment';
import {catchError} from 'rxjs/operators';
import Swal from 'sweetalert2';
import {throwError} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {AuthService} from './auth.service';
import {AdministradorService} from './administrador.service';

@Injectable({
  providedIn: 'root'
})
export class EmpresaService {
  urlServcice = 'empresa';

  constructor(
    private http: HttpClient,
    private ruter: Router,
    private authService: AuthService,
    private adminService: AdministradorService,
  ) { }

  crearRegistroEmpresa(registroUsuario: RegistroUsuario): any{
    return this.http.post<ReponseObject<RegistroUsuario>>(
      `${environment.urlBase}${this.urlServcice}/registrarEmpresa` , registroUsuario);
  }
}
