import { Injectable } from '@angular/core';
import {FormGroup} from '@angular/forms';
import {Empresa} from '../common/models/empresa';

@Injectable({
  providedIn: 'root'
})
export class PasswordService {
  private _empresa: Empresa;
  constructor() { }

  passwordIguales(pass1Name: string, pass2Name: string){
    return (formGroup: FormGroup) => {
      const pass1Control = formGroup.get(pass1Name);
      const pass2Control = formGroup.get(pass2Name);

      if (pass1Control.value === pass2Control.value) {
        pass2Control.setErrors(null);
      } else {
        pass2Control.setErrors({noEsIgual: true});
      }

    };
  }

  guardarInfoEmpresa(empresa: Empresa): void {
    this._empresa = empresa;
    //sessionStorage.setItem('usuario', JSON.stringify(this._usuario));
  }

  public get empresa(): Empresa{
    return this._empresa;
  }
}
