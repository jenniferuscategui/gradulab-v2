import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {AuthService} from './auth.service';
import {AdministradorService} from './administrador.service';
import {Publicacion} from '../common/models/publicacion';
import {ReponseObject} from '../common/models/reponseObject';
import {environment} from '../../environments/environment';
import {catchError, map} from 'rxjs/operators';
import {Observable, throwError} from 'rxjs';
import {Comentario} from '../common/models/comentario';
import Swal from 'sweetalert2';
import {RegistroUsuario} from '../common/models/registroUsuario';
import {NgxSpinnerService} from "ngx-spinner";
import {Graduado} from "../common/models/graduado";
import {Usuario} from "../common/models/usuario";

@Injectable({
  providedIn: 'root'
})
export class GraduadoService {

  urlServcice = 'gradudado';
  UrlDirector = 'directorPrograma';

  constructor(
    private http: HttpClient,
    private ruter: Router,
    private authService: AuthService,
    private adminService: AdministradorService,
    private spinner: NgxSpinnerService
  ) { }

  crearRegistroGraduado(registroUsuario: RegistroUsuario): any{
    return this.http.post<ReponseObject<RegistroUsuario>>(
      `${environment.urlBase}${this.urlServcice}/registrarGraduado` , registroUsuario, {
      headers: this.adminService.agregarAutorizationHeader()
    }).pipe(
      catchError(e => {
        Swal.fire('Error al cargar', 'error');
        return throwError(e);
      })
    );
  }

  listarSolicitudes(): any {
    // const params = pageQuery;
    this.spinner.show();
    return this.http.get<ReponseObject<Usuario>>(`${environment.urlBase}${this.UrlDirector}/listarSoliciGraduados` )
      .pipe(
        catchError(e => {
          console.log(e.message);
          Swal.fire('Error al cargar', e.message, 'error');
          return throwError(e);
        })
      );
  }
/**
  subirImagen(archivo: File, id): Observable<Publicacion>{
    const formData = new FormData();
    formData.append('archivo', archivo);
    formData.append('id', id);
    return this.http.post<ReponseObject<Publicacion>>(`${environment.urlBase}publicacion/subir/imaben`, formData)
      .pipe(
        map((response: any) => response.publicacion as Publicacion),
        catchError(e => {
          Swal.fire('Error al cargar', e.message, 'error');
          return throwError(e);
        })
      );
  }

  subirImagen(archivo: File, id): Observable<Publicacion>{
    const formData = new FormData();
    formData.append('archivo', archivo);
    formData.append('id', id);
    return this.http.post<ReponseObject<Publicacion>>(`${environment.urlBase}publicacion/subir/imaben`, formData)
      .pipe(
        map((response: any) => response.publicacion as Publicacion),
        catchError(e => {
          Swal.fire('Error al cargar', e.message, 'error');
          return throwError(e);
        })
      );
  }

  meGusta(publicacion: Publicacion){
    return this.http.post(`${environment.urlBase}publicacion/megusta`, publicacion).pipe(
      catchError(e => {
        Swal.fire('Error al cargar', e.message, 'error');
        return throwError(e);
      })
    );
  }

  meEncanta(publicacion: Publicacion){
    return this.http.post(`${environment.urlBase}publicacion/meencanta`, publicacion).pipe(
      catchError(e => {
        Swal.fire('Error al cargar', e.message, 'error');
        return throwError(e);
      })
    );
  }

  comentarPublicacion(comentario: Comentario): any{
    return this.http.post<ReponseObject<Comentario>>(environment.urlBase + 'publicacion/crear', comentario).pipe(
      catchError(e => {
        Swal.fire('Error al cargar', e.message, 'error');
        return throwError(e);
      })
    );
  }
*/


}
