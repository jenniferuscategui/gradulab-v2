import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EditarinfoadminService {
  httpOptions = { headers: new HttpHeaders({}) };
  readonly url = environment.urlBase;

  constructor(private http: HttpClient) { }

  getUsuario(id) {
    return this.http.get(this.url + `usuario/obtenerInfo/${id}`, this.httpOptions);
  }

  postUsuario(obj) {
    return this.http.post(this.url + 'usuario/editarPerfil', obj, this.httpOptions);
  }

  postPassword(obj) {
    return this.http.post(this.url + 'usuario/cambiarclave', obj, this.httpOptions);
  }

}
