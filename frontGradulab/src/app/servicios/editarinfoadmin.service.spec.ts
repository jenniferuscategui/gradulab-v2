import { TestBed } from '@angular/core/testing';

import { EditarinfoadminService } from './editarinfoadmin.service';

describe('EditarinfoadminService', () => {
  let service: EditarinfoadminService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EditarinfoadminService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
