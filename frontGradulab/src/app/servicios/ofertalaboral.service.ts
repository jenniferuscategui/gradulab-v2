import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {AuthService} from './auth.service';
import {AdministradorService} from './administrador.service';
import {ListaofertasComponent} from '../pages/ofertalaboral/listaofertas/listaofertas.component';
import {ReponseObject} from '../common/models/reponseObject';
import {environment} from '../../environments/environment';
import {catchError} from 'rxjs/operators';
import {throwError} from 'rxjs';
import {OfertaLaboral} from '../common/models/ofertaLaboral';
import Swal from "sweetalert2";

@Injectable({
  providedIn: 'root'
})
export class OfertalaboralService {

  constructor(
    private http: HttpClient,
    private ruter: Router,
    private authService: AuthService,
    private adminService: AdministradorService,
  )  { }

  crearOferta( ){}

  listarOferta(ofertalaboral: OfertaLaboral): any{
    return this.http.post<ReponseObject<ListaofertasComponent>>(environment.urlBase + 'ofertalaboral/listar', OfertaLaboral, {
      headers: this.adminService.agregarAutorizationHeader()
    }).pipe(
      catchError(e => {
        Swal.fire('Error al listar', e.message, 'error');
        return throwError(e);
      })
    );
  }

  listarcandidatos(ofertalaboral: OfertaLaboral): any {
    return this.http.post<ReponseObject<ListaofertasComponent>>(environment.urlBase + 'ofertalaboral/listarcandidatos', OfertaLaboral, {
    headers: this.adminService.agregarAutorizationHeader()
  }).pipe(
    catchError(e => {
  Swal.fire('Error al listar', e.message, 'error');
  return throwError(e);
})
);
  }


}
