package com.gradulab.reportes.general.exception;

import com.gradulab.reportes.general.constants.ResponseCodes;
import com.gradulab.reportes.general.utils.ResponseObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GradulabExceptionHandler extends Exception {

    @Autowired
    private MessageSource messageSource;

    private static final Logger logger = LoggerFactory.getLogger(GradulabExceptionHandler.class);

    private String message;

    private Integer errorCode;


    /**
     * Constructor por defecto
     */
    public GradulabExceptionHandler(){}


    public GradulabExceptionHandler(String message) {
        this.message = message;
    }

    public GradulabExceptionHandler(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public GradulabExceptionHandler(String message,Integer errorCode) {
        this.message = message;
        this.errorCode = errorCode;
    }

    @Override
    public String getMessage(){
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    /**
     * Se crea metodo para manejar las excepciones no controladas en los rest del microservicio.
     * @param exception
     * @return
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> processException( Exception exception){
        logger.error(exception.getMessage());
        return buildResponseEntity(new ResponseObject<>(ResponseCodes.EXCEPTION_CODE,
                messageSource.getMessage("exception.message", null, LocaleContextHolder.getLocale())),HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * Se crea el metodo para manejar las excepciones en los rest del microservicio.
     * Captura excepciones de tipo GradulabExceptionHandler y entrega el mensaje al front.
     * @param gradulabExceptionHandler excepcion que captura
     * @return el responseEntity de tipo object
     */
    @ExceptionHandler(GradulabExceptionHandler.class)
    public ResponseEntity<Object> processGradulabExceptionHandler( GradulabExceptionHandler gradulabExceptionHandler){
        logger.error(gradulabExceptionHandler.getMessage());
        return buildResponseEntity(new ResponseObject<>(ResponseCodes.REQUEST_ERROR_CODE, gradulabExceptionHandler.getMessage()),HttpStatus.OK);
    }


    /**
     * Se crea el response entity para crear la respuesta al momento de llamar la excepcion de gradulab
     * @param responseObject el objeto que se responde para la excepcion
     * @param status el estado que devuelve la exception al momento que se lance
     * @return responde el responseEntity de tipo objecto
     */
    private ResponseEntity<Object> buildResponseEntity(ResponseObject<Void> responseObject, HttpStatus status){
        return new ResponseEntity<>(responseObject, status);
    }
}
