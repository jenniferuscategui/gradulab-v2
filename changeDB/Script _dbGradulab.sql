/**
  Se crea la tabla persona
 */
CREATE TABLE GDL_Persona (
    pers_id                     BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    pers_tipo_docuemnto         VARCHAR(20),
    pers_documento              VARCHAR(50) UNIQUE,
    pers_apellidos              VARCHAR(150),
    pers_nombre                 VARCHAR(100),
    pers_sexo                   VARCHAR(20),
    pers_fecha_nacimiento       DATE,
    pers_lugar_nacimiento       VARCHAR(200)     NULL ,
    pers_direccion              VARCHAR(200)      NULL ,
    pers_telefono               VARCHAR(50)      NULL ,
    pers_ccelular               VARCHAR(50)      NULL ,
    pers_correo_personal        VARCHAR(200) NOT NULL ,
    pers_correo_corporativo     VARCHAR(200)     NULL ,
    pers_fotografia             VARCHAR(200)     NULL,
    pers_fecha_ingreso_reg      DATE NOT NULL,
    pers_fecha_modifica_reg     DATE
);

/**
  Se crea la tabla usuario
 */
CREATE TABLE GDL_Usuario (
    usua_id                     BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    usua_login                  VARCHAR(100),
    usua_contraseña             VARCHAR(50),
    usua_habilitado             VARCHAR(150),
    usua_fecha_ingreso          DATE,
    usua_pers_id                BIGINT UNSIGNED,
    usua_fecha_ingreso_reg      DATE NOT NULL,
    usua_fecha_modifica_reg     DATE,
    FOREIGN KEY (usua_pers_id) REFERENCES GDL_Persona(pers_id)
);

/**
  Se crea la tabla rol
 */
CREATE TABLE GDL_Rol (
    rol_id                     BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    rol_nombre                 VARCHAR(20),
    rol_fecha_ingreso_reg      DATE NOT NULL,
    rol_fecha_modifica_reg     DATE
);


/**
  Se crea la tabla roles_usuarios
 */
CREATE TABLE GDL_Roles_Usuarios (
    rous_id                          BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    rous_usuario_id                  BIGINT UNSIGNED,
    rous_rol_id                      BIGINT UNSIGNED,
    rous_fecha_ingreso_reg           DATE NOT NULL,
    rous_fecha_modifica_reg          DATE,
    FOREIGN KEY (rous_usuario_id) REFERENCES GDL_Usuario(usua_id ),
    FOREIGN KEY (rous_rol_id) REFERENCES GDL_Rol(rol_id)
);


/**
  Se crea la tabla GDL_Graduado
 */
CREATE TABLE GDL_Graduado (
    grad_id                  BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    grad_fecha_grado         DATE,
    grad_fecha_solicitud     DATE,
    grad_codigo              VARCHAR(100) UNIQUE,
    grad_estado              VARCHAR(10),
    grad_modalidad           VARCHAR(100),
    grad_nombre_trabajo      VARCHAR(100),
    grad_folio               VARCHAR(100),
    grad_acta                VARCHAR(100) NULL ,
    grad_pers_id             BIGINT UNSIGNED NULL ,
    grad_fecha_ingreso_reg   DATE NOT NULL,
    grad_fecha_modifica_reg  DATE,
    FOREIGN KEY (grad_pers_id) REFERENCES GDL_Persona(pers_id )

);

/**
  Se crea la tabla GDL_Info_Laboral
 */
CREATE TABLE GDL_Info_Laboral (
    inla_id                  BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    inla_empresa             VARCHAR(100),
    inla_fecha_inicio        DATE,
    inla_fecha_final         DATE,
    inla_cargo               VARCHAR(100),
    inla_area                VARCHAR(100),
    inla_grad_id             BIGINT UNSIGNED,
    inla_fecha_ingreso_reg   DATE NOT NULL,
    inla_fecha_modifica_reg  DATE,
    FOREIGN KEY (inla_grad_id) REFERENCES GDL_Graduado(grad_id )

);



/**
  Se crea la tabla GDL_Info_academica
 */
CREATE TABLE GDL_Info_Academica (
    inac_id                  BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    inac_nombre              VARCHAR(100),
    inac_fecha               DATE,
    inac_tipo                VARCHAR(50),
    inac_titulo_obt          VARCHAR(100),
    inac_institucion         VARCHAR(100),
    inac_grad_id             BIGINT UNSIGNED,
    inac_fecha_ingreso_reg   DATE NOT NULL,
    inac_fecha_modifica_reg  DATE,
    FOREIGN KEY (inac_grad_id) REFERENCES GDL_Graduado(grad_id )

);


/**
  Se crea la tabla GDL_Tipo_Red
 */
CREATE TABLE GDL_Tipo_Red (
    tire_id                  BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    tire_nombre              VARCHAR(100),
    tire_icono               VARCHAR(100),
    tire_fecha_ingreso_reg   DATE NOT NULL,
    tire_fecha_modifica_reg  DATE
);

/**
  Se crea la tabla GDL_enlace_redes
 */
CREATE TABLE GDL_Enlace_Redes (
    enre_id                  BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    enre_nombre              VARCHAR(100),
    enre_tire_id             BIGINT UNSIGNED NOT NULL,
    enre_grad_id             BIGINT UNSIGNED NOT NULL,
    enre_fecha_ingreso_reg   DATE NOT NULL,
    enre_fecha_modifica_reg  DATE,
    FOREIGN KEY (enre_tire_id) REFERENCES GDL_Tipo_Red(tire_id),
    FOREIGN KEY (enre_grad_id) REFERENCES GDL_Graduado(grad_id)

);


/**
  Se crea la tabla GDL_Empresa
 */
CREATE TABLE GDL_Empresa (
    empr_id                  BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    empr_nombre              VARCHAR(100),
    empr_nit                 VARCHAR(100) UNIQUE,
    empr_direccion           VARCHAR(150),
    empr_email               VARCHAR (200),
    empr_telefono            VARCHAR(100),
    empr_area                VARCHAR(100),
    empr_fecha_ingreso_reg   DATE NOT NULL,
    empr_fecha_modifica_reg  DATE

);


/**
  Se crea la tabla GDL_Contacto
 */
CREATE TABLE GDL_Contacto (
    cont_id                  BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    cont_empr_id             BIGINT UNSIGNED,
    cont_pers_id             BIGINT UNSIGNED,
    cont_fecha_ingreso_reg   DATE NOT NULL,
    cont_fecha_modifica_reg  DATE,
    FOREIGN KEY (cont_empr_id) REFERENCES GDL_Empresa(empr_id),
    FOREIGN KEY (cont_pers_id) REFERENCES GDL_Persona(pers_id)

);

/**
  Se crea la tabla GDL_Contacto
 */
CREATE TABLE GDL_Oferta_Laboral (
    ofla_id                 BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    ofla_estado             VARCHAR(20),
    ofla_descripcion        VARCHAR(100),
    ofla_cargo              VARCHAR(100),
    ofla_experiencia        VARCHAR(100),
    ofla_salario            VARCHAR(100),
    ofla_tipo_contrato      VARCHAR(20),
    ofla_conocimiento       VARCHAR(100),
    ofla_empr_id            BIGINT UNSIGNED,
    ofla_fecha_ingreso_reg   DATE NOT NULL,
    ofla_fecha_modifica_reg  DATE,
    FOREIGN KEY (ofla_empr_id) REFERENCES GDL_Empresa(empr_id)
);


/**
  Se crea la tabla GDL_Candidato
 */
CREATE TABLE GDL_Candidato (
    cand_id                  BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    cand_grad_id             BIGINT UNSIGNED,
    cand_ofla_id             BIGINT UNSIGNED,
    cand_fecha_ingreso_reg   DATE NOT NULL,
    cand_fecha_modifica_reg  DATE,
    FOREIGN KEY (cand_grad_id) REFERENCES GDL_Graduado(grad_id),
    FOREIGN KEY (cand_ofla_id) REFERENCES GDL_Oferta_Laboral(ofla_id)

);


/**
  Se crea la tabla GDL_Publicacion
 */
CREATE TABLE GDL_Publicacion (
    publ_id                  BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    publ_contrenido          VARCHAR(200),
    publ_multimedia          VARCHAR(200),
    publ_fecha_pu            DATE NOT NULL,
    publ_fecha_ingreso_reg   DATE NOT NULL,
    publ_fecha_modifica_reg  DATE,
    publ_usua_id             BIGINT UNSIGNED,
    FOREIGN KEY (publ_usua_id) REFERENCES GDL_Usuario(usua_id)

);


/**
  Se crea la tabla GDL_Comentario
 */
CREATE TABLE GDL_Comentario (
    come_id                  BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    come_descripcion         VARCHAR(200),
    come_publ_id             BIGINT UNSIGNED,
    come_usua_id             BIGINT UNSIGNED,
    come_fecha_ingreso_reg   DATE NOT NULL,
    come_fecha_modifica_reg  DATE,
    FOREIGN KEY (come_usua_id) REFERENCES GDL_Usuario(usua_id),
    FOREIGN KEY (come_publ_id) REFERENCES GDL_Publicacion(publ_id)

);

/**
  Se crea la tabla GDL_Reaccion
 */
CREATE TABLE GDL_Reaccion (
    reac_id                  BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    reac_nombre              VARCHAR(200),
    reac_icono               VARCHAR(200),
    reac_fecha_ingreso_reg   DATE NOT NULL,
    reac_fecha_modifica_reg  DATE

);


/**
  Se crea la tabla GDL_Reaccion_Publicacion
 */
CREATE TABLE GDL_Reaccion_Publicacion (
    repu_id                  BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    repu_cantidad            BIGINT,
    repu_reac_id             BIGINT UNSIGNED,
    repu_publ_id             BIGINT UNSIGNED,
    repu_fecha_ingreso_reg   DATE NOT NULL,
    repu_fecha_modifica_reg  DATE,
    FOREIGN KEY (repu_reac_id) REFERENCES GDL_Reaccion(reac_id),
    FOREIGN KEY (repu_publ_id) REFERENCES GDL_Publicacion(publ_id)

);


/**
 Se crea la tabla GDL_Opciones
 */
CREATE TABLE GDL_Opciones (
    op_id                  BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    op_identificacion      VARCHAR(200),
    op_titulo              VARCHAR(200),
    op_accion              VARCHAR(200),
    op_padre               VARCHAR(200),
    op_orden               BIGINT,
    op_fecha_ingreso_reg   DATE NOT NULL,
    op_fecha_modifica_reg  DATE

);



/**
  Se crea la tabla GDL_Opciones_roles
 */
CREATE TABLE GDL_Opciones_Roles (
    opro_id                BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    opro_opcion_id         BIGINT UNSIGNED,
    opro_rol_id            BIGINT UNSIGNED,
    op_fecha_ingreso_reg   DATE NOT NULL,
    op_fecha_modifica_reg  DATE,
    FOREIGN KEY (opro_opcion_id) REFERENCES GDL_Opciones(op_id),
    FOREIGN KEY (opro_rol_id) REFERENCES  GDL_Rol(rol_id)

);

/**
  se crea la tabla eveentos y noticias
 */
CREATE TABLE GDL_EventosNoticias(
    evno_id         BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    evno_titulo     VARCHAR(200),
    evno_categoria  VARCHAR(200),
    evno_tipo        VARCHAR(50),
    evno_descripcion VARCHAR(200),
    evno_imagen      VARCHAR(200),
    evno_fecha       DATE NOT NULL,
    evno_habilitado  VARCHAR(2),
    evno_fecha_ingreso_reg   DATE NOT NULL,
    evno_fecha_modifica_reg  DATE
);

/**
  Se crea la tabla de notificaciones
 */
CREATE TABLE GDL_Notificaciones(
    no_id BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    no_tipo VARCHAR(5),
    no_asunto VARCHAR(100),
    no_mensaje VARCHAR(200),
    no_fecha_ingreso_reg   DATE NOT NULL,
    no_fecha_modifica_reg  DATE
);

/**
  Se crea la tabla de notificaciones usuario
 */

CREATE TABLE GDL_Notificiones_Usuarios(
    nous_id                BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    nous_usuario_id        BIGINT UNSIGNED,
    nous_asunto            VARCHAR(100),
    nous_mensaje           VARCHAR(200),
    nous_estado            INT(2),
    nous_intentos          INT(2),
    nous_email             VARCHAR(200),
    nous_fecha_ingreso_reg   DATE NOT NULL,
    nous_fecha_modifica_reg  DATE,
    FOREIGN KEY (nous_usuario_id) REFERENCES GDL_Usuario(usua_id)

);


/**
  Se crea la tabla de parametros
 */
CREATE TABLE GDL_Parametros(
    pa_id BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    pa_nombre VARCHAR(100),
    pa_descripcion VARCHAR(200),
    pa_valor VARCHAR(200),
    pa_fecha_ingreso_reg   DATE NOT NULL,
    pa_fecha_modifica_reg  DATE

);


/***
  Se crean las tabla para las encuestas
 */

CREATE TABLE GDL_Encuestas(
    en_id BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    en_id_usuario BIGINT UNSIGNED NOT NULL,
    en_titulo VARCHAR(200),
    en_estado VARCHAR(200),
    en_descriptcion VARCHAR (200),
    en_estado TINYINT(1) NOT NULL,
    en_fecha_inicio DATE NOT NULL,
    en_fecha_fin DATE NOT NULL,
    en_fecha_ingreso_reg   DATE NOT NULL,
    en_fecha_modifica_reg  DATE,
    FOREIGN KEY (en_id_usuario) REFERENCES GDL_Usuario(usua_id)

);

/**
  Se crea la tabla de tipo de preguntas
 */

CREATE TABLE GDL_Tipo_Pregunta(
    tppr_id BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    tppr_nombre VARCHAR(200) NOT NULL,
    tppr_descripcion VARCHAR(200) NOT NULL,
    tppr_fecha_ingreso_reg   DATE NOT NULL,
    tppr_fecha_modifica_reg  DATE
);


/**
  Se crea la tabla pregunta
 */
CREATE TABLE GDL_Preguntas(
    pr_id BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    pr_encuesta_id BIGINT UNSIGNED NOT NULL,
    pr_titulo VARCHAR(200),
    pr_tipo_pregunta_id BIGINT UNSIGNED NOT NULL,
    pr_fecha_ingreso_reg   DATE NOT NULL,
    pr_fecha_modifica_reg  DATE,
    FOREIGN KEY (pr_encuesta_id) REFERENCES GDL_Encuestas(en_id),
    FOREIGN KEY (pr_tipo_pregunta_id) REFERENCES GDL_Tipo_Pregunta(tppr_id)

);


/**
  Se crea la tabla opciones encuestas
 */

CREATE TABLE GDL_Opciones_Encuestas(
    openc_id BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    openc_pregunta_id BIGINT UNSIGNED NOT NULL,
    openc_valor VARCHAR (50),
    openc_fecha_ingreso_reg   DATE NOT NULL,
    openc_fecha_modifica_reg  DATE,
    FOREIGN KEY (openc_pregunta_id) REFERENCES GDL_Preguntas(pr_id)
);


/**
  Se crea la tabla resultados
 */
CREATE TABLE GDL_Resultados(
    re_id BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    re_openc_id BIGINT UNSIGNED NOT NULL,
    re_fecha_ingreso_reg   DATE NOT NULL,
    re_fecha_modifica_reg  DATE,
    FOREIGN KEY (re_openc_id) REFERENCES GDL_Opciones_Encuestas(openc_id)
);


/**
  Se insertan datos base
 */


INSERT INTO GDL_Tipo_Pregunta (tppr_id, tppr_nombre, tppr_descripcion) VALUES
(1, 'Selección múltiple', 'Se podrá escoger solo una opción\r\nelemento input type radio'),
(2, 'Desplegable', 'Se podrá escoger una opción\r\nElemento select y option'),
(3, 'Casilla de verificación', 'Se podrá escoger más de una opción\r\ninput type checkbox'),
(4, 'Texto', 'Se almacenara la respuesta');
