package com.gradulab.administrador.dto;

import com.gradulab.administrador.entity.Graduado;

import java.util.Date;

public class InfoAcademicaDTO extends DTO{

    private Long id;

    private String nombre;

    private String tipo;

    private String tituloObt;

    private String institucion;

    private Date fecha;

    private Graduado graduado;

    private Date fechaModificaReg;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTituloObt() {
        return tituloObt;
    }

    public void setTituloObt(String tituloObt) {
        this.tituloObt = tituloObt;
    }

    public String getInstitucion() {
        return institucion;
    }

    public void setInstitucion(String institucion) {
        this.institucion = institucion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Graduado getGraduado() {
        return graduado;
    }

    public void setGraduado(Graduado graduado) {
        this.graduado = graduado;
    }

    public Date getFechaModificaReg() {
        return fechaModificaReg;
    }

    public void setFechaModificaReg(Date fechaModificaReg) {
        this.fechaModificaReg = fechaModificaReg;
    }
}
