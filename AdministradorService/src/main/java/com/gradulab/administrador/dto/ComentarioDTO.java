package com.gradulab.administrador.dto;


import com.gradulab.administrador.entity.Publicacion;
import com.gradulab.administrador.entity.Usuario;

public class ComentarioDTO extends DTO{

    private Long id;

    private String descripcion;

    private Publicacion publicacion;

    private Usuario usuario;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Publicacion getPublicacion() {
        return publicacion;
    }

    public void setPublicacion(Publicacion publicacion) {
        this.publicacion = publicacion;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }
}
