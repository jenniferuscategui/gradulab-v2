package com.gradulab.administrador.dto;


import java.util.Date;

public class PublicacionDTO extends DTO{

    private Long id;

    private String contenido;

    private Date fechaPu;

    private String multimedia;

    private UsuarioDTO usuario;

    private String enlaceVideo;

    private Long cantidadMegusta;

    private Long catidadMencanta;

    private Long cantidadComentario;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public String getMultimedia() {
        return multimedia;
    }

    public void setMultimedia(String multimedia) {
        this.multimedia = multimedia;
    }

    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public Date getFechaPu() {
        return fechaPu;
    }

    public void setFechaPu(Date fechaPu) {
        this.fechaPu = fechaPu;
    }

    public String getEnlaceVideo() {
        return enlaceVideo;
    }

    public void setEnlaceVideo(String enlaceVideo) {
        this.enlaceVideo = enlaceVideo;
    }

    public Long getCantidadMegusta() {
        return cantidadMegusta;
    }

    public void setCantidadMegusta(Long cantidadMegusta) {
        this.cantidadMegusta = cantidadMegusta;
    }

    public Long getCatidadMencanta() {
        return catidadMencanta;
    }

    public void setCatidadMencanta(Long catidadMencanta) {
        this.catidadMencanta = catidadMencanta;
    }

    public Long getCantidadComentario() {
        return cantidadComentario;
    }

    public void setCantidadComentario(Long cantidadComentario) {
        this.cantidadComentario = cantidadComentario;
    }
}
