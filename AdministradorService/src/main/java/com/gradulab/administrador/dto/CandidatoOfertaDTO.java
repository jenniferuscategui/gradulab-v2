package com.gradulab.administrador.dto;

public class CandidatoOfertaDTO extends DTO{

    private Long id;

    private GraduadoDTO graduadoDto;

    private OfertaLaboralDTO ofertaLaboralDto;


    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public GraduadoDTO getGraduadoDto() {
        return graduadoDto;
    }

    public void setGraduadoDto(GraduadoDTO graduadoDto) {
        this.graduadoDto = graduadoDto;
    }

    public OfertaLaboralDTO getOfertaLaboralDto() {
        return ofertaLaboralDto;
    }

    public void setOfertaLaboralDto(OfertaLaboralDTO ofertaLaboralDto) {
        this.ofertaLaboralDto = ofertaLaboralDto;
    }
}
