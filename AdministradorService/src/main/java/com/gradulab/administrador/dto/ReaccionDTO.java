package com.gradulab.administrador.dto;

import java.util.Date;

public class ReaccionDTO extends DTO{

    private Long id;

    private Long repuCantidad;

    private String reacNombre;

    private String reacIcono;

    private Date reacFechaModificaReg;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Long getRepuCantidad() {
        return repuCantidad;
    }

    public void setRepuCantidad(Long repuCantidad) {
        this.repuCantidad = repuCantidad;
    }

    public String getReacNombre() {
        return reacNombre;
    }

    public void setReacNombre(String reacNombre) {
        this.reacNombre = reacNombre;
    }

    public String getReacIcono() {
        return reacIcono;
    }

    public void setReacIcono(String reacIcono) {
        this.reacIcono = reacIcono;
    }

    public Date getReacFechaModificaReg() {
        return reacFechaModificaReg;
    }

    public void setReacFechaModificaReg(Date reacFechaModificaReg) {
        this.reacFechaModificaReg = reacFechaModificaReg;
    }
}
