package com.gradulab.administrador.dto;

import com.gradulab.administrador.entity.Graduado;
import com.gradulab.administrador.entity.TipoRed;

import java.util.Date;

public class EnlaceRedesDTO extends DTO{

    private Long id;

    private String nombre;

    private TipoRed tipoRed;

    private Graduado graduado;

    private Date fechaModificaReg;


    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public TipoRed getTipoRed() {
        return tipoRed;
    }

    public void setTipoRed(TipoRed tipoRed) {
        this.tipoRed = tipoRed;
    }

    public Graduado getGraduado() {
        return graduado;
    }

    public void setGraduado(Graduado graduado) {
        this.graduado = graduado;
    }

    public Date getFechaModificaReg() {
        return fechaModificaReg;
    }

    public void setFechaModificaReg(Date fechaModificaReg) {
        this.fechaModificaReg = fechaModificaReg;
    }
}
