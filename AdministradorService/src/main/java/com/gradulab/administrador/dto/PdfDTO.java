package com.gradulab.administrador.dto;

import org.jfree.chart.JFreeChart;

import java.util.ArrayList;

public class PdfDTO {

    private String titulo;
    private ArrayList<ValorEstadisticoDTO> data;
    private JFreeChart graficas;

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public ArrayList<ValorEstadisticoDTO> getData() {
        return data;
    }

    public void setData(ArrayList<ValorEstadisticoDTO> data) {
        this.data = data;
    }

    public JFreeChart getGraficas() {
        return graficas;
    }

    public void setGraficas(JFreeChart graficas) {
        this.graficas = graficas;
    }
}
