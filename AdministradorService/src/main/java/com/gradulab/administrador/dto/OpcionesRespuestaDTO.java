package com.gradulab.administrador.dto;

public class OpcionesRespuestaDTO {

    private long idOpcion;
    private String valor;
    private long idPregunta;

    public long getIdOpcion() {
        return idOpcion;
    }

    public void setIdOpcion(long idOpcion) {
        this.idOpcion = idOpcion;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public long getIdPregunta() {
        return idPregunta;
    }

    public void setIdPregunta(long idPregunta) {
        this.idPregunta = idPregunta;
    }
}
