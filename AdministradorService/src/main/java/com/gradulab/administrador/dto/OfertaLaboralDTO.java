package com.gradulab.administrador.dto;

public class OfertaLaboralDTO extends DTO{

    private Long id;

    private String estado;

    private String descripcion;

    private String experiencia;

    private String salario;

    private String tipoContrato;

    private String conocimiento;

    private EmpresaDTO empresaDto;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getExperiencia() {
        return experiencia;
    }

    public void setExperiencia(String experiencia) {
        this.experiencia = experiencia;
    }

    public String getSalario() {
        return salario;
    }

    public void setSalario(String salario) {
        this.salario = salario;
    }

    public String getTipoContrato() {
        return tipoContrato;
    }

    public void setTipoContrato(String tipoContrato) {
        this.tipoContrato = tipoContrato;
    }

    public String getConocimiento() {
        return conocimiento;
    }

    public void setConocimiento(String conocimiento) {
        this.conocimiento = conocimiento;
    }

    public EmpresaDTO getEmpresaDto() {
        return empresaDto;
    }

    public void setEmpresaDto(EmpresaDTO empresaDto) {
        this.empresaDto = empresaDto;
    }
}
