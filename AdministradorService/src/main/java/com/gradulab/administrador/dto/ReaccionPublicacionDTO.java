package com.gradulab.administrador.dto;

import com.gradulab.administrador.entity.Reaccion;

public class ReaccionPublicacionDTO extends DTO{

    private Long reaccion;

    private PublicacionDTO publicacion;

    public Long getReaccion() {
        return reaccion;
    }

    public void setReaccion(Long reaccion) {
        this.reaccion = reaccion;
    }

    public PublicacionDTO getPublicacion() {
        return publicacion;
    }

    public void setPublicacion(PublicacionDTO publicacion) {
        this.publicacion = publicacion;
    }
}
