package com.gradulab.administrador.dto;

public class ResgistroPersonaDTO extends DTO{

    private PersonaDTO personaDto;
    private EmpresaDTO empresaDto;
    private String contrasena;
    private String titulo;

    public PersonaDTO getPersonaDto() {
        return personaDto;
    }

    public void setPersonaDto(PersonaDTO personaDto) {
        this.personaDto = personaDto;
    }

    public EmpresaDTO getEmpresaDto() {
        return empresaDto;
    }

    public void setEmpresaDto(EmpresaDTO empresaDto) {
        this.empresaDto = empresaDto;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
}
