package com.gradulab.administrador.dto;

import java.util.Date;

public class DatosReporteDTO {

    private String codigoGraduado;
    private String modalidad;
    private int edad;
    private String Sexo;
    private Date fechaGrado;
    private boolean estaTrabajando;

    public String getcodigoGraduado() {
        return codigoGraduado;
    }

    public void setCodigoGraduado(String codigo) {
        this.codigoGraduado = codigo;
    }

    public String getModalidad() {
        return modalidad;
    }

    public void setModalidad(String modalidad) {
        this.modalidad = modalidad;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getSexo() {
        return Sexo;
    }

    public void setSexo(String sexo) {
        Sexo = sexo;
    }

    public Date getFechaGrado() {
        return fechaGrado;
    }

    public void setFechaGrado(Date fechaGrado) {
        this.fechaGrado = fechaGrado;
    }

    public boolean isEstaTrabajando() {
        return estaTrabajando;
    }

    public void setEstaTrabajando(boolean estaTrabajando) {
        this.estaTrabajando = estaTrabajando;
    }
}
