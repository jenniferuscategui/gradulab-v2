package com.gradulab.administrador.entity;

import com.gradulab.administrador.dto.InfoAcademicaDTO;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "GDL_Info_academica")
public class InfoAcademica extends Model<InfoAcademicaDTO> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "inac_id")
    private Long id;

    @Column(name = "inac_nombre")
    private String nombre;

    @Column(name = "inac_tipo")
    private String tipo;

    @Column(name = "inac_titulo_obt")
    private String tituloObt;


    @Column(name = "inac_institucion")
    private String institucion;

    @Column(name = "inac_fecha")
    private Date fecha;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "grad_id")
    private Graduado graduado;

    @Temporal(TemporalType.DATE)
    @Column(name = "inac_fecha_ingreso_reg")
    private Date fechaIngresoReg;

    @Column(name = "inac_fecha_modifica_reg")
    private Date fechaModificaReg;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTituloObt() {
        return tituloObt;
    }

    public void setTituloObt(String tituloObt) {
        this.tituloObt = tituloObt;
    }

    public String getInstitucion() {
        return institucion;
    }

    public void setInstitucion(String institucion) {
        this.institucion = institucion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Graduado getGraduado() {
        return graduado;
    }

    public void setGraduado(Graduado graduado) {
        this.graduado = graduado;
    }

    public Date getFechaIngresoReg() {
        return fechaIngresoReg;
    }

    public void setFechaIngresoReg(Date fechaIngresoReg) {
        this.fechaIngresoReg = fechaIngresoReg;
    }

    public Date getFechaModificaReg() {
        return fechaModificaReg;
    }

    public void setFechaModificaReg(Date fechaModificaReg) {
        this.fechaModificaReg = fechaModificaReg;
    }

    @Override
    public Class<InfoAcademicaDTO> getDtoClass() {
        return null;
    }

    @Override
    public InfoAcademicaDTO getDto() {
        return super.getDto();
    }
}
