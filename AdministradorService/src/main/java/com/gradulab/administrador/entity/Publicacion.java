package com.gradulab.administrador.entity;

import com.gradulab.administrador.dto.PublicacionDTO;
import com.gradulab.administrador.dto.UsuarioDTO;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table( name = "GDL_Publicacion")
public class Publicacion extends Model<PublicacionDTO>{

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column(name = "publ_id")
    private Long id;

    @Column(name = "publ_contenido")
    private String contenido;

    @Column(name = "publ_multimedia")
    private String multimedia;

    @Column(name = "publ_fecha_pu")
    private Date fechaPu;

    @Column(name = "publ_fecha_ingreso_reg")
    private Date fechaIngresoReg;

    @Column(name = "publ_fecha_modifica_reg")
    private Date fechaModificareg;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "publ_usua_id")
    private Usuario usuario;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public String getMultimedia() {
        return multimedia;
    }

    public void setMultimedia(String multimedia) {
        this.multimedia = multimedia;
    }

    public Date getFechaPu() {
        return fechaPu;
    }

    public void setFechaPu(Date fechaPu) {
        this.fechaPu = fechaPu;
    }

    public Date getFechaIngresoReg() {
        return fechaIngresoReg;
    }

    public void setFechaIngresoReg(Date fechaIngresoReg) {
        this.fechaIngresoReg = fechaIngresoReg;
    }

    public Date getFechaModificareg() {
        return fechaModificareg;
    }

    public void setFechaModificareg(Date fechaModificareg) {
        this.fechaModificareg = fechaModificareg;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @Override
    public PublicacionDTO getDto() {
        PublicacionDTO publicacionDTO = super.getDto();
        if (usuario != null){
            publicacionDTO.setUsuario(this.usuario.getDto());
        }
        return publicacionDTO;
    }

    @Override
    public Class<PublicacionDTO> getDtoClass() {
        return PublicacionDTO.class;
    }
}
