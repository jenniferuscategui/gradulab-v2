package com.gradulab.administrador.entity;

import com.gradulab.administrador.dto.CandidatoOfertaDTO;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

public class CandidatoOferta extends Model<CandidatoOfertaDTO>{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cand_id")
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "cand_grad_id")
    private Graduado graduado;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "cand_ofla_id")
    private OfertaLaboral ofertaLaboral;

    @Column(name = "cont_fecha_ingreso_reg")
    @Temporal(TemporalType.DATE)
    private Date fechaIngresoRegistro;

    @Column(name = "cont_fecha_modifica_reg")
    private Date fechaModificaRegistro;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Graduado getGraduado() {
        return graduado;
    }

    public void setGraduado(Graduado graduado) {
        this.graduado = graduado;
    }

    public Date getFechaIngresoRegistro() {
        return fechaIngresoRegistro;
    }

    public void setFechaIngresoRegistro(Date fechaIngresoRegistro) {
        this.fechaIngresoRegistro = fechaIngresoRegistro;
    }

    public Date getFechaModificaRegistro() {
        return fechaModificaRegistro;
    }

    public void setFechaModificaRegistro(Date fechaModificaRegistro) {
        this.fechaModificaRegistro = fechaModificaRegistro;
    }

    @Override
    public CandidatoOfertaDTO getDto(){
        CandidatoOfertaDTO candidatoOfertaDto = super.getDto();
        if (ofertaLaboral !=null){
            candidatoOfertaDto.setOfertaLaboralDto(this.ofertaLaboral.getDto());
        }
        if (graduado !=null){
            candidatoOfertaDto.setGraduadoDto(this.graduado.getDto());
        }
        return candidatoOfertaDto;
    }

    @Override
    public Class<CandidatoOfertaDTO> getDtoClass(){
        return CandidatoOfertaDTO.class;
    }
}
