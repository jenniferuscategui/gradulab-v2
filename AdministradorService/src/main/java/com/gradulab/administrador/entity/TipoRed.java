package com.gradulab.administrador.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "GDL_Tipo_Red")
public class TipoRed {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "tire_id")
    private Long id;

    @Column(name = "tire_nombre")
    private String nombre;

    @Column(name = "tire_icono")
    private String icono;

    @Temporal(TemporalType.DATE)
    @Column(name = "tire_fecha_ingreso_reg")
    private Date fechaIngresoReg;

    @Column(name = "tire_fecha_modifica_reg")
    private Date fechaModificaReg;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIcono() {
        return icono;
    }

    public void setIcono(String icono) {
        this.icono = icono;
    }

    public Date getFechaIngresoReg() {
        return fechaIngresoReg;
    }

    public void setFechaIngresoReg(Date fechaIngresoReg) {
        this.fechaIngresoReg = fechaIngresoReg;
    }

    public Date getFechaModificaReg() {
        return fechaModificaReg;
    }

    public void setFechaModificaReg(Date fechaModificaReg) {
        this.fechaModificaReg = fechaModificaReg;
    }
}
