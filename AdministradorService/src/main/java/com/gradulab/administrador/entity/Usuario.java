package com.gradulab.administrador.entity;

import com.gradulab.administrador.dto.UsuarioDTO;
import com.gradulab.administrador.general.utils.BooleanToStringConverter;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "GDL_Usuario")
public class Usuario extends Model<UsuarioDTO> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "usua_id")
    private Long id;

    @Column(name = "usua_login")
    private String login;

    @Column(name = "usua_contraseña")
    private String contrasena;

    @Convert(converter = BooleanToStringConverter.class)
    @Column(name = "usua_habilitado")
    private Boolean habilitado;

    @Column(name = "usua_fecha_ingreso")
    private Date fechaIngreso;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "usua_pers_id")
    private Persona persona;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "GDL_Roles_Usuarios", joinColumns = @JoinColumn(name = "rous_usuario_id"), inverseJoinColumns = @JoinColumn(name = "rous_rol_id"))
    private List<Rol> roles;

    @Column(name = "usua_fecha_ingreso_reg")
    @Temporal(TemporalType.DATE)
    private Date fechaIngresoReg;

    @Column(name = "usua_fecha_modifica_reg")
    private Date fechaModificaReg;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public Boolean getHabilitado() {
        return habilitado;
    }

    public void setHabilitado(Boolean habilitado) {
        this.habilitado = habilitado;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public Date getFechaIngresoReg() {
        return fechaIngresoReg;
    }

    public void setFechaIngresoReg(Date fechaIngresoReg) {
        this.fechaIngresoReg = fechaIngresoReg;
    }

    public Date getFechaModificaReg() {
        return fechaModificaReg;
    }

    public void setFechaModificaReg(Date fechaModificaReg) {
        this.fechaModificaReg = fechaModificaReg;
    }

    public List<Rol> getRoles() {
        return roles;
    }

    public void setRoles(List<Rol> roles) {
        this.roles = roles;
    }

    @Override
    public UsuarioDTO getDto(){
        UsuarioDTO usuarioDto = super.getDto();
        if (persona != null){
            usuarioDto.setPersonaDto(this.persona.getDto());
        }
        return usuarioDto;
    }

    @Override
    public Class<UsuarioDTO> getDtoClass(){
        return UsuarioDTO.class;
    }

}
