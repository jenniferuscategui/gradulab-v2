package com.gradulab.administrador.entity;

import com.gradulab.administrador.dto.EmpresaDTO;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "GDL_Empresa")
public class Empresa extends Model<EmpresaDTO>{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "empr_id")
    private Long id;

    @Column(name = "empr_nombre")
    private String nombre;

    @Column(name = "empr_nit")
    private String nit;

    @Column(name = "empr_direccion")
    private String direccion;

    @Column(name = "empr_telefono")
    private String telefono;

    @Column(name = "empr_area")
    private String area;

    @Column(name = "empr_email")
    private String email;

    @Column(name = "empr_fecha_ingreso_reg")
    private Date fechaIngresoRegistro;

    @Column(name = "empr_fecha_modifica_reg")
    private Date fechaModificaRegistro;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getFechaIngresoRegistro() {
        return fechaIngresoRegistro;
    }

    public void setFechaIngresoRegistro(Date fechaIngresoRegistro) {
        this.fechaIngresoRegistro = fechaIngresoRegistro;
    }

    public Date getFechaModificaRegistro() {
        return fechaModificaRegistro;
    }

    public void setFechaModificaRegistro(Date fechaModificaRegistro) {
        this.fechaModificaRegistro = fechaModificaRegistro;
    }

    @Override
    public EmpresaDTO getDto(){
        return super.getDto();
    }

    @Override
    public Class<EmpresaDTO> getDtoClass(){
        return EmpresaDTO.class;
    }
}
