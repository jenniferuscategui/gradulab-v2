package com.gradulab.administrador.entity;


import com.gradulab.administrador.dto.PersonaDTO;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@Entity
@Table(name = "GDL_Persona")
public class Persona extends Model<PersonaDTO> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pers_id")
    private Long id;

    @Column(name = "pers_tipo_docuemnto")
    private String tipoDocumento;

    @Column(name = "pers_documento")
    private String documento;

    @Column(name = "pers_apellidos")
    private String apellidos;

    @Column(name = "pers_nombre")
    private String nombre;

    @Column(name = "pers_sexo")
    private String sexo;

    @Column(name = "pers_fecha_nacimiento")
    private String fechaNacimiento;

    @Column(name = "pers_direccion")
    private String direccion;

    @Column(name = "pers_lugar_nacimiento")
    private String lugarNacimiento;

    @Column(name = "pers_telefono")
    private String telefono;

    @Column(name = "pers_ccelular")
    private String celular;

    @Column(name = "pers_correo_personal")
    private String correoPersonal;

    @Column(name = "pers_correo_corporativo")
    private String correoCorporativo;

    @Column(name = "pers_fotografia")
    private String fotografia;

    @Temporal(TemporalType.DATE)
    @Column(name = "pers_fecha_ingreso_reg")
    private Date fechaIngresoReg;

    @Column(name = "pers_fecha_modifica_reg")
    private Date fechaModificaReg;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getLugarNacimiento() {
        return lugarNacimiento;
    }

    public void setLugarNacimiento(String lugarNacimiento) {
        this.lugarNacimiento = lugarNacimiento;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getCorreoPersonal() {
        return correoPersonal;
    }

    public void setCorreoPersonal(String correoPersonal) {
        this.correoPersonal = correoPersonal;
    }

    public String getCorreoCorporativo() {
        return correoCorporativo;
    }

    public void setCorreoCorporativo(String correoCorporativo) {
        this.correoCorporativo = correoCorporativo;
    }

    public String getFotografia() {
        return fotografia;
    }

    public void setFotografia(String fotografia) {
        this.fotografia = fotografia;
    }

    public Date getFechaIngresoReg() {
        return fechaIngresoReg;
    }

    public void setFechaIngresoReg(Date fechaIngresoReg) {
        this.fechaIngresoReg = fechaIngresoReg;
    }

    public Date getFechaModificaReg() {
        return fechaModificaReg;
    }

    public void setFechaModificaReg(Date fechaModificaReg) {
        this.fechaModificaReg = fechaModificaReg;
    }

    @Override
    public  PersonaDTO getDto(){
        return super.getDto();
    }

    @Override
    public Class<PersonaDTO> getDtoClass(){
        return PersonaDTO.class;
    }
}


