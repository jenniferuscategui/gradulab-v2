package com.gradulab.administrador.entity;

import com.gradulab.administrador.dto.EnlaceRedesDTO;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "GDL_Enlace_Redes")
public class EnlaceRedes extends Model<EnlaceRedesDTO>{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "enre_id")
    private Long id;

    @Column(name = "enre_nombre")
    private String nombre;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "enre_tire_id")
    private TipoRed tipoRed;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "grad_id")
    private Graduado graduado;

    @Temporal(TemporalType.DATE)
    @Column(name = "enre_fecha_ingreso_reg")
    private Date fechaIngresoReg;

    @Column(name = "enre_fecha_modifica_reg")
    private Date fechaModificaReg;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public TipoRed getTipoRed() {
        return tipoRed;
    }

    public void setTipoRed(TipoRed tipoRed) {
        this.tipoRed = tipoRed;
    }

    public Graduado getGraduado() {
        return graduado;
    }

    public void setGraduado(Graduado graduado) {
        this.graduado = graduado;
    }

    public Date getFechaIngresoReg() {
        return fechaIngresoReg;
    }

    public void setFechaIngresoReg(Date fechaIngresoReg) {
        this.fechaIngresoReg = fechaIngresoReg;
    }

    public Date getFechaModificaReg() {
        return fechaModificaReg;
    }

    public void setFechaModificaReg(Date fechaModificaReg) {
        this.fechaModificaReg = fechaModificaReg;
    }


    @Override
    public Class<EnlaceRedesDTO> getDtoClass() {
        return EnlaceRedesDTO.class;
    }

    @Override
    public EnlaceRedesDTO getDto() {
        return super.getDto();
    }
}
