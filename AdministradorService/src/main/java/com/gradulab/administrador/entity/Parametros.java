package com.gradulab.administrador.entity;

import com.gradulab.administrador.dto.ParametroDTO;
import com.gradulab.administrador.dto.PersonaDTO;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@Entity
@Table(name = "GDL_Parametros")
public class Parametros extends Model<ParametroDTO> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pers_id")
    private Long id;

    @Column(name = "pa_nombre")
    private String nombre;

    @Column(name = "pa_descripcion")
    private String descripcion;

    @Column(name = "pa_valor")
    private String valor;

    @Column(name = "pa_codigo")
    private String codigo;

    @Temporal(TemporalType.DATE)
    @Column(name = "pa_fecha_ingreso_reg")
    private Date fechaIngresoReg;

    @Column(name = "pa_fecha_modifica_reg")
    private Date fechaModificaReg;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public Date getFechaIngresoReg() {
        return fechaIngresoReg;
    }

    public void setFechaIngresoReg(Date fechaIngresoReg) {
        this.fechaIngresoReg = fechaIngresoReg;
    }

    public Date getFechaModificaReg() {
        return fechaModificaReg;
    }

    public void setFechaModificaReg(Date fechaModificaReg) {
        this.fechaModificaReg = fechaModificaReg;
    }

    @Override
    public ParametroDTO getDto(){
        return super.getDto();
    }

    @Override
    public Class<ParametroDTO> getDtoClass(){
        return ParametroDTO.class;
    }
}
