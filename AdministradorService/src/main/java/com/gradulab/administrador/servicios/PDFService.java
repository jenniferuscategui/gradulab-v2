package com.gradulab.administrador.servicios;

import com.gradulab.administrador.dto.PdfDTO;
import com.gradulab.administrador.dto.ValorEstadisticoDTO;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.log.Logger;
import com.itextpdf.text.log.LoggerFactory;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;


@Service
public class PDFService {

    private static final Logger logger = LoggerFactory.getLogger(PDFService.class);

    public ByteArrayInputStream crearPDF(PdfDTO pdfDTO) {
        Document document = new Document();
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {

            PdfPTable table = new PdfPTable(3);
            table.setWidthPercentage(60);
            table.setWidths(new int[]{1, 3, 3});

            PdfPCell hcell;
            hcell = new PdfPCell(new Phrase("Id"));
            hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hcell);

            hcell = new PdfPCell(new Phrase("Valor"));
            hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hcell);

            hcell = new PdfPCell(new Phrase("Cantidad"));
            hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hcell);
            ArrayList<ValorEstadisticoDTO> lista = pdfDTO.getData();
            int contador = 0;
            for (ValorEstadisticoDTO dato : lista) {
                contador++;
                PdfPCell cell;

                cell = new PdfPCell(new Phrase(contador));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(dato.getCategoria()));
                cell.setPaddingLeft(5);
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(String.valueOf(dato.getCantidad())));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell.setPaddingRight(5);
                table.addCell(cell);
            }
            BufferedImage buffer = pdfDTO.getGraficas().createBufferedImage(400, 300);
            ByteArrayOutputStream img_bytes = new ByteArrayOutputStream();
            ImageIO.write( buffer, "png",img_bytes );
            Image imagen= Image.getInstance(img_bytes.toByteArray());

            imagen.setAbsolutePosition(45, 150);
            Paragraph titulo = new Paragraph(pdfDTO.getTitulo());

            PdfWriter.getInstance(document, out);
            document.open();
            document.add(titulo);
            document.add(imagen);
            document.add(table);
            document.close();

        } catch (DocumentException ex) {
            logger.error("Error occurred: {0}", ex);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ByteArrayInputStream(out.toByteArray());
    }

}
