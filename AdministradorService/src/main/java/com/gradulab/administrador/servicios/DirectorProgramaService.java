package com.gradulab.administrador.servicios;

import com.gradulab.administrador.dto.ContactoEmpresaDTO;
import com.gradulab.administrador.dto.ParametroDTO;
import com.gradulab.administrador.dto.PersonaDTO;
import com.gradulab.administrador.dto.UsuarioDTO;
import com.gradulab.administrador.entity.Parametros;
import com.gradulab.administrador.entity.Persona;
import com.gradulab.administrador.entity.Usuario;
import com.gradulab.administrador.general.constants.Constantes;
import com.gradulab.administrador.general.enums.EnumEstadoSolicitud;
import com.gradulab.administrador.general.enums.EnumTipoNotificacion;
import com.gradulab.administrador.general.utils.DTOList;
import com.gradulab.administrador.general.utils.DateUtils;
import com.gradulab.administrador.repository.*;
import com.gradulab.administrador.dto.GraduadoDTO;
import com.gradulab.administrador.entity.Graduado;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class DirectorProgramaService {


    @Autowired
    private EmpresaRespository empresaRespository;

    @Autowired
    private ConactoEmpresaRepository conactoEmpresaRepository;

    @Autowired
    private UsusarioRepository ususarioRepository;

    @Autowired
    private GraduadoRepository graduadoRepository;

    @Autowired
    private PersonaRepository personaRepository;

    @Autowired
    private DTOList dtoList;

    @Autowired
    private DateUtils dateUtils;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    NotificacionesService notificacionesService;

    @Autowired
    ParametrosRepository parametrosRepository;


    private static final Logger logger = LoggerFactory.getLogger(DirectorProgramaService.class);

    private static final String SORT_BY_NAME = "nombre";


    /**
     * busca busca los contacto empresa que esten registrados pero que aun
     * no tengan usuario
     *
     * @return list<ContatcoEmpresaDTO> lista de contacto empresa que no tengan usuario
     */
    public List<ContactoEmpresaDTO> buscarSolicitudesEmpresa() {
        logger.info("Se obtiene el listado de las solicitudes de los registros de empresa");
        return dtoList.getDtoList(conactoEmpresaRepository.findAll()
                .stream()
                .filter(contactoEmpresa -> ususarioRepository.findByPersona(contactoEmpresa.getPersona()) == null)
                .collect(Collectors.toList()));

    }

    /**
     * busca las solicitudes de las graduados que esten registrados pero que aun
     * no tengan usuario
     *
     * @return list<GraduadoDTO> lista de solicitudes de graduados que no tengan usuario
     */
    public List<GraduadoDTO> buscarSolicitudesGraduados() {
        List<Graduado> graduadoPage = graduadoRepository.findAll();
        return dtoList.getDtoList(graduadoPage);
    }

    public PersonaDTO prosecarSolicitudGraduado(String documentoPersona, Boolean confirmacion) {

        Optional<Persona> persona = personaRepository.findByDocumento(documentoPersona);

        if (persona.isPresent()) {
            if (Boolean.TRUE.equals(confirmacion)) {
                Usuario usuario = ususarioRepository.findByPersona(persona.get());
                usuario.setHabilitado(true);
                return ususarioRepository.save(usuario).getPersona().getDto();
            } else {
                this.borrarGraduado(persona.get());
                this.borrarPersona(persona.get());
            }
        }
        return null;
    }

    public List<PersonaDTO> buscarSolicituGraduado(){
        List<Usuario> usuarios = ususarioRepository.findByHabilitado(Constantes.ST_N);

        List<Persona> graduados = new ArrayList<>();
        for ( Usuario usuario : usuarios
             ) {
           Optional <Graduado> graduado = graduadoRepository.findByPersona(usuario.getPersona());
            graduado.ifPresent(value -> graduados.add(value.getPersona()));

        }
        return dtoList.getDtoList(graduados);
    }

    private Usuario crearUsuario(Persona persona) {
        Usuario usuario = new Usuario();
        usuario.setPersona(persona);
        usuario.setLogin(persona.getDocumento());
        usuario.setContrasena(passwordEncoder.encode(persona.getDocumento()));
        usuario.setHabilitado(Constantes.ST_S);
        usuario.setFechaModificaReg(dateUtils.obtenerFechaActual());
        usuario = ususarioRepository.save(usuario);
        notificacionesService.contruirNotificacion(usuario, EnumTipoNotificacion.ACEPTA_SOLICITUD_REGISTRO);
        return usuario;
    }

    private void borrarPersona(Persona persona) {
        personaRepository.delete(persona);
    }

    private void borrarGraduado(Persona persona) {
        Optional<Graduado> graduado = graduadoRepository.findByPersona(persona);
        graduado.ifPresent(value -> graduadoRepository.delete(value));
    }

    public List<ParametroDTO> listarSolicitudCambioCorreo(){
        return dtoList.getDtoList(parametrosRepository.findByDescripcion(EnumEstadoSolicitud.SOLICITUD_PENDIENTE));
    }

    public ParametroDTO cambiarCorreo(ParametroDTO parametroDTO){
        if (parametroDTO.getConfirmacion()){
            Optional<Persona> persona = personaRepository.findByDocumento(parametroDTO.getCodigo());
            if (persona.isPresent()){
                persona.get().setCorreoPersonal(parametroDTO.getValor());
                personaRepository.save(persona.get());

            }else {
                Optional<Parametros> para = parametrosRepository.findById(parametroDTO.getId());
                para.ifPresent(parametros -> parametrosRepository.delete(parametros));
            }

        }
        return parametroDTO;
    }
}
