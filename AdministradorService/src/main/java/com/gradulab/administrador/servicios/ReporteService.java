package com.gradulab.administrador.servicios;

import com.gradulab.administrador.dto.DatosRegistroDTO;
import com.gradulab.administrador.dto.DatosReporteDTO;
import com.gradulab.administrador.dto.PdfDTO;
import com.gradulab.administrador.dto.ValorEstadisticoDTO;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

@Service
public class ReporteService {

    @Autowired
    private ExcelService excelService;

    @Autowired
    private GraduadoService graduadoService;

    @Autowired
    private PDFService pdfService;

    public ByteArrayInputStream crearReporteExcel () throws IOException {
        String[] encabezado = {"Id.","Código", "Cédula", "Apellidos", "Nombres", "Celular", "Email"};
        ArrayList <DatosRegistroDTO> listado = graduadoService.listarGraduadosConInformacionContacto();
        return excelService.crearExcel(encabezado, listado);
    }

    // Caracterizacion por sexo
    public ByteArrayInputStream crearReporteCaracterizacionSexo(Date fechaInicio, Date fechaFin) {
        ArrayList<DatosReporteDTO> listadoGraduados = graduadoService.listarGraduadosAno(fechaInicio, fechaFin);
        int cantidadGraduados = listadoGraduados.size();
        PdfDTO pdfDTO = new PdfDTO();
        ArrayList<ValorEstadisticoDTO> clasificacionSexo = new ArrayList();
        clasificacionSexo.add(new ValorEstadisticoDTO("FEMENINO", 0));
        clasificacionSexo.add(new ValorEstadisticoDTO("MASCULINO", 0));
        clasificacionSexo.add(new ValorEstadisticoDTO("NULO", 0));
        JFreeChart graficaSexo = null;
        if (listadoGraduados != null) {
            for (DatosReporteDTO dt: listadoGraduados) {
                //validacion de sexo
                if (dt.getSexo().equalsIgnoreCase("FEMENINO")) {
                    clasificacionSexo.get(0).setCantidad(clasificacionSexo.get(0).getCantidad()+1);
                } else if (dt.getSexo().equalsIgnoreCase("MASCULINO")) {
                    clasificacionSexo.get(1).setCantidad(clasificacionSexo.get(1).getCantidad()+1);
                } else {
                    clasificacionSexo.get(2).setCantidad(clasificacionSexo.get(2).getCantidad()+1);
                }
            }
            graficaSexo = this.generarGrafico(clasificacionSexo, "Grafica clasificacion por sexo");
        }
        pdfDTO.setTitulo("REPORTE DE CARACTERIZACION POR SEXO DE GRADUADOS");
        pdfDTO.setData(clasificacionSexo);
        pdfDTO.setGraficas(graficaSexo);
        return pdfService.crearPDF(pdfDTO);
    }

    public ByteArrayInputStream crearReporteCaracterizacionEdad(Date fechaInicio, Date fechaFin) {
        ArrayList<DatosReporteDTO> listadoGraduados = graduadoService.listarGraduadosAno(fechaInicio, fechaFin);
        int cantidadGraduados = listadoGraduados.size();
        PdfDTO pdfDTO = new PdfDTO();
        ArrayList<ValorEstadisticoDTO> clasificacionEdad = new ArrayList();
        clasificacionEdad.add(new ValorEstadisticoDTO("Menores de 18", 0));
        clasificacionEdad.add(new ValorEstadisticoDTO("[19-25]", 0));
        clasificacionEdad.add(new ValorEstadisticoDTO("[26-30]", 0));
        clasificacionEdad.add(new ValorEstadisticoDTO("[31-35]", 0));
        clasificacionEdad.add(new ValorEstadisticoDTO("[36-40]", 0));
        clasificacionEdad.add(new ValorEstadisticoDTO("[41-45]", 0));
        clasificacionEdad.add(new ValorEstadisticoDTO("[46-50]", 0));
        clasificacionEdad.add(new ValorEstadisticoDTO("Mayores de 50", 0));
        clasificacionEdad.add(new ValorEstadisticoDTO("NULO", 0));
        JFreeChart graficaEdad = null;
        if (listadoGraduados != null) {
            for (DatosReporteDTO dt: listadoGraduados) {
                //validacion rango de edad
                if (dt.getEdad()<18) {
                    clasificacionEdad.get(0).setCantidad(clasificacionEdad.get(0).getCantidad()+1);
                } else if (dt.getEdad() >= 18 && dt.getEdad() <= 25){
                    clasificacionEdad.get(1).setCantidad(clasificacionEdad.get(1).getCantidad()+1);
                } else if (dt.getEdad() >= 26 && dt.getEdad() <= 30){
                    clasificacionEdad.get(2).setCantidad(clasificacionEdad.get(2).getCantidad()+1);
                } else if (dt.getEdad() >= 31 && dt.getEdad() <= 35){
                    clasificacionEdad.get(3).setCantidad(clasificacionEdad.get(3).getCantidad()+1);
                } else if (dt.getEdad() >= 36 && dt.getEdad() <= 40){
                    clasificacionEdad.get(4).setCantidad(clasificacionEdad.get(4).getCantidad()+1);
                } else if (dt.getEdad() >= 41 && dt.getEdad() <= 45){
                    clasificacionEdad.get(5).setCantidad(clasificacionEdad.get(5).getCantidad()+1);
                } else if (dt.getEdad() >= 46 && dt.getEdad() <= 50){
                    clasificacionEdad.get(6).setCantidad(clasificacionEdad.get(6).getCantidad()+1);
                }  else if (dt.getEdad() > 50 ){
                    clasificacionEdad.get(7).setCantidad(clasificacionEdad.get(7).getCantidad()+1);
                } else {
                    clasificacionEdad.get(8).setCantidad(clasificacionEdad.get(8).getCantidad()+1);
                }
            }
            graficaEdad = this.generarGrafico(clasificacionEdad, "Grafica clasificacion por edad");
        }
        pdfDTO.setTitulo("REPORTE DE CARACTERIZACION POR EDAD DE GRADUADOS");
        pdfDTO.setData(clasificacionEdad);
        pdfDTO.setGraficas(graficaEdad);
        return pdfService.crearPDF(pdfDTO);
    }

    public ByteArrayInputStream crearReporteCaracterizacionModalidad(Date fechaInicio, Date fechaFin) {
        ArrayList<DatosReporteDTO> listadoGraduados = graduadoService.listarGraduadosAno(fechaInicio, fechaFin);
        int cantidadGraduados = listadoGraduados.size();
        PdfDTO pdfDTO = new PdfDTO();
        ArrayList<ValorEstadisticoDTO> clasificacionModa = new ArrayList();
        clasificacionModa.add(new ValorEstadisticoDTO("CURSO DE PROFUNDIZACION", 0));
        clasificacionModa.add(new ValorEstadisticoDTO("PASANTIA", 0));
        clasificacionModa.add(new ValorEstadisticoDTO("LABOR DE CONSULTORIA", 0));
        clasificacionModa.add(new ValorEstadisticoDTO("TRABAJO SOCIAL", 0));
        clasificacionModa.add(new ValorEstadisticoDTO("PROYECTO DE EXTENSION", 0));
        clasificacionModa.add(new ValorEstadisticoDTO("TRABAJO DE INVESTIGACION", 0));
        clasificacionModa.add(new ValorEstadisticoDTO("MONOGRAFIA", 0));
        clasificacionModa.add(new ValorEstadisticoDTO("PROYECTO DE INVESTIGACION", 0));
        clasificacionModa.add(new ValorEstadisticoDTO("NULO", 0));
        JFreeChart graficaModalidad = null;
        if (listadoGraduados != null) {
            for (DatosReporteDTO dt: listadoGraduados) {
                //Validacion por modalidad de trabajo de grado
                if (dt.getModalidad().equalsIgnoreCase("CURSO DE PROFUNDIZACION")){
                    clasificacionModa.get(0).setCantidad(clasificacionModa.get(0).getCantidad()+1);
                } else if (dt.getModalidad().equalsIgnoreCase("PASANTIA")){
                    clasificacionModa.get(1).setCantidad(clasificacionModa.get(1).getCantidad()+1);
                } else if (dt.getModalidad().equalsIgnoreCase("LABOR DE CONSULTORIA")){
                    clasificacionModa.get(2).setCantidad(clasificacionModa.get(2).getCantidad()+1);
                } else if (dt.getModalidad().equalsIgnoreCase("TRABAJO SOCIAL")){
                    clasificacionModa.get(3).setCantidad(clasificacionModa.get(3).getCantidad()+1);
                } else if (dt.getModalidad().equalsIgnoreCase("PROYECTO DE EXTENSION")){
                    clasificacionModa.get(4).setCantidad(clasificacionModa.get(4).getCantidad()+1);
                } else if (dt.getModalidad().equalsIgnoreCase("TRABAJO DE INVESTIGACION")){
                    clasificacionModa.get(5).setCantidad(clasificacionModa.get(5).getCantidad()+1);
                } else if (dt.getModalidad().equalsIgnoreCase("MONOGRAFIA")){
                    clasificacionModa.get(6).setCantidad(clasificacionModa.get(6).getCantidad()+1);
                } else if (dt.getModalidad().equalsIgnoreCase("PROYECTO DE INVESTIGACION")){
                    clasificacionModa.get(7).setCantidad(clasificacionModa.get(7).getCantidad()+1);
                } else {
                    clasificacionModa.get(8).setCantidad(clasificacionModa.get(8).getCantidad()+1);
                }
            }
            graficaModalidad = this.generarGrafico(clasificacionModa, "Grafica clasificacion por modalidad de trabajo de grado");
        }
        pdfDTO.setTitulo("REPORTE DE CARACTERIZACION POR MODALIDAD DE TRABAJO DE GRADO DE LOS GRADUADOS");
        pdfDTO.setData(clasificacionModa);
        pdfDTO.setGraficas(graficaModalidad);
        return pdfService.crearPDF(pdfDTO);
    }

    /*public ByteArrayInputStream crearReporteCaracterizacion(Date fechaInicio, Date fechaFin) {
        ArrayList<DatosReporteDTO> listadoGraduados = graduadoService.listarGraduadosAno(fechaInicio, fechaFin);
        int cantidadGraduados = listadoGraduados.size();
        PdfDTO pdfDTO = new PdfDTO();
        ArrayList<ValorEstadisticoDTO> clasificacionSexo = new ArrayList();
        ArrayList<ValorEstadisticoDTO> clasificacionEdad = new ArrayList();
        ArrayList<ValorEstadisticoDTO> clasificacionModa = new ArrayList();
        clasificacionSexo.add(new ValorEstadisticoDTO("FEMENINO", 0));
        clasificacionSexo.add(new ValorEstadisticoDTO("MASCULINO", 0));
        clasificacionSexo.add(new ValorEstadisticoDTO("NULO", 0));
        clasificacionEdad.add(new ValorEstadisticoDTO("Menores de 18", 0));
        clasificacionEdad.add(new ValorEstadisticoDTO("[19-25]", 0));
        clasificacionEdad.add(new ValorEstadisticoDTO("[26-30]", 0));
        clasificacionEdad.add(new ValorEstadisticoDTO("[31-35]", 0));
        clasificacionEdad.add(new ValorEstadisticoDTO("[36-40]", 0));
        clasificacionEdad.add(new ValorEstadisticoDTO("[41-45]", 0));
        clasificacionEdad.add(new ValorEstadisticoDTO("[46-50]", 0));
        clasificacionEdad.add(new ValorEstadisticoDTO("Mayores de 50", 0));
        clasificacionEdad.add(new ValorEstadisticoDTO("NULO", 0));
        clasificacionModa.add(new ValorEstadisticoDTO("CURSO DE PROFUNDIZACION", 0));
        clasificacionModa.add(new ValorEstadisticoDTO("PASANTIA", 0));
        clasificacionModa.add(new ValorEstadisticoDTO("LABOR DE CONSULTORIA", 0));
        clasificacionModa.add(new ValorEstadisticoDTO("TRABAJO SOCIAL", 0));
        clasificacionModa.add(new ValorEstadisticoDTO("PROYECTO DE EXTENSION", 0));
        clasificacionModa.add(new ValorEstadisticoDTO("TRABAJO DE INVESTIGACION", 0));
        clasificacionModa.add(new ValorEstadisticoDTO("MONOGRAFIA", 0));
        clasificacionModa.add(new ValorEstadisticoDTO("PROYECTO DE INVESTIGACION", 0));
        clasificacionModa.add(new ValorEstadisticoDTO("NULO", 0));
        JFreeChart graficaSexo = null;
        JFreeChart graficaEdad = null;
        JFreeChart graficaModalidad = null;
        if (listadoGraduados != null) {
            for (DatosReporteDTO dt: listadoGraduados) {
                //validacion de sexo
                if (dt.getSexo().equalsIgnoreCase("FEMENINO")) {
                    clasificacionSexo.get(0).setCantidad(clasificacionSexo.get(0).getCantidad()+1);
                } else if (dt.getSexo().equalsIgnoreCase("MASCULINO")) {
                    clasificacionSexo.get(1).setCantidad(clasificacionSexo.get(1).getCantidad()+1);
                } else {
                    clasificacionSexo.get(2).setCantidad(clasificacionSexo.get(2).getCantidad()+1);
                }
                //validacion rango de edad
                if (dt.getEdad()<18) {
                    clasificacionEdad.get(0).setCantidad(clasificacionEdad.get(0).getCantidad()+1);
                } else if (dt.getEdad() >= 18 && dt.getEdad() <= 25){
                    clasificacionEdad.get(1).setCantidad(clasificacionEdad.get(1).getCantidad()+1);
                } else if (dt.getEdad() >= 26 && dt.getEdad() <= 30){
                    clasificacionEdad.get(2).setCantidad(clasificacionEdad.get(2).getCantidad()+1);
                } else if (dt.getEdad() >= 31 && dt.getEdad() <= 35){
                    clasificacionEdad.get(3).setCantidad(clasificacionEdad.get(3).getCantidad()+1);
                } else if (dt.getEdad() >= 36 && dt.getEdad() <= 40){
                    clasificacionEdad.get(4).setCantidad(clasificacionEdad.get(4).getCantidad()+1);
                } else if (dt.getEdad() >= 41 && dt.getEdad() <= 45){
                    clasificacionEdad.get(5).setCantidad(clasificacionEdad.get(5).getCantidad()+1);
                } else if (dt.getEdad() >= 46 && dt.getEdad() <= 50){
                    clasificacionEdad.get(6).setCantidad(clasificacionEdad.get(6).getCantidad()+1);
                }  else if (dt.getEdad() > 50 ){
                    clasificacionEdad.get(7).setCantidad(clasificacionEdad.get(7).getCantidad()+1);
                } else {
                    clasificacionEdad.get(8).setCantidad(clasificacionEdad.get(8).getCantidad()+1);
                }
                //Validacion por modalidad de trabajo de grado
                if (dt.getModalidad().equalsIgnoreCase("CURSO DE PROFUNDIZACION")){
                    clasificacionModa.get(0).setCantidad(clasificacionModa.get(0).getCantidad()+1);
                } else if (dt.getModalidad().equalsIgnoreCase("PASANTIA")){
                    clasificacionModa.get(1).setCantidad(clasificacionModa.get(1).getCantidad()+1);
                } else if (dt.getModalidad().equalsIgnoreCase("LABOR DE CONSULTORIA")){
                    clasificacionModa.get(2).setCantidad(clasificacionModa.get(2).getCantidad()+1);
                } else if (dt.getModalidad().equalsIgnoreCase("TRABAJO SOCIAL")){
                    clasificacionModa.get(3).setCantidad(clasificacionModa.get(3).getCantidad()+1);
                } else if (dt.getModalidad().equalsIgnoreCase("PROYECTO DE EXTENSION")){
                    clasificacionModa.get(4).setCantidad(clasificacionModa.get(4).getCantidad()+1);
                } else if (dt.getModalidad().equalsIgnoreCase("TRABAJO DE INVESTIGACION")){
                    clasificacionModa.get(5).setCantidad(clasificacionModa.get(5).getCantidad()+1);
                } else if (dt.getModalidad().equalsIgnoreCase("MONOGRAFIA")){
                    clasificacionModa.get(6).setCantidad(clasificacionModa.get(6).getCantidad()+1);
                } else if (dt.getModalidad().equalsIgnoreCase("PROYECTO DE INVESTIGACION")){
                    clasificacionModa.get(7).setCantidad(clasificacionModa.get(7).getCantidad()+1);
                } else {
                    clasificacionModa.get(8).setCantidad(clasificacionModa.get(8).getCantidad()+1);
                }
            }
            graficaEdad = this.generarGrafico(clasificacionEdad, "Grafica clasificacion por edad");
            graficaSexo = this.generarGrafico(clasificacionSexo, "Grafica clasificacion por sexo");
            graficaModalidad = this.generarGrafico(clasificacionModa, "Grafica clasificacion por modalidad de trabajo de grado");
        }
        pdfDTO.setTitulo("REPORTE DE CARACTERIZACION POR SEXO DE GRADUADOS");
        pdfDTO.setData(clasificacionSexo);
        pdfDTO.setGraficas(graficaSexo);
        return pdfService.crearPDF(pdfDTO);
    }
    * */

    // Tasa de vinculacion laboral
    public ByteArrayInputStream crearReporteCaracterizacionLaboral() {
        ArrayList<DatosReporteDTO> listadoGraduados = graduadoService.listarGraduados();
        PdfDTO pdfDTO = new PdfDTO();
        int cantidadGraduados = listadoGraduados.size();
        ArrayList<ValorEstadisticoDTO> vinculacionLaboral= new ArrayList();
        vinculacionLaboral.add(new ValorEstadisticoDTO("TRABAJANDO", 0));
        vinculacionLaboral.add(new ValorEstadisticoDTO("DESEMPLEADO", 0));
        JFreeChart graficaAno;
        if (listadoGraduados != null) {
            for (DatosReporteDTO dt: listadoGraduados) {
                if (dt.isEstaTrabajando()) {
                    vinculacionLaboral.get(0).setCantidad(vinculacionLaboral.get(0).getCantidad()+1);
                } else {
                    vinculacionLaboral.get(1).setCantidad(vinculacionLaboral.get(1).getCantidad()+1);
                }
            }
            graficaAno = this.generarGrafico(vinculacionLaboral, "Grafica clasificacion por edad");
        }
        return pdfService.crearPDF(pdfDTO);
    }

    // Titulados por año
    public ByteArrayInputStream crearReporteCantidad(Date fechaInicio, Date fechaFin) {
        ArrayList<DatosReporteDTO> listadoGraduados = graduadoService.listarGraduadosAno(fechaInicio, fechaFin);
        int cantidadGraduados = listadoGraduados.size();
        PdfDTO pdfDTO = new PdfDTO();
        int anoInicio = fechaInicio.getYear();
        int anoFin = fechaFin.getYear();
        ArrayList<ValorEstadisticoDTO> clasificacionPorAno= new ArrayList();
        JFreeChart graficaAno;
        while (anoInicio <= anoFin) {
            clasificacionPorAno.add(new ValorEstadisticoDTO(""+anoInicio , 0));
            anoInicio++;
        }
        if (listadoGraduados != null) {
            for (DatosReporteDTO dt: listadoGraduados) {
                if (dt.getFechaGrado() != null) {
                    for (int i = 0; i < clasificacionPorAno.size(); i++) {
                        ValorEstadisticoDTO dato = clasificacionPorAno.get(i);
                        if (dato.getCategoria().equalsIgnoreCase(String.valueOf(dt.getFechaGrado().getYear()))) {
                            clasificacionPorAno.get(i).setCantidad(dato.getCantidad()+1);
                        }
                    }
                }
            }
            graficaAno = this.generarGrafico(clasificacionPorAno, "Grafica clasificacion por edad");
        }
        return pdfService.crearPDF(pdfDTO);
    }

    private JFreeChart  generarGrafico (ArrayList<ValorEstadisticoDTO> datos, String titulo) {
        DefaultPieDataset data = new DefaultPieDataset();
        for (ValorEstadisticoDTO valor: datos) {
            data.setValue(valor.getCategoria(), valor.getCantidad());
        }
        JFreeChart chart = ChartFactory.createPieChart(
                titulo,
                data,
                true,
                true,
                false);
        return chart;
    }
}
