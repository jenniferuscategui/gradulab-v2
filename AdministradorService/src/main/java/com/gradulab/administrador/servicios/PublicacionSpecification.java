package com.gradulab.administrador.servicios;

import com.gradulab.administrador.entity.Persona;
import com.gradulab.administrador.entity.Publicacion;
import com.gradulab.administrador.general.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class PublicacionSpecification {

    private static final Logger logger = LoggerFactory.getLogger(PublicacionSpecification.class);

    /**
     * al ser una clase utilitaria no se quiere que se cree una instancia de ella
     * y se lanza una excepción en caso de que se intente crear una.
     */
    private PublicacionSpecification() { throw new IllegalStateException("Utility class");}

    /**
     * specificacion para cear el filtro de las lista de personas,
     * donde de busca por nombre y documento
     * @param fecha de la persona a buscar
     * @return Specification<Persona> los criterios de busqueda para obtener la lista.
     */
    public  static Specification<Publicacion> obtenerPublicacionSpecification(Date fecha){
        logger.info("Se crea la especificacion para la busqueda");

        return (root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();

            if(fecha!= null){
                predicates.add(criteriaBuilder.equal(root.get("fechaPu"), fecha));
            }

            return criteriaBuilder.and(predicates.toArray(new Predicate[]{}));
        };
    }
}
