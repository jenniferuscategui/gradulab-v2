package com.gradulab.administrador.servicios;

import com.gradulab.administrador.entity.Usuario;

public interface IAuthUsuarioService {

    public Usuario findByUsername(String username);
}
