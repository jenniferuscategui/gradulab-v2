package com.gradulab.administrador.servicios;

import com.gradulab.administrador.dto.ComentarioDTO;
import com.gradulab.administrador.dto.DatosRegistroDTO;
import com.gradulab.administrador.dto.DatosReporteDTO;
import com.gradulab.administrador.dto.GraduadoDTO;
import com.gradulab.administrador.dto.InfoAcademicaDTO;
import com.gradulab.administrador.dto.InfoLaboralDTO;
import com.gradulab.administrador.dto.PersonaDTO;
import com.gradulab.administrador.dto.PublicacionDTO;
import com.gradulab.administrador.dto.ReaccionPublicacionDTO;
import com.gradulab.administrador.dto.ResgistroPersonaDTO;
import com.gradulab.administrador.entity.Comentario;
import com.gradulab.administrador.entity.Graduado;
import com.gradulab.administrador.entity.InfoAcademica;
import com.gradulab.administrador.entity.InfoLaboral;
import com.gradulab.administrador.entity.Persona;
import com.gradulab.administrador.entity.Publicacion;
import com.gradulab.administrador.entity.Reaccion;
import com.gradulab.administrador.entity.ReaccionPublicacion;
import com.gradulab.administrador.entity.Usuario;
import com.gradulab.administrador.general.constants.Constantes;
import com.gradulab.administrador.general.enums.EnumEstadoGraduado;
import com.gradulab.administrador.general.exception.GradulabExceptionHandler;
import com.gradulab.administrador.general.utils.DTOList;
import com.gradulab.administrador.general.utils.PropertiesServicesReader;
import com.gradulab.administrador.repository.ComentarioRepository;
import com.gradulab.administrador.repository.GraduadoRepository;
import com.gradulab.administrador.repository.InfoAcademicaRepository;
import com.gradulab.administrador.repository.InfoLaboralRepository;
import com.gradulab.administrador.repository.PersonaRepository;
import com.gradulab.administrador.repository.PublicacionRepository;
import com.gradulab.administrador.repository.ReaccionPublicacionRepository;
import com.gradulab.administrador.repository.ReaccionRepository;
import com.gradulab.administrador.repository.UsusarioRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class GraduadoService {

    @Autowired
    private PersonaRepository personaRepository;

    @Autowired
    private InfoLaboralRepository infoLaboralRepository;

    @Autowired
    private InfoAcademicaRepository infoAcademicaRepository;


    @Autowired
    private GraduadoRepository graduadoRepository;

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private PersonaService personaService;

    @Autowired
    private PublicacionRepository publicacionRepository;

    @Autowired
    private UsusarioRepository ususarioRepository;

    @Autowired
    private ComentarioRepository comentarioRepository;

    @Autowired
    private ReaccionPublicacionRepository reaccionPublicacionRepository;

    @Autowired
    private ReaccionRepository reaccionRepository;

    @Autowired
    private DTOList dtoList;




    private static final Logger logger = LoggerFactory.getLogger(GraduadoService.class);
    private static final String SORT_BY_FECHA = "fechaPu";
    private static final String SORT_BY_CODIGO = "codigo";

    private static final String SORT_BY_NAME = "nombre";
    private static final String SORT_BY_DOCUMENT = "documento";


    /**
     * lista de personas paginadas filtradas por nombre y documento
     * ordenada decendentemente por documento y como segundo criterio
     * de orden el nombre.
     * @param nombre nombre de la persona que se quiere buscar.
     * @param documento docuemnto de la persona que se quiere buscar.
     * @return Page<PersonaDTO> lista paginada con 10 elementos por pagina.
     */
    /**public Page<GraduadoDTO> buscarGraduados(Integer page, String nombre, String documento, String codigo) {
        logger.info("Se obtiene el listado de graduados");
        var params = PageRequest.of(page -1, Integer.parseInt("10"), Sort.by(SORT_BY_CODIGO).descending());
        Specification<Graduado> specification = GraduadoSpecificacion.obtenerGraduadoSpecification(nombre, documento, codigo);
        Page<Graduado> graduadosPage = graduadoRepository.findAll(specification,params);
        if (graduadosPage.getNumberOfElements() > 0) {
            return new PageImpl<>(dtoList.getDtoList(graduadosPage.getContent()), params, graduadosPage.getTotalElements());
        }
        return new PageImpl<>(new ArrayList<>());
    }*/

    public List<GraduadoDTO> buscarGraduados(String documento, String codigo) {
        logger.info("Se obtiene el listado de graduados");
        List<Graduado> listaGraduado = new ArrayList<>();
        if (codigo != null){
            Optional<Graduado> graduado = graduadoRepository.findByCodigo(codigo);
            graduado.ifPresent(listaGraduado::add);

        } else if (documento != null){
            Optional<Persona> persona = personaRepository.findByDocumento(documento);
            if (persona.isPresent()){
                Optional<Graduado> graduado = graduadoRepository.findByPersona(persona.get());
                graduado.ifPresent(listaGraduado::add);
            }
        }
        return dtoList.getDtoList(listaGraduado);
    }


    /**
     * lista de personas paginadas filtradas por nombre y documento
     * ordenada decendentemente por documento y como segundo criterio
     * de orden el nombre.
     * @param fecha nombre de la persona que se quiere buscar.
     * @return Page<PersonaDTO> lista paginada con 10 elementos por pagina.
     */
    public Page<PublicacionDTO>  buscarPublicacion(Date fecha, Integer page) {
        logger.info("Se obtiene el listado de personas");
        PageRequest params = PageRequest.of(page -1, Integer.parseInt("10"), Sort.by(SORT_BY_FECHA).descending());
        Specification<Publicacion> specification = PublicacionSpecification.obtenerPublicacionSpecification(fecha);
        Page<Publicacion> publicacionPage = publicacionRepository.findAll(specification, params);
        if (publicacionPage.getNumberOfElements() > 0) {
            List<PublicacionDTO> list = dtoList.getDtoList(publicacionPage.getContent());
            list.forEach(publicacionDTO -> {
               publicacionDTO = buscarCantidadReacciones(1L,publicacionDTO);
               publicacionDTO = buscarCantidadReacciones(2L, publicacionDTO);
               publicacionDTO = buscarCantidadComentarios(publicacionDTO);
            });
            return new PageImpl<>(list, params, publicacionPage.getTotalElements());
        }
        return new PageImpl<>(new ArrayList<>());
    }

    public PublicacionDTO buscarCantidadReacciones(Long idReaccion,PublicacionDTO publicacionDTO){
        Reaccion reaccion = new Reaccion();
        Publicacion publicacion = new Publicacion();

        publicacion.setId(publicacionDTO.getId());
        reaccion.setId(idReaccion);

        Optional<ReaccionPublicacion> reac = reaccionPublicacionRepository.findByPublicacionAndReaccion(publicacion,reaccion);
        if (reac.isPresent() && idReaccion == 1L){
            publicacionDTO.setCantidadMegusta(reac.get().getCantidad());
        }else if (reac.isPresent() && idReaccion == 2L){
            publicacionDTO.setCatidadMencanta(reac.get().getCantidad());
        } else if (idReaccion == 1L){
            publicacionDTO.setCantidadMegusta(0L);
        } else {
            publicacionDTO.setCatidadMencanta(0L);
        }

        return publicacionDTO;
    }

    public PublicacionDTO buscarCantidadComentarios(PublicacionDTO publicacionDTO){
        Publicacion publicacion = new Publicacion();
        publicacion.setId(publicacionDTO.getId());
        Long cantidadComent = comentarioRepository.countComentarioByPublicacion(publicacion);
        if (cantidadComent != null){
            publicacionDTO.setCantidadComentario(cantidadComent);
        }else{
            publicacionDTO.setCantidadComentario(0L);
        }

        return publicacionDTO;
    }

    /**
     * guarda la informacion que se recibe del graduado
     * @param resgistroPersonaDTO el objecto que tiene la informacion basica del graduado y el codigo
     * @return GraduadoDTO retorna el objeto graduado que se guardo.
     */
    public GraduadoDTO registrarGraduado(ResgistroPersonaDTO resgistroPersonaDTO ) throws GradulabExceptionHandler {
        logger.info("### SE GUARDA LA INFORMACION DE LA PERSONA ###");
        Graduado graduado = new Graduado();
        if (usuarioService.existeUsuarioDePersona(resgistroPersonaDTO.getPersonaDto())){
            throw new GradulabExceptionHandler("usuario ya existe");
        }
        usuarioService.crearUsuarioGraduadoFile(resgistroPersonaDTO.getPersonaDto(), Constantes.ST_N, resgistroPersonaDTO.getContrasena(), false);

        graduado.setPersona(personaService.guardarPersona(resgistroPersonaDTO.getPersonaDto()));
        graduado.setCodigo(resgistroPersonaDTO.getPersonaDto().getCodigoGraduado());
        graduado.setEstado(EnumEstadoGraduado.GRADUADO.getNombre());
        graduado.setFechaIngresoReg(new Date());
        graduado = graduadoRepository.save(graduado);
        return graduado.getDto();
    }



    /**
     * Se registra la informacion laboral del graduado
     * si ya tiene informacion se actualiza.
     * @param infoLaboralDTO el objeto con la informacion laboral.
     * @return InfoLaboralDTO la informacion que se actualizo
     */
    public InfoLaboralDTO ingresarInfoLaborar(InfoLaboralDTO infoLaboralDTO){
        Optional<Graduado> graduado= graduadoRepository.findById(infoLaboralDTO.getGraduadoDto().getId());
        if (graduado.isPresent()){
            Optional<InfoLaboral> infoLaboralOptional = infoLaboralRepository.findByGraduado(graduado.get());
            if (infoLaboralOptional.isPresent()){
               return guardarInfoLaboralGraduado(infoLaboralOptional.get(), infoLaboralDTO, graduado.get());
            }else {
                InfoLaboral infoLaboral = new InfoLaboral();
                return guardarInfoLaboralGraduado(infoLaboral, infoLaboralDTO, graduado.get());
            }
        }
        return null;
    }

    private InfoLaboralDTO guardarInfoLaboralGraduado(InfoLaboral infoLaboral, InfoLaboralDTO infoLaboralDTO, Graduado graduado){
        BeanUtils.copyProperties(infoLaboralDTO, infoLaboral);
        infoLaboral.setGraduado(graduado);
        return infoLaboralRepository.save(infoLaboral).getDto();
    }

    /**
     * Se registra la informacion laboral del graduado
     * si ya tiene informacion se actualiza.
     * @param infoAcademicaDTO el objeto con la informacion laboral.
     * @return InfoLaboralDTO la informacion que se actualizo
     */
    public InfoAcademicaDTO ingresarInfoAcademica(InfoAcademicaDTO infoAcademicaDTO){
        Optional<Graduado> graduado = graduadoRepository.findById(infoAcademicaDTO.getGraduado().getId());
        if (graduado.isPresent()){
            Optional<InfoAcademica> infoAcademicaOptional = infoAcademicaRepository.findByGraduado(graduado.get());
            if (infoAcademicaOptional.isPresent()){
                return guardarInfoAcademicaGraduado(infoAcademicaOptional.get(),infoAcademicaDTO, graduado.get());
            }else {
                InfoAcademica infoAcademica = new InfoAcademica();
                return guardarInfoAcademicaGraduado(infoAcademica,infoAcademicaDTO, graduado.get());
            }
        }
        return null;
    }

    private InfoAcademicaDTO guardarInfoAcademicaGraduado(InfoAcademica infoAcademica, InfoAcademicaDTO infoAcademicaDTO, Graduado graduado){
        BeanUtils.copyProperties(infoAcademicaDTO, infoAcademica);
        infoAcademica.setGraduado(graduado);
        return infoAcademicaRepository.save(infoAcademica).getDto();
    }

    //Busca el graduado por el id persona
    public GraduadoDTO buscarPorIdPersona(Long idPersona) {
        Persona persona = new Persona();
        persona.setId(idPersona);
        Optional<Graduado> graduadoDTO = graduadoRepository.findByPersona(persona);;
        return graduadoDTO.map(Graduado::getDto).orElse(null);
    }



    public boolean actualizarGraduado(GraduadoDTO graduadoDto) {
        boolean respuesta = false;
        Graduado graduado = null;
        graduado.setId(graduadoDto.getId());
        graduado.setFechaGrado(graduado.getFechaGrado());
        graduado.setCodigo(graduadoDto.getCodigo());
        graduado.setEstado(graduadoDto.getEstado());
        graduado.setFolio(graduadoDto.getFolio());
        graduado.setActa(graduadoDto.getActa());
        graduado = graduadoRepository.save(graduado);
        if (graduado.getDto() != null) {
            respuesta = true;
        }
        return respuesta;
    }



    public PublicacionDTO crearPublicacion(PublicacionDTO publicacionDTO) {
        Optional<Usuario> usuario = ususarioRepository.findById(publicacionDTO.getUsuario().getId());
         try {
             if (usuario.isPresent() && graduadoRepository.findByPersona(usuario.get().getPersona()).isPresent()) {
                 Publicacion publicacion = new Publicacion();
                 BeanUtils.copyProperties(publicacionDTO, publicacion);
                 publicacion.setFechaPu(new Date());
                 publicacion.setFechaIngresoReg(new Date());
                 publicacion.setFechaModificareg(new Date());
                 publicacion.setFechaPu(new Date());
                 publicacion = publicacionRepository.save(publicacion);
                 return publicacion.getDto();
             }

         }catch (Exception e){
             logger.error(e.getMessage());
             return null;
         }
        return null;
    }

    public PublicacionDTO adjuntarFoto(MultipartFile archivo, Long id){
        logger.info("#### Inicia la subida de imagen ####");
        Map<String, Object> response = new HashMap<>();
        Optional<Publicacion> publicacion = publicacionRepository.findById(id);
        if(!archivo.isEmpty() && publicacion.isPresent()){
            String nombreArchivo = "pu"+publicacion.get().getId()+archivo.getOriginalFilename().replace(" ","");
            Path rutaArchivo  = Paths.get(PropertiesServicesReader.getInstance().getValue("url.files.base")).resolve(nombreArchivo).toAbsolutePath();
            try{
                Files.copy(archivo.getInputStream(), rutaArchivo);
            }catch (IOException e){
                logger.error(e.getMessage());

            }
            String nombreFotoAnterior = publicacion.get().getMultimedia();

            if(nombreFotoAnterior !=null && nombreFotoAnterior.length() >0) {
                Path rutaFotoAnterior = Paths.get(PropertiesServicesReader.getInstance().getValue("url.files.base")).resolve(nombreFotoAnterior).toAbsolutePath();
                File archivoFotoAnterior = rutaFotoAnterior.toFile();
                if(archivoFotoAnterior.exists() && archivoFotoAnterior.canRead()) {
                    archivoFotoAnterior.delete();
                }
            }
            Publicacion p = publicacion.get();
            p.setMultimedia(nombreArchivo);
            publicacionRepository.save(p);
            return p.getDto();
        }
        return null;
    }

    public ComentarioDTO comentarPublicacion(ComentarioDTO comentarioDTO) {
        Optional<Usuario> usuario = ususarioRepository.findById(comentarioDTO.getUsuario().getId());

        if (usuario.isPresent() && graduadoRepository.findById(usuario.get().getPersona().getId()).isPresent()) {
            Optional<Publicacion> publicacion = publicacionRepository.findById(comentarioDTO.getPublicacion().getId());
            if (publicacion.isPresent()){
                Comentario comentario = new Comentario();
                BeanUtils.copyProperties(comentarioDTO, comentario);
                comentario.setFechaModificaReg(new Date());
                comentario.setFechaModificaReg(new Date());
                return comentarioRepository.save(comentario).getDto();
            }
        }
        return null;
    }

    public ComentarioDTO editarPublicacion(ComentarioDTO comentarioDTO) {
        Optional<Comentario> comentario = comentarioRepository.findById(comentarioDTO.getId());

        if (comentario.isPresent()) {
            comentario.get().setDescripcion(comentarioDTO.getDescripcion());
            comentario.get().setFechaModificaReg(new Date());
            return comentarioRepository.save(comentario.get()).getDto();
        }
        return null;
    }

    public ReaccionPublicacionDTO reaccionarPublicacion(ReaccionPublicacionDTO reaccionPublicacionDTO) {

        Optional<Publicacion> publicacion = publicacionRepository.findById(reaccionPublicacionDTO.getPublicacion().getId());
        Optional<Reaccion> reaccion = reaccionRepository.findById(reaccionPublicacionDTO.getReaccion());
        //buscar el usuario
        ReaccionPublicacion reaccionPublicacionObj = new ReaccionPublicacion();

        if (publicacion.isPresent() && reaccion.isPresent()) {

            Optional<ReaccionPublicacion> reaccionPublicacion = reaccionPublicacionRepository.findByPublicacionAndReaccion(publicacion.get(),reaccion.get());

            if(reaccionPublicacion.isPresent()){
                reaccionPublicacionObj = reaccionPublicacion.get();
                reaccionPublicacionObj.setCantidad(reaccionPublicacionObj.getCantidad() + 1L);
            }else{
                reaccionPublicacionObj.setCantidad(1L);
            }
            reaccionPublicacionObj.setPublicacion(publicacion.get());
            reaccionPublicacionObj.setReaccion(reaccion.get());
            reaccionPublicacionObj.setFechaIngresoReg(new Date());
            return reaccionPublicacionRepository.save(reaccionPublicacionObj).getDto();
        }
        return null;
    }

    /**
     * guarda la informacion que se recibe del graduado
     * @param graduadoDTO el objecto que tiene la informacion basica del graduado
     * @return GraduadoDTO retorna el objeto graduado que se guardo.
     */
    public boolean registrarGraduado(GraduadoDTO graduadoDTO, PersonaDTO personaDTO )  {
        boolean respuesta = false;
        Graduado graduado = new Graduado();
        if (usuarioService.existeUsuarioDePersona(graduadoDTO.getPersonaDto())){
            respuesta = true;
        }
        graduado.setPersona(personaRepository.save(personaService.guardarPersona(personaDTO)));
        graduado.setFechaSolicitud(graduadoDTO.getFechaSolicitud());
        graduado.setCodigo(graduadoDTO.getCodigo());
        graduado.setFechaGrado(graduadoDTO.getFechaGrado());
        graduado.setActa(graduadoDTO.getActa());
        graduado.setFolio(graduadoDTO.getFolio());
        graduado.setEstado(graduadoDTO.getEstado());
        graduado.setFechaIngresoReg(new Date());
        graduado = graduadoRepository.save(graduado);
        if (graduado.getDto() != null) {
            respuesta = true;
        }
        return respuesta;
    }

    public ArrayList listarGraduadosConInformacionContacto() {
        ArrayList<DatosRegistroDTO> informacionContacto = new ArrayList<>();
        List<Graduado> graduados = graduadoRepository.findAll();
        int contador = 0;
        for (Graduado g : graduados) {
            DatosRegistroDTO datos = new DatosRegistroDTO();
            contador++;
            datos.setIdRegistro(contador);
            datos.setCodigo(g.getCodigo());
            Optional<Persona> persona = personaRepository.findById(g.getPersona().getId());
            if (persona.isPresent()) {
                datos.setCedula(persona.get().getDocumento());
                datos.setApellidos(persona.get().getApellidos());
                datos.setNombres(persona.get().getNombre());
                datos.setCelular(persona.get().getCelular());
                datos.setEmail(persona.get().getCorreoPersonal());
            }
            informacionContacto.add(datos);
        }
        return informacionContacto;
    }

    public ArrayList listarGraduadosAno( Date fechaInicio, Date fechaFin) {
        ArrayList<DatosReporteDTO> listado = new ArrayList<DatosReporteDTO>();
        List<Graduado> graduados = graduadoRepository.findByFechaGradoBetween(fechaInicio, fechaFin);
        for (Graduado g : graduados) {
            if (g.getEstado().equalsIgnoreCase(String.valueOf(EnumEstadoGraduado.GRADUADO))){
                DatosReporteDTO datos = new DatosReporteDTO();
                datos.setCodigoGraduado(g.getCodigo());
                datos.setModalidad(g.getModalidad());
                datos.setFechaGrado(g.getFechaGrado());
                Optional<Persona> persona = personaRepository.findById(g.getPersona().getId());
                if (persona.isPresent()) {
                    Period edad = Period.between(LocalDate.parse(persona.get().getFechaNacimiento()), LocalDate.now());
                    datos.setEdad(edad.getYears());
                    datos.setSexo(persona.get().getSexo());
                }
                listado.add(datos);
            }

        }
        return listado;
    }

    public ArrayList listarGraduados() {
        ArrayList<DatosReporteDTO> listado = new ArrayList<DatosReporteDTO>();
        List<Graduado> graduados = graduadoRepository.findAll();
        for (Graduado g : graduados) {
            if (g.getEstado().equalsIgnoreCase(String.valueOf(EnumEstadoGraduado.GRADUADO))){
                DatosReporteDTO datos = new DatosReporteDTO();
                boolean trabajando = false;
                datos.setCodigoGraduado(g.getCodigo());
                datos.setModalidad(g.getModalidad());
                datos.setFechaGrado(g.getFechaGrado());
                Optional<Persona> persona = personaRepository.findById(g.getPersona().getId());
                if (persona.isPresent()) {
                    Period edad = Period.between(LocalDate.parse(persona.get().getFechaNacimiento()), LocalDate.now());
                    datos.setEdad(edad.getYears());
                    datos.setSexo(persona.get().getSexo());
                }
                Optional<InfoLaboral> infoLaboralOptional = infoLaboralRepository.findByGraduado(g);
                if (infoLaboralOptional.isPresent()) {
                    if (infoLaboralOptional.get().getFechaFin() != null) {
                        trabajando = true;
                    }
                }
                datos.setEstaTrabajando(trabajando);
                listado.add(datos);
            }

        }
        return listado;
    }

}
