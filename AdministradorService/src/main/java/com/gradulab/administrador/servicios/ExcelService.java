package com.gradulab.administrador.servicios;

import com.gradulab.administrador.dto.DatosRegistroDTO;
import com.gradulab.administrador.dto.EmpresaDTO;
import com.gradulab.administrador.dto.GraduadoDTO;
import com.gradulab.administrador.dto.PersonaDTO;
import com.gradulab.administrador.entity.Graduado;
import com.gradulab.administrador.entity.InfoLaboral;
import com.gradulab.administrador.general.constants.Constantes;
import com.gradulab.administrador.general.enums.EnumEstadoGraduado;
import com.gradulab.administrador.repository.GraduadoRepository;
import com.gradulab.administrador.repository.InfoLaboralRepository;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Optional;

@Service
public class ExcelService {

    @Autowired
    private PersonaService personaService;
    @Autowired
    private EmpresaService empresaService;
    @Autowired
    private GraduadoService graduadoService;
    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private GraduadoRepository graduadoRepository;

    @Autowired
    private InfoLaboralRepository infoLaboralRepository;

    public String importarExcel (MultipartFile listado, byte tipo) throws IOException {
        ArrayList <DatosRegistroDTO> datosArchivo = new ArrayList<>();
        String respuesta = null;
        boolean finalizado = false;
        if (listado.isEmpty()) {
            return "El archivo está vacio";
        }
        if (tipo == 1 || tipo == 2) {
            datosArchivo = leerExcel(listado, tipo);
        }
        if (datosArchivo.isEmpty()) {
            return "No hay información";
        }
        if ( tipo == 1) {
            finalizado = almacenarInformacion (datosArchivo);
        } else {
            finalizado = actualizarInformacion (datosArchivo);
        }
        return respuesta;
    }

    private ArrayList<DatosRegistroDTO> leerExcel(MultipartFile listado, byte tipo) throws IOException {
        ArrayList<DatosRegistroDTO> informacion = new ArrayList<>();
        HSSFWorkbook libro = new HSSFWorkbook (listado.getInputStream());
        HSSFSheet hoja = libro.getSheetAt(0);

        for(int i = 4; i < hoja.getLastRowNum()+1; i++) {
            HSSFRow fila = hoja.getRow(i);
            String trabaja = null;
            String noTrabaja = null;
            DatosRegistroDTO dato = new DatosRegistroDTO();
            dato.setApellidos(fila.getCell(1).getStringCellValue());
            dato.setNombres(fila.getCell(2).getStringCellValue());
            dato.setCedula(fila.getCell(3).getStringCellValue());
            if ( tipo == 1) {
                dato.setFechaSolicitud(fila.getCell (0).getDateCellValue());
                dato.setCodigo(fila.getCell(4).getStringCellValue());
                dato.setTelefono(fila.getCell(5).getStringCellValue());
                dato.setCelular(fila.getCell(6).getStringCellValue());
                dato.setEmail(fila.getCell(7).getStringCellValue());
                trabaja = fila.getCell(8).getStringCellValue();
                noTrabaja = fila.getCell(9).getStringCellValue();
                if (trabaja.equalsIgnoreCase("X") && noTrabaja == null) {
                    dato.setTrabaja(true);
                } else if (noTrabaja.equalsIgnoreCase("X") && trabaja == null) {
                    dato.setTrabaja(false);
                }
                dato.setNombreEmpresa(fila.getCell(10).getStringCellValue());
                dato.setNombreRepresentante(fila.getCell(11).getStringCellValue());
                dato.setTelefonoEmpresa(fila.getCell(12).getStringCellValue());
                dato.setCorreoEmpresa(fila.getCell(13).getStringCellValue());
            } else if ( tipo == 2) {
                dato.setFechaGrado(fila.getCell (0).getDateCellValue());
                dato.setActa(fila.getCell(4).getStringCellValue());
                dato.setFolio(fila.getCell(5).getStringCellValue());
        //        dato.setModalidad(fila.getCell(6).getStringCellValue());
        //        dato.setNombreTrabajo(fila.getCell(7).getStringCellValue());
            }
            informacion.add(dato);
        }
        return informacion;
    }

    private boolean almacenarInformacion(ArrayList<DatosRegistroDTO> datosArchivo) {
        boolean respuesta = false;
        int contador = 0;
        for (DatosRegistroDTO dato : datosArchivo) {
            boolean registroGraduado = false;
            boolean registroPersona = false;
            PersonaDTO persona = new PersonaDTO(dato.getCedula(), dato.getApellidos(), dato.getNombres(),
                    dato.getTelefono(), dato.getCelular(), dato.getEmail());
            if (personaService.guardarPersona(persona) != null) {
                registroPersona = true;
            }

            GraduadoDTO graduado = new GraduadoDTO(dato.getFechaSolicitud(), dato.getCodigo(), EnumEstadoGraduado.NO_GRADUADO.getNombre());
            registroGraduado = graduadoService.registrarGraduado(graduado, persona);
            if (registroGraduado && registroPersona) {
                contador++;
            }

            InfoLaboral infoLaboral = new InfoLaboral();
            if (dato.isTrabaja()) {
                infoLaboral.setEmpresa(dato.getNombreEmpresa());
                infoLaboral.setTelefonoEmpresa(dato.getTelefonoEmpresa());
                infoLaboral.setEmailEmpresa(dato.getCorreoEmpresa());
                Optional<Graduado> graduado2 = graduadoRepository.findByCodigo(dato.getCodigo());
                graduado2.ifPresent(infoLaboral::setGraduado);
                infoLaboralRepository.save(infoLaboral);
            }
        }
        if (contador == datosArchivo.size()) {
            respuesta = true;
        }
        return respuesta;
    }



    private boolean actualizarInformacion(ArrayList<DatosRegistroDTO> datosArchivo) {
        boolean respuesta = false;
        int totalRegistros = datosArchivo.size();
        int contador = 0;
        for (DatosRegistroDTO dato : datosArchivo) {
            boolean asignoUsuario = false;
            boolean guardoGraduado = false;
            PersonaDTO persona = personaService.buscarPorDocuemnto(dato.getCedula());
            if (persona == null) {
                persona = new PersonaDTO(dato.getCedula(), dato.getApellidos(), dato.getNombres());
                personaService.guardarPersona(persona);
            }
            GraduadoDTO graduado = graduadoService.buscarPorIdPersona(persona.getId());
            if (graduado == null) {
                graduado = new GraduadoDTO(dato.getFechaGrado(), dato.getActa(), dato.getFolio(), EnumEstadoGraduado.GRADUADO.getNombre());
                guardoGraduado = graduadoService.registrarGraduado(graduado, persona);
            } else {
                graduado.setFechaGrado(dato.getFechaGrado());
                graduado.setActa(dato.getActa());
                graduado.setFolio(dato.getFolio());
                graduado.setEstado(EnumEstadoGraduado.GRADUADO.getNombre());
                graduadoService.actualizarGraduado(graduado);
            }
            if (!usuarioService.existeUsuarioDePersona(persona)) {
                usuarioService.crearUsuarioGraduadoFile(persona, Constantes.ST_S, null, false);
            }
            if (asignoUsuario && guardoGraduado) {
                contador++;
            }
        }
        if (totalRegistros == contador) {
            respuesta = true;
        }
        return respuesta;
    }


    public ByteArrayInputStream crearExcel(String[] encabezado, ArrayList<DatosRegistroDTO> listado) throws IOException {
        try(
                Workbook workbook = new XSSFWorkbook();
                ByteArrayOutputStream out = new ByteArrayOutputStream();
        ) {
            CreationHelper createHelper = workbook.getCreationHelper();
            Sheet sheet = workbook.createSheet("Información de contacto");
            Font headerFont = workbook.createFont();
            headerFont.setBold(true);

            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setFont(headerFont);
            Row headerRow = sheet.createRow(0);
            for (int col = 0; col < encabezado.length; col++) {
                Cell cell = headerRow.createCell(col);
                cell.setCellValue(encabezado[col]);
                cell.setCellStyle(headerCellStyle);
            }

            int rowIdx = 1;
            for (DatosRegistroDTO dato : listado) {
                Row row = sheet.createRow(rowIdx++);
                //{"Id.","Código", "Cédula", "Apellidos", "Nombres", "Celular", "Email"};
                row.createCell(0).setCellValue(dato.getIdRegistro());
                row.createCell(1).setCellValue(dato.getCodigo());
                row.createCell(2).setCellValue(dato.getCedula());
                row.createCell(3).setCellValue(dato.getApellidos());
                row.createCell(4).setCellValue(dato.getNombres());
                row.createCell(5).setCellValue(dato.getCelular());
                row.createCell(6).setCellValue(dato.getEmail());
            }
            workbook.write(out);
            return new ByteArrayInputStream(out.toByteArray());
        }
    }


}
