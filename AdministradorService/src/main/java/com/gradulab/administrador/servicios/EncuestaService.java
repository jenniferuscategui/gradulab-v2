package com.gradulab.administrador.servicios;

import com.gradulab.administrador.dto.EncuestaDTO;
import com.gradulab.administrador.dto.OpcionesRespuestaDTO;
import com.gradulab.administrador.dto.PreguntaDTO;

public class EncuestaService {


    public void crearEncuesta(EncuestaDTO encuestaDTO) {
    }

    public void agregarPregunta(PreguntaDTO preguntaDTO, EncuestaDTO encuestaDTO) {
    }

    public void agregarOpciones(OpcionesRespuestaDTO opcionesRespuestaDTO, PreguntaDTO preguntaDTO) {
    }

    public void mostrarResultados(EncuestaDTO encuestaDTO) {
    }
}
