package com.gradulab.administrador.servicios;

import com.gradulab.administrador.dto.ContactoEmpresaDTO;
import com.gradulab.administrador.dto.EmpresaDTO;
import com.gradulab.administrador.dto.OfertaLaboralDTO;
import com.gradulab.administrador.dto.PersonaDTO;
import com.gradulab.administrador.dto.ResgistroPersonaDTO;
import com.gradulab.administrador.entity.ContactoEmpresa;
import com.gradulab.administrador.entity.Empresa;
import com.gradulab.administrador.entity.OfertaLaboral;
import com.gradulab.administrador.entity.Persona;
import com.gradulab.administrador.general.constants.Constantes;
import com.gradulab.administrador.general.enums.EnumEstadoOfertaLaboral;
import com.gradulab.administrador.general.exception.GradulabExceptionHandler;
import com.gradulab.administrador.general.utils.DateUtils;
import com.gradulab.administrador.repository.ConactoEmpresaRepository;
import com.gradulab.administrador.repository.EmpresaRespository;
import com.gradulab.administrador.repository.OfertaLaboralRespository;
import com.gradulab.administrador.repository.PersonaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Locale;
import java.util.Optional;

@Service
public class EmpresaService {

    @Autowired
    private PersonaRepository personaRepository;

    @Autowired
    private EmpresaRespository empresaRespository;

    @Autowired
    private ConactoEmpresaRepository conactoEmpresaRepository;

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private OfertaLaboralRespository ofertaLaboralRespository;

    @Autowired
    private DateUtils dateUtils;

    @Autowired
    private PersonaService personaService;


    private static final Logger logger = LoggerFactory.getLogger(EmpresaService.class);

    /**
     * guarda la informacion del contacto empresa y de la empresa
     * @param resgistroPersonaDTO el objecto que tiene la informacion basica de la
     *                            empresa y el contacto empresa.
     * @return ContatcoEmpresaDTO retorna el objeto graduado que se guardo.
     */
    public ContactoEmpresaDTO registrarEmpresa(ResgistroPersonaDTO resgistroPersonaDTO ) throws GradulabExceptionHandler {
        logger.info("Se registra  el contacto empresa");
        ContactoEmpresa contactoEmpresa = new ContactoEmpresa();
        Boolean validarEmpresa = this.exiteEmpresa(resgistroPersonaDTO.getEmpresaDto());
        if (Boolean.TRUE.equals(validarEmpresa)){
            throw new GradulabExceptionHandler("la empresa ya existe");
        }
        String documento = resgistroPersonaDTO.getPersonaDto().getDocumento().concat(resgistroPersonaDTO.getEmpresaDto().getNit());
        resgistroPersonaDTO.getPersonaDto().setDocumento(documento);
        if (Boolean.TRUE.equals(personaService.buscarPorDocuemnto(documento) != null)){
            throw new GradulabExceptionHandler("la contacto empresa ya existe");
        }
        resgistroPersonaDTO.getPersonaDto().setCodigoGraduado(null);
        resgistroPersonaDTO.getPersonaDto().setCorreoCorporativo(resgistroPersonaDTO.getPersonaDto().getCorreoPersonal());
        resgistroPersonaDTO.getPersonaDto().setCorreoPersonal("");
        contactoEmpresa.setPersona(personaService.guardarPersona(resgistroPersonaDTO.getPersonaDto()));
        contactoEmpresa.setEmpresa(guardarEmpresa(resgistroPersonaDTO.getEmpresaDto()));
        contactoEmpresa.setFechaIngresoRegistro(dateUtils.obtenerFechaActual());
        usuarioService.crearUsuarioGraduadoFile(contactoEmpresa.getPersona().getDto(), Constantes.ST_N, resgistroPersonaDTO.getContrasena(), true);
        return conactoEmpresaRepository.save(contactoEmpresa).getDto();
    }

    public Boolean exiteEmpresa(EmpresaDTO empresaDTO){
        logger.info("### SE BUSCA SI LA EMPRESA EXISTE ###");
        Optional<Empresa> empresa = empresaRespository.findByNit(empresaDTO.getNit());
        return empresa.isPresent();
    }

    /**
     * Se guarda la empresa.
     * @param empresaDTO informacion de la empresa.
     * @return objeto empresa que se guardo.
     */
    public Empresa guardarEmpresa(EmpresaDTO empresaDTO) {
        logger.info("Se guarda el empresa");
        Empresa empresa = new Empresa();
        BeanUtils.copyProperties(empresaDTO, empresa);
        empresa.setFechaIngresoRegistro(dateUtils.obtenerFechaActual());
        return empresaRespository.save(empresa);
    }

    /**
     * Se crea oferta labora
     * @param ofertaLaboralDTO la informacionde la oferta laboral
     * @return la informacion de la oferta laboral.
     * @throws GradulabExceptionHandler se maneja la excepcion en caso de que no exista la empresa.
     */
    public OfertaLaboral crearOfertaLaboral(OfertaLaboralDTO ofertaLaboralDTO) throws GradulabExceptionHandler {

        Optional<Empresa> empresaOptional = empresaRespository.findById(ofertaLaboralDTO.getEmpresaDto().getId());

        if (empresaOptional.isPresent()) {
            OfertaLaboral ofertaLaboral = new OfertaLaboral();
            ofertaLaboral.setEmpresa(empresaOptional.get());
            ofertaLaboral.setConocimiento(ofertaLaboralDTO.getConocimiento());
            ofertaLaboral.setDescripcion(ofertaLaboralDTO.getDescripcion());
            ofertaLaboral.setExperiencia(ofertaLaboralDTO.getExperiencia());
            ofertaLaboral.setFechaIngresoRegistro(dateUtils.obtenerFechaActual());
            ofertaLaboral.setSalario(ofertaLaboralDTO.getSalario());
            ofertaLaboral.setTipoContrato(ofertaLaboralDTO.getTipoContrato());
            ofertaLaboral.setFechaModificaRegistro(dateUtils.obtenerFechaActual());
            ofertaLaboral.setEstado(EnumEstadoOfertaLaboral.ABIERTA.getNombre());
            ofertaLaboralRespository.save(ofertaLaboral);
            return ofertaLaboral;
        }else{
            throw new GradulabExceptionHandler("La empresa no existe");
        }
    }

    /**
     * se lista las ofertas laborares de la empresa
     * @param empresa
     * @return el listado de la oferta laboral.
     * @throws GradulabExceptionHandler excepcion en caso de que no exita la oferta laboral.
     */
    public List<OfertaLaboral> listarOfertasEmpresa(EmpresaDTO empresa) throws GradulabExceptionHandler {

        Optional<Empresa> empresaOptional = empresaRespository.findById(empresa.getId());

        try {
            if (empresaOptional.isPresent()) {
                return ofertaLaboralRespository.findAllByEmpresa(empresaOptional.get());
            } else {
                throw new GradulabExceptionHandler("La empresa no existe");
            }
        }catch (Exception e){
            logger.error(e.getMessage());
        }

        return null;
    }

    /*Valida si el correo de la empresa se encuentra registrado en el sistema
    * */
    public Boolean existeEmpresa(String correoEmpresa){
        logger.info("Confirma si ya se encuentra registrado el correo de la empresa");
        return empresaRespository.findByEmail(correoEmpresa) != null;
    }
}
