package com.gradulab.administrador.servicios;

import com.gradulab.administrador.entity.Notificacion;
import com.gradulab.administrador.entity.NotificacionesUsuario;
import com.gradulab.administrador.general.enums.EnumEstadoNotificacion;
import com.gradulab.administrador.repository.NoitificacionRepository;
import com.gradulab.administrador.repository.NotificacionesUsuarioRepository;
import com.gradulab.administrador.entity.Usuario;
import com.gradulab.administrador.general.enums.EnumTipoNotificacion;
import com.gradulab.administrador.general.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Service
public class NotificacionesService {

    @Autowired
    private NoitificacionRepository notificacionRepository;

    @Autowired
    private NotificacionesUsuarioRepository notificacionesUsuarioRespository;

    @Autowired
    private JavaMailSender emailSender;

    private final Logger logger = LoggerFactory.getLogger(NotificacionesService.class);


    public void buscarNotificacionesPendientes(){
        logger.info("#### SE BUSCAN LOS ULTIMOS CORREOS PENDIENDTES PARA ENVIAR LA NOTIFICACION ####");
        List<NotificacionesUsuario> list = notificacionesUsuarioRespository
                .findFirst10ByEstadoOrderByFechaIngresoRegistroAsc(EnumEstadoNotificacion.PENDIENTE.getId());

        if (!list.isEmpty()){
            list.forEach(this::sent);
        }else {
            logger.info("#### NO HAY CORREOS PENDIENTES ####");
        }
    }

    public void sent(NotificacionesUsuario notificacionesUsuario){
        logger.info("#### SE ENVIA LA NOTIFICACION ####");

        try{
            SimpleMailMessage message = new SimpleMailMessage();
            message.setTo(notificacionesUsuario.getEmail());
            message.setFrom("proyecto.gradulab@gmail.com");
            message.setSubject(notificacionesUsuario.getAsuntoMensaje());
             message.setText(notificacionesUsuario.getMensaje());
            emailSender.send(message);
            notificacionesUsuario.setIntentos(+1);
            notificacionesUsuario.setEstado(EnumEstadoNotificacion.ENVIADA.getId());
            notificacionesUsuarioRespository.save(notificacionesUsuario);
        } catch (Exception exception){
            logger.info("#### FALLO EN EL ENIO DE LA NOTIFICACION ####");
            logger.error(exception.getMessage());
            notificacionesUsuario.setIntentos(+1);
            notificacionesUsuario.setEstado(EnumEstadoNotificacion.FALLIDA.getId());
            notificacionesUsuarioRespository.save(notificacionesUsuario);
        }

    }

    public void contruirNotificacion(Usuario usuario, EnumTipoNotificacion tipoNotificacion){
        logger.info("#### se prepara la notificacion ####");
        NotificacionesUsuario notificacionesUsuario = new NotificacionesUsuario();
        Optional<Notificacion> notificacion = notificacionRepository.findById(tipoNotificacion.getId());
        HashMap<String, String> params = new HashMap<>();
        if (notificacion.isPresent()){
            params.put("nombre",usuario.getPersona().getNombre());
            if (EnumTipoNotificacion.RECUPERAR_CLAVE.equals(tipoNotificacion)){
                params.put("clave", usuario.getPersona().getDocumento());
            }
            notificacionesUsuario.setUsuario(usuario);
            if (usuario.getPersona().getCorreoPersonal().equals("") && usuario.getPersona().getCorreoCorporativo() != null){
                notificacionesUsuario.setEmail(usuario.getPersona().getCorreoCorporativo());
            }else {
                notificacionesUsuario.setEmail(usuario.getPersona().getCorreoPersonal());
            }

            notificacionesUsuario.setAsuntoMensaje(StringUtils.parseParams(notificacion.get().getAsuntoMensaje(), params));
            notificacionesUsuario.setMensaje(StringUtils.parseParams(notificacion.get().getMensaje(), params));
            notificacionesUsuario.setEstado(EnumEstadoNotificacion.PENDIENTE.getId());
            notificacionesUsuario.setFechaIngresoRegistro(new Date());
            notificacionesUsuario.setIntentos(0);
            notificacionesUsuarioRespository.save(notificacionesUsuario);
        }
    }




}
