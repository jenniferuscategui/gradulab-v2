package com.gradulab.administrador.servicios;

import com.gradulab.administrador.dto.PersonaDTO;
import com.gradulab.administrador.dto.ResgistroPersonaDTO;
import com.gradulab.administrador.dto.UsuarioDTO;
import com.gradulab.administrador.entity.Graduado;
import com.gradulab.administrador.entity.InfoAcademica;
import com.gradulab.administrador.entity.Parametros;
import com.gradulab.administrador.entity.Persona;
import com.gradulab.administrador.entity.Usuario;
import com.gradulab.administrador.general.enums.EnumEstadoSolicitud;
import com.gradulab.administrador.general.enums.EnumTipoNotificacion;
import com.gradulab.administrador.general.utils.DateUtils;
import com.gradulab.administrador.repository.GraduadoRepository;
import com.gradulab.administrador.repository.InfoAcademicaRepository;
import com.gradulab.administrador.repository.ParametrosRepository;
import com.gradulab.administrador.repository.PersonaRepository;
import com.gradulab.administrador.repository.UsusarioRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

@Service
public class UsuarioService {

    @Autowired
    private PersonaRepository personaRepository;

    @Autowired
    private GraduadoRepository graduadoRepository;

    @Autowired
    private UsusarioRepository ususarioRepository;

    @Autowired
    private DateUtils dateUtils;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    private NotificacionesService notificacionesService;

    @Autowired
    private ParametrosRepository parametrosRepository;

    @Autowired
    private InfoAcademicaRepository infoAcademicaRepository;

    private static final Logger logger = LoggerFactory.getLogger(UsuarioService.class);


    public Boolean existeUsuarioDePersona(PersonaDTO personaDTO){
        logger.info("Se busca el usuario de la persona solicitada");
        Optional<Persona> persona = personaRepository.findByDocumento(personaDTO.getDocumento());
        return persona.filter(value -> ususarioRepository.findByPersona(value) != null).isPresent();

    }

    public boolean crearUsuarioGraduadoFile(PersonaDTO persona, Boolean hablitado, String password, boolean vieneEmpresa) {
        boolean respuesta = false;
        Usuario usuario = new Usuario();
        if (vieneEmpresa ){
            usuario.setLogin(persona.getCorreoCorporativo());
        }else{
            usuario.setLogin(persona.getDocumento());
        }
        usuario.setLogin(persona.getDocumento());
        if (password == null){
            usuario.setContrasena(passwordEncoder.encode(persona.getDocumento()));
        }else {
            usuario.setContrasena(passwordEncoder.encode(password));
        }

        usuario.setHabilitado(hablitado);
        usuario.setFechaIngreso(dateUtils.obtenerFechaActual());
        usuario.setFechaIngresoReg(dateUtils.obtenerFechaActual());
        usuario = ususarioRepository.save(usuario);
        respuesta = true;

        if (password == null  && persona.getCorreoPersonal() != null){
            notificacionesService.contruirNotificacion(usuario, EnumTipoNotificacion.REGISTRO_AUTOMATICO_USUARIO);
        }
        return respuesta;
    }

    public String recuperarClave(String login){
        Usuario usuario = ususarioRepository.findByLogin(login);
        if (usuario != null){
            usuario.setContrasena(passwordEncoder.encode(usuario.getPersona().getDocumento()));
            notificacionesService.contruirNotificacion(usuario, EnumTipoNotificacion.RECUPERAR_CLAVE);
            return "Se ha mandado a su correo su nueva clave";
        }
        return "el usuario no exite";
    }

    public String notificarCambioCorreo(String nombre, String cedula, String correo){
        Optional<Persona> persona = personaRepository.findByDocumento(cedula);
        if (persona.isPresent()){
            Usuario usuario = ususarioRepository.findByLogin(cedula);
            if (usuario != null){
                Parametros parametro = new Parametros();
                parametro.setNombre(nombre+ " " + cedula);
                parametro.setDescripcion(EnumEstadoSolicitud.SOLICITUD_PENDIENTE.getNombre());
                parametro.setValor(correo);
                parametro.setFechaIngresoReg(new Date());
                parametrosRepository.save(parametro);
                return "Se envio la solictud";
            }
            return  "Usuario no encontrado";
        }
        return "Usuario no encontrado";
    }

    public ResgistroPersonaDTO obtenerInfo(Long id){
        ResgistroPersonaDTO registroPersona = new ResgistroPersonaDTO();
        Optional<Usuario> usuario = ususarioRepository.findById(id);
        if (usuario.isPresent()){
            registroPersona.setPersonaDto(usuario.get().getPersona().getDto());
            Optional<InfoAcademica> infoAcademica = obtenerInfoAcademica(usuario.get().getPersona());
            infoAcademica.ifPresent(academica -> registroPersona.setTitulo(academica.getTituloObt()));
        }

        return registroPersona;
    }

    public ResgistroPersonaDTO actualizarPerfil(ResgistroPersonaDTO resgistroPersonaDTO){
        if (resgistroPersonaDTO.getPersonaDto().getId() != null){
            Optional<Persona> persona = personaRepository.findById(resgistroPersonaDTO.getPersonaDto().getId());
            if (persona.isPresent()){
                persona.get().setNombre(resgistroPersonaDTO.getPersonaDto().getNombre());
                persona.get().setApellidos(resgistroPersonaDTO.getPersonaDto().getApellidos());
                persona.get().setTelefono(resgistroPersonaDTO.getPersonaDto().getTelefono());
                persona.get().setCelular(resgistroPersonaDTO.getPersonaDto().getCelular());
                Optional<InfoAcademica> infoAcademica = obtenerInfoAcademica(persona.get());
                if(infoAcademica.isPresent()){
                    resgistroPersonaDTO.setTitulo(infoAcademica.get().getTituloObt());
                    infoAcademicaRepository.save(infoAcademica.get());
                }

                if (resgistroPersonaDTO.getEmpresaDto() != null){
                    persona.get().setCorreoPersonal(resgistroPersonaDTO.getPersonaDto().getCorreoPersonal());
                } else{
                    persona.get().setCorreoCorporativo(resgistroPersonaDTO.getPersonaDto().getCorreoCorporativo());
                }
                resgistroPersonaDTO.setPersonaDto(personaRepository.save(persona.get()).getDto());

            }

        }
        return resgistroPersonaDTO;
    }

    /**
     * Se oibtiene la informacion academica apartir de una persona
     * @param persona persona a la cual se le onbbtiene la info academica
     * @return Optional<InfoAcademica>
     */
    public Optional<InfoAcademica> obtenerInfoAcademica(Persona persona){
        Optional<Graduado> graduado = graduadoRepository.findByPersona(persona);
        return graduado.flatMap((
                graduado1 -> infoAcademicaRepository.findByGraduado(graduado1)));
    }



    public UsuarioDTO buscarPorId(Long id){
        Optional<Usuario> usu = ususarioRepository.findById(id);
        return usu.map(Usuario::getDto).orElse(null);
    }

    public String cambiarClave(Long idUsuario, String clave){
        Optional<Usuario> usuario = ususarioRepository.findById(idUsuario);
        if (usuario.isPresent()){
            usuario.get().setContrasena(passwordEncoder.encode(clave));
            return "Se Cambio la contraseña correctamente";
        }
        return "Error al momento de la operacion";
    }




}
