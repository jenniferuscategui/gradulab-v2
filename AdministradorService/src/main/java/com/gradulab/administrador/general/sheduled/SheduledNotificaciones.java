package com.gradulab.administrador.general.sheduled;

import com.gradulab.administrador.servicios.AuthUsuarioService;
import com.gradulab.administrador.servicios.NotificacionesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class SheduledNotificaciones {

    @Autowired
    private NotificacionesService notificacionesService;

    private final Logger logger = LoggerFactory.getLogger(SheduledNotificaciones.class);


    @Scheduled(fixedDelay = 300000) // Cada 5 min
    public void procesarNotificacionesPendientes(){
        logger.info("### INICIA EL PROCESAMIENTO DE NOTIFICACIONES ####");
        notificacionesService.buscarNotificacionesPendientes();
        logger.info("### TERMINA EL PROCESAMIENTO DE NOTIFICACIONES ###");
    }
}
