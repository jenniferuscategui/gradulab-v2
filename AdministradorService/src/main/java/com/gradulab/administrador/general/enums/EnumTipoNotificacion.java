package com.gradulab.administrador.general.enums;

public enum EnumTipoNotificacion {
    ACEPTA_SOLICITUD_REGISTRO( 1L, "Notificacion Registro Usuario"),
    ACEPTA_SOLICITUD_EMPRESA( 2L, "Notificacion Registro Empresa"),
    REGISTRO_AUTOMATICO_USUARIO( 3L, "Notificacion de registro automatico"),
    RECUPERAR_CLAVE( 4L, "Notificacion de clave nueva")
    ;

    private Long id;
    private String nombre;

    EnumTipoNotificacion(Long id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public Long getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }
}
