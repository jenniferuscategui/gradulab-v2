package com.gradulab.administrador.general.enums;

public enum EnumEstadoSolicitud {

    SOLICITUD_PENDIENTE( 1L, "Solicitud_Pendiente"),
    SOLICITUD_ACCEPTADA( 2L, "Solicitud_Acceptada"),
    ;

    private Long id;
    private String nombre;

    EnumEstadoSolicitud(Long id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public Long getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }
}
