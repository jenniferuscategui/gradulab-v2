package com.gradulab.administrador.general.enums;

public enum EnumTipoDocumento {

    CC( 1, "Cedula Ciudadania"),
    CE( 2, "Cedula Extranjeria");

    private int id;
    private String nombre;

    EnumTipoDocumento(int id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public int getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }
}
