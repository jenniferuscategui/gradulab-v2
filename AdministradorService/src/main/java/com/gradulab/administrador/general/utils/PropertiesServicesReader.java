package com.gradulab.administrador.general.utils;

import java.util.ResourceBundle;

/**
 * Descripción metodo para leer el archivo de propiedades
 **/
 public class PropertiesServicesReader {

    private final ResourceBundle resourceProperties;

    private static PropertiesServicesReader singleton = null;

    /**
     * constructor
     */
    private PropertiesServicesReader() {
        resourceProperties = ResourceBundle.getBundle("service");
    }

    /**
     * Descripción
     *
     * @return PropertiesReader
     */
    public static PropertiesServicesReader getInstance() {

        synchronized (PropertiesServicesReader.class) {
            if (null == singleton) {
                singleton = new PropertiesServicesReader();
            }
        }
        return singleton;
    }

    /**
     * Descripción
     *
     * @param key
     * @return String
     */
    public String getValue(String key) {

        return resourceProperties.getString(key);
    }

    /**
     * Descripción obtiene el mesnaje de las propiedades
     *
     * @param key
     * @param parameters
     * @return String
     */
    public String getValue(String key, String... parameters) {

        String message = resourceProperties.getString(key);
        for (int i = 0; i < parameters.length; i++) {
            message = message.replaceAll("\\{" + i + "\\}", parameters[i]);
        }
        return message;
    }

}
