package com.gradulab.administrador.general.enums;

public enum EnumTipoNotificaciones {

    ACEPTAR_SOLICITUD_REGISTRO( 1, "Acetar solicitud"),
    RECHAZAR_SOLICITUD_REGISTRO( 2, "Reachazar solicituid");

    private int id;
    private String nombre;

    EnumTipoNotificaciones(int id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public int getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }
}
