package com.gradulab.administrador.general.utils;

import java.util.HashMap;
import java.util.Set;

public class StringUtils {

    /**
     * Indica si el valr esta núlo o vacio
     *
     * @param value el valor que quiero comparar
     * @return un booleano falso si no esta vacio o nulo y vertadero si lo esta
     */
    public static boolean isEmpty(String value) {
        return value == null || "".equals(value);
    }

    /**
     * @param mensaje el mensaje que recive que contiene la variable a modificar
     * @param params  el param,etro a ingresar al mensaje
     * @return un cadena con el mensaje a arreglado
     */
    public static String parseParams(String mensaje, HashMap<String, String> params) {
        try {
            if (params != null && params.size() > 0) {
                Set<String> keys = params.keySet();
                for (String key : keys) {
                    mensaje = mensaje.replaceAll("<" + key + ">", params.get(key));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return mensaje;


    }
}
