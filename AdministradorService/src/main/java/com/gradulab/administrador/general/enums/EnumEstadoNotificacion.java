package com.gradulab.administrador.general.enums;

public enum EnumEstadoNotificacion {

    PENDIENTE( 1, "Pendiente de Envio"),
    ENVIADA( 2, "Notificacion Enviada"),
    FALLIDA(3, "Envio Fallido"),
    ;

    private final Integer id;
    private String nombre;

    EnumEstadoNotificacion(Integer id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public Integer getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }
}
