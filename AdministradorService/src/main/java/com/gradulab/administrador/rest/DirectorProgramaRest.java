package com.gradulab.administrador.rest;

import com.gradulab.administrador.dto.GraduadoDTO;
import com.gradulab.administrador.dto.ParametroDTO;
import com.gradulab.administrador.dto.PersonaDTO;
import com.gradulab.administrador.dto.UsuarioDTO;
import com.gradulab.administrador.general.constants.ResponseCodes;
import com.gradulab.administrador.general.utils.ResponseObject;
import com.gradulab.administrador.servicios.DirectorProgramaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Parameter;
import java.util.List;

@RestController
@RequestMapping("/directorPrograma")
public class DirectorProgramaRest {

    @Autowired
    private DirectorProgramaService directorProgramaService;

    @Autowired
    private MessageSource messageSource;

    private static final String exceptionMessage = "Exception.message";

    @GetMapping("/listarGraduados")
    public ResponseObject<List<GraduadoDTO>> listarSolicitudesGraduados() {

        try {
            List<GraduadoDTO> graduadoDTOpage = directorProgramaService.buscarSolicitudesGraduados();
            return new ResponseObject<>(graduadoDTOpage);
        } catch (Exception e) {

            return new ResponseObject<>(
                    ResponseCodes.REQUEST_ERROR_CODE, messageSource.getMessage(
                    exceptionMessage, null, LocaleContextHolder.getLocale()));
        }
    }

    @GetMapping("/listarSoliciGraduados")
    public ResponseObject<List<PersonaDTO>> listarSoGraduados() {

        try {
            List<PersonaDTO> graduadoDTOpage = directorProgramaService.buscarSolicituGraduado();
            return new ResponseObject<>(graduadoDTOpage);
        } catch (Exception e) {

            return new ResponseObject<>(
                    ResponseCodes.REQUEST_ERROR_CODE, messageSource.getMessage(
                    exceptionMessage, null, LocaleContextHolder.getLocale()));
        }

    }

    @PostMapping("/aceptarSolicitudGraduado")
    public ResponseObject<PersonaDTO> aceptarSolicitudGraduado(@RequestParam String documentoPersona,
                                                               @RequestParam Boolean confirmacion) {
        try {
            PersonaDTO personaDTO = directorProgramaService.prosecarSolicitudGraduado(documentoPersona, confirmacion);
            return new ResponseObject<>(personaDTO);
        } catch (Exception e) {
            return new ResponseObject<>(ResponseCodes.REQUEST_ERROR_CODE, messageSource.getMessage(
                    exceptionMessage, null, LocaleContextHolder.getLocale()));
        }
    }

    @PostMapping("/cambiarCorreo")
    public ResponseObject<ParametroDTO> aceptarSolicitudGraduado(@RequestParam ParametroDTO parametroDTO) {
        try {
            ParametroDTO parametroDTO1= directorProgramaService.cambiarCorreo(parametroDTO);
            return new ResponseObject<>(parametroDTO1);
        } catch (Exception e) {
            return new ResponseObject<>(ResponseCodes.REQUEST_ERROR_CODE, messageSource.getMessage(
                    exceptionMessage, null, LocaleContextHolder.getLocale()));
        }
    }

    @GetMapping("/listarCambioCorreo")
    public ResponseObject<List<ParametroDTO>> listarCambioCorreo() {

        try {
            List<ParametroDTO> graduadoDTOpage = directorProgramaService.listarSolicitudCambioCorreo();
            return new ResponseObject<>(graduadoDTOpage);
        } catch (Exception e) {

            return new ResponseObject<>(
                    ResponseCodes.REQUEST_ERROR_CODE, messageSource.getMessage(
                    exceptionMessage, null, LocaleContextHolder.getLocale()));
        }
    }

}
