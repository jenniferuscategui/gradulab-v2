package com.gradulab.administrador.rest;

import com.gradulab.administrador.general.utils.PropertiesServicesReader;
import com.gradulab.administrador.servicios.ExcelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;

@RestController
@RequestMapping("/archivo")
public class ArchivoRest {

    @Autowired
    private ExcelService excelService;

    @PostMapping("/cargarExcel")
    public String importarExcel(@RequestParam ("file")MultipartFile listado, byte tipo) {
        String respuesta = null;
        try {
            respuesta = excelService.importarExcel(listado, tipo);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            return respuesta;
        }
    }

    @GetMapping("obtener/img/{nombreFoto:.+}")
    public ResponseEntity<Resource> verFoto(@PathVariable String nombreFoto){

        Path rutaArchivo = Paths.get(PropertiesServicesReader.getInstance().getValue("url.files.base")).resolve(nombreFoto).toAbsolutePath();

        Resource recurso = null;

        try {
            recurso = new UrlResource(rutaArchivo.toUri());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        if(!recurso.exists() && !recurso.isReadable()) {
            throw new RuntimeException("Error no se pudo cargar la imagen: " + nombreFoto);
        }
        HttpHeaders cabecera = new HttpHeaders();
        cabecera.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + recurso.getFilename() + "\"");

        return new ResponseEntity<Resource>(recurso, cabecera, HttpStatus.OK);
    }
}
