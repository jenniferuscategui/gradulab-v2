package com.gradulab.administrador.rest;

import com.gradulab.administrador.dto.ComentarioDTO;
import com.gradulab.administrador.dto.PersonaDTO;
import com.gradulab.administrador.dto.PublicacionDTO;
import com.gradulab.administrador.dto.ReaccionPublicacionDTO;
import com.gradulab.administrador.entity.Publicacion;
import com.gradulab.administrador.general.constants.ResponseCodes;

import com.gradulab.administrador.general.utils.ResponseObject;
import com.gradulab.administrador.servicios.GraduadoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;


@RestController
@RequestMapping("/publicacion")
public class PublicacionRest {

    private static final Logger logger = LoggerFactory.getLogger(PublicacionRest.class);

    private static final String exceptionMessage = "Exception.message";

    @Autowired
    private GraduadoService graduadoService;

    @Autowired
    private MessageSource messageSource;


    @GetMapping(value = "/listar", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<Page<PublicacionDTO>> obtenerPublicaciones(
            @RequestParam Integer page,
            @RequestParam Integer totalElements,
            @RequestParam(value = "fechaPu", required = false) Date fecha){
        logger.info("#### SE LISTAN LAS PUBLICACIONES ###");
        try {
            Page<PublicacionDTO> publicacionDTOPage = graduadoService.buscarPublicacion(fecha, page);
            return new ResponseObject<>(publicacionDTOPage);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new ResponseObject<>(
                    ResponseCodes.REQUEST_ERROR_CODE, messageSource.getMessage(
                    exceptionMessage,null, LocaleContextHolder.getLocale()));
        }
    }

    @PostMapping("/crear")
    public ResponseObject<PublicacionDTO> crearPublicacion(@RequestBody PublicacionDTO publicacionDTO){
        try{
            logger.info("#### SE CREAN LAS PUBLICACIONES ###");
            PublicacionDTO publicacion = graduadoService.crearPublicacion(publicacionDTO);
            return new ResponseObject<>(publicacion);
        }catch (Exception e){
            return new ResponseObject<>(ResponseCodes.REQUEST_ERROR_CODE, messageSource.getMessage(
                    exceptionMessage, null, LocaleContextHolder.getLocale()));
        }
    }

    @PostMapping("/subir/imaben")
    public ResponseObject<PublicacionDTO> upload(@RequestParam("archivo")MultipartFile archivo, @RequestParam("id") String id) {
        try{
            logger.info("##### Se sube la imagen al servbidor #######");
            PublicacionDTO publicacion = graduadoService.adjuntarFoto(archivo, Long.parseLong(id));
            return new ResponseObject<>(publicacion);
        }catch (Exception e){
            return new ResponseObject<>(ResponseCodes.REQUEST_ERROR_CODE, messageSource.getMessage(
                    exceptionMessage, null, LocaleContextHolder.getLocale()));
        }
    }

    @PostMapping("/megusta")
    public ResponseObject<String> meGustaPublicacion(@RequestBody PublicacionDTO publicacionDTO){
        try{
            logger.info("#### SE REACCIONA ME GUSTA LA PUBLICACION ###");
            ReaccionPublicacionDTO reaccionPu = new ReaccionPublicacionDTO();
            reaccionPu.setPublicacion(publicacionDTO);
            reaccionPu.setReaccion(1L);
             graduadoService.reaccionarPublicacion(reaccionPu);
            return new ResponseObject<>("se guardo reaccion");
        }catch (Exception e){
            return new ResponseObject<>(ResponseCodes.REQUEST_ERROR_CODE, messageSource.getMessage(
                    exceptionMessage, null, LocaleContextHolder.getLocale()));
        }
    }

    @PostMapping("/meencanta")
    public ResponseObject<String> meEncantaPublicacion(@RequestBody PublicacionDTO publicacionDTO){
        try{
            logger.info("#### SE REACCIONA ME ENCANTA LA PUBLICACION ###");
            ReaccionPublicacionDTO reaccionPu = new ReaccionPublicacionDTO();
            reaccionPu.setPublicacion(publicacionDTO);
            reaccionPu.setReaccion(2L);
            graduadoService.reaccionarPublicacion(reaccionPu);
            return new ResponseObject<>("se reaccion me encanta");
        }catch (Exception e){
            return new ResponseObject<>(ResponseCodes.REQUEST_ERROR_CODE, messageSource.getMessage(
                    exceptionMessage, null, LocaleContextHolder.getLocale()));
        }
    }

    @PostMapping("/comentar")
    public ResponseObject<String> comentar(@RequestBody ComentarioDTO comentarioDTO){
        try{
            logger.info("#### SE COMENTA LA PUBLICACION ###");
            graduadoService.comentarPublicacion(comentarioDTO);
            return new ResponseObject<>("Se agrego comentario");
        }catch (Exception e){
            return new ResponseObject<>(ResponseCodes.REQUEST_ERROR_CODE, messageSource.getMessage(
                    exceptionMessage, null, LocaleContextHolder.getLocale()));
        }
    }



}
