package com.gradulab.administrador.rest;

import com.gradulab.administrador.dto.DatosRegistroDTO;
import com.gradulab.administrador.dto.OfertaLaboralDTO;
import com.gradulab.administrador.entity.OfertaLaboral;
import com.gradulab.administrador.general.constants.ResponseCodes;
import com.gradulab.administrador.general.exception.GradulabExceptionHandler;
import com.gradulab.administrador.general.utils.ResponseObject;
import com.gradulab.administrador.servicios.PersonaService;
import com.gradulab.administrador.servicios.ReporteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

@RestController
@RequestMapping("/administrador/reporte")
public class ReporteRest {

    @Autowired
    private ReporteService reporteService;

    @Autowired
    private MessageSource messageSource;

    @PostMapping(value = "/generar")
    public void crearReporte(byte tipoReporte, Date fechaInicio, Date fechaFin) throws IOException {
      //  reporteService.crearReporte(tipoReporte, fechaInicio, fechaFin);
        if (tipoReporte == 6) {
            crearReporteExcel();
        } else {
            crearReportePDF(tipoReporte, fechaInicio, fechaFin);
        }
    }

    @GetMapping(value = "/descargar/listado_contactos.xlsx")
    public ResponseEntity crearReporteExcel() throws IOException {
        ByteArrayInputStream in = reporteService.crearReporteExcel();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "attachment; filename=listado_contactos.xlsx");

        return ResponseEntity
                .ok()
                .headers(headers)
                .body(new InputStreamResource(in));
    }

    public ResponseEntity crearReportePDF(byte  tipoReporte, Date fechaInicio, Date fechaFin) {
        ByteArrayInputStream in = null;

        switch (tipoReporte) {
            case 1: //Caracterización de los Graduados
                in = reporteService.crearReporteCaracterizacionSexo(fechaInicio, fechaFin);
                break;
            case 2: //Caracterización de los Graduados
                in = reporteService.crearReporteCaracterizacionEdad(fechaInicio, fechaFin);
                break;
            case 3: //Caracterización de los Graduados
                in = reporteService.crearReporteCaracterizacionModalidad(fechaInicio, fechaFin);
                break;
            case 4: //Posicionamiento Laboral
                in = reporteService.crearReporteCaracterizacionLaboral();

            case 5: //Cantidad de Graduados
                in = reporteService.crearReporteCantidad(fechaInicio, fechaFin);
                break;
        }
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "attachment; filename=listado_contactos.xlsx");
        return ResponseEntity
                .ok()
                .headers(headers)
                .body(new InputStreamResource(in));
    }
}
