package com.gradulab.administrador.rest;

import com.gradulab.administrador.dto.EncuestaDTO;
import com.gradulab.administrador.dto.OpcionesRespuestaDTO;
import com.gradulab.administrador.dto.PreguntaDTO;
import com.gradulab.administrador.servicios.EncuestaService;
import com.gradulab.administrador.servicios.ReporteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.PostMapping;

public class EncuestaRest {

    @Autowired
    private EncuestaService encuestaService;

    @Autowired
    private MessageSource messageSource;

    @PostMapping(value = "/crearEncuesta")
    public void crearEncuesta (EncuestaDTO encuestaDTO) {
        encuestaService.crearEncuesta(encuestaDTO);
    }

    @PostMapping(value = "/agregarPregunta")
    public void añadirPregunta (PreguntaDTO preguntaDTO, EncuestaDTO encuestaDTO) {
        encuestaService.agregarPregunta(preguntaDTO, encuestaDTO);
    }

    @PostMapping(value = "/añadirOpciones")
    public void añadirOpciones (OpcionesRespuestaDTO opcionesRespuestaDTO, PreguntaDTO preguntaDTO) {
        encuestaService.agregarOpciones(opcionesRespuestaDTO, preguntaDTO);

    }

    @PostMapping(value = "/mostrarResultados")
    public void mostrarResultados (EncuestaDTO encuestaDTO) {
        encuestaService.mostrarResultados(encuestaDTO);
    }

}
