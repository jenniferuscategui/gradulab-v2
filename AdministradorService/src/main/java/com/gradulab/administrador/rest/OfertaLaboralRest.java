package com.gradulab.administrador.rest;

import com.gradulab.administrador.dto.EmpresaDTO;
import com.gradulab.administrador.dto.OfertaLaboralDTO;
import com.gradulab.administrador.entity.OfertaLaboral;
import com.gradulab.administrador.general.constants.ResponseCodes;
import com.gradulab.administrador.general.exception.GradulabExceptionHandler;
import com.gradulab.administrador.general.utils.ResponseObject;
import com.gradulab.administrador.servicios.EmpresaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/oferta")
public class OfertaLaboralRest {

    @Autowired
    private EmpresaService empresaService;

    @Autowired
    private MessageSource messageSource;

    private static final String exceptionMessage = "Exception.message";

    @PostMapping(value = "/crearOfertaLaboral", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<OfertaLaboral> crearOfertaLaboral(@RequestBody OfertaLaboralDTO ofertaLaboralDTO) {

        try{
            OfertaLaboral ofertaLaboral = empresaService.crearOfertaLaboral(ofertaLaboralDTO);
            return new ResponseObject<>(ofertaLaboral);
        }catch (GradulabExceptionHandler ge){
            return new ResponseObject<>(ResponseCodes.REQUEST_ERROR_CODE, messageSource.getMessage(
                    ge.getMessage(), null, LocaleContextHolder.getLocale()));
        }catch (Exception e){
            return new ResponseObject<>(ResponseCodes.REQUEST_ERROR_CODE, messageSource.getMessage(
                    exceptionMessage, null, LocaleContextHolder.getLocale()));
        }
    }

    @GetMapping(value = "/listarOfertasLaboralesEmpresa", produces = MediaType.APPLICATION_JSON_VALUE)
    public  ResponseObject<List<OfertaLaboral>> listarOfertasLaboralesEmpresa (@RequestBody EmpresaDTO empresaDTO){

        try{
            List<OfertaLaboral> ofertaLaboralList= empresaService.listarOfertasEmpresa(empresaDTO);
            return new ResponseObject<>(ofertaLaboralList);
        }catch (GradulabExceptionHandler ge){
            return new ResponseObject<>(ResponseCodes.EXCEPTION_CODE, messageSource.getMessage(
                    ge.getMessage(), null, LocaleContextHolder.getLocale()));
        }catch (Exception e){
            return new ResponseObject<>(ResponseCodes.REQUEST_ERROR_CODE, messageSource.getMessage(
                    exceptionMessage, null, LocaleContextHolder.getLocale()));
        }
    }
}
