package com.gradulab.administrador.rest;

import com.gradulab.administrador.dto.PersonaDTO;
import com.gradulab.administrador.entity.Persona;
import com.gradulab.administrador.general.constants.ResponseCodes;
import com.gradulab.administrador.general.utils.ResponseObject;
import com.gradulab.administrador.servicios.NotificacionesService;
import com.gradulab.administrador.servicios.PersonaService;
import com.gradulab.administrador.servicios.UsuarioService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/administrador/persona")
public class PersonaRest {

    @Autowired
    private PersonaService personaService;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private UsuarioService usuarioService;


    private static final Logger logger = LoggerFactory.getLogger(PersonaRest.class);
    private static final String exceptionMessage = "Exception.message";


    @GetMapping(value = "/list", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseObject<Page<PersonaDTO>> obtenerPersonas(
            @RequestParam(value = "nombre", required = false) String nombre,
            @RequestParam(value = "documento", required = false)String documento){

        try {
            Page<PersonaDTO> personaDTOPage = personaService.buscarPersonas(nombre, documento);
            return new ResponseObject<>(personaDTOPage);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new ResponseObject<>(
                    ResponseCodes.REQUEST_ERROR_CODE, messageSource.getMessage(
                            exceptionMessage,null, LocaleContextHolder.getLocale()));
        }
    }

    @GetMapping(value="/per",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseObject<PersonaDTO> buscarPersona(){
        String  s= "12345678";
        try{
             return new ResponseObject<>(personaService.buscarPorDocuemnto(s));
        } catch (Exception e){
            logger.error(e.getMessage(), e);
            return new ResponseObject<>(
                    ResponseCodes.REQUEST_ERROR_CODE, messageSource.getMessage(
                    exceptionMessage,null, LocaleContextHolder.getLocale()));
        }
    }

    @PostMapping(value = "/recuperarclave", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<String> recuperarClave(@RequestBody String cedula){
        try{
            return new ResponseObject<>(usuarioService.recuperarClave(cedula));
        } catch (Exception e){
            logger.error(e.getMessage(), e);
            return new ResponseObject<>(
                    ResponseCodes.REQUEST_ERROR_CODE, messageSource.getMessage(
                    exceptionMessage,null, LocaleContextHolder.getLocale()));
        }
    }

    @PostMapping(value = "/recuperarusuario", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<String> recuperarUsuario(@RequestBody PersonaDTO personaDTO){
        try{
            return new ResponseObject<>(usuarioService.notificarCambioCorreo(personaDTO.getNombre(),personaDTO.getDocumento(), personaDTO.getCorreoPersonal()));
        } catch (Exception e){
            logger.error(e.getMessage(), e);
            return new ResponseObject<>(
                    ResponseCodes.REQUEST_ERROR_CODE, messageSource.getMessage(
                    exceptionMessage,null, LocaleContextHolder.getLocale()));
        }
    }
}
