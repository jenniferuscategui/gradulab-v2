package com.gradulab.administrador.rest;

import com.gradulab.administrador.dto.ContactoEmpresaDTO;
import com.gradulab.administrador.dto.ResgistroPersonaDTO;
import com.gradulab.administrador.general.constants.ResponseCodes;
import com.gradulab.administrador.general.utils.ResponseObject;
import com.gradulab.administrador.servicios.DirectorProgramaService;
import com.gradulab.administrador.servicios.EmpresaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/empresa")
public class EmpresaRest {

    @Autowired
    DirectorProgramaService directorProgramaService;

    @Autowired
    private EmpresaService empresaService;

    @Autowired
    private MessageSource messageSource;


    private static final Logger logger = LoggerFactory.getLogger(EmpresaRest.class);
    private static final String EXCEPTION_MESSAGE = "Exception.message";

    @GetMapping(value = "/list", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<List<ContactoEmpresaDTO>> obtenerSolicitudes() {

        try {
            List<ContactoEmpresaDTO> personaDTOPage = directorProgramaService.buscarSolicitudesEmpresa();
            return new ResponseObject<>(personaDTOPage);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new ResponseObject<>(
                    ResponseCodes.REQUEST_ERROR_CODE, messageSource.getMessage(
                    EXCEPTION_MESSAGE, null, LocaleContextHolder.getLocale()));
        }

    }

    @PostMapping(value = "/registrarEmpresa", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<ContactoEmpresaDTO> registrarEmpresa (@RequestBody ResgistroPersonaDTO resgistroPersonaDto){
        try{
            ContactoEmpresaDTO contactoEmpresaDto = empresaService.registrarEmpresa(resgistroPersonaDto);
            return new ResponseObject<>(contactoEmpresaDto);
        } catch (Exception e){
            logger.error(e.getMessage());
            return new ResponseObject<>( ResponseCodes.REQUEST_ERROR_CODE,
                    messageSource.getMessage(EXCEPTION_MESSAGE, null, LocaleContextHolder.getLocale()));
        }
    }

}
