package com.gradulab.administrador.rest;

import com.gradulab.administrador.dto.ResgistroPersonaDTO;
import com.gradulab.administrador.dto.UsuarioDTO;
import com.gradulab.administrador.general.constants.ResponseCodes;
import com.gradulab.administrador.general.utils.ResponseObject;
import com.gradulab.administrador.servicios.UsuarioService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/usuario")
public class UsuarioRest {

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private MessageSource messageSource;

    private static final Logger logger = LoggerFactory.getLogger(UsuarioRest.class);

    private static final String exceptionMessage = "Exception.message";

    @GetMapping(value="/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<UsuarioDTO>  getUsuarioPorId(@PathVariable Long id){

        return new ResponseObject<>(usuarioService.buscarPorId(id));
    }

    @PostMapping("/editarPerfil")
    public ResponseObject<ResgistroPersonaDTO> crearPublicacion(@RequestBody ResgistroPersonaDTO resgistroPersonaDTO){
        try{
            logger.info("#### Se edita el perfil de la persona ###");

           return new ResponseObject<>(usuarioService.actualizarPerfil(resgistroPersonaDTO));
        }catch (Exception e){
            return new ResponseObject<>(ResponseCodes.REQUEST_ERROR_CODE, messageSource.getMessage(
                    exceptionMessage, null, LocaleContextHolder.getLocale()));
        }

    }

    @GetMapping("/obtenerInfo/{id}")
    public ResponseObject<ResgistroPersonaDTO> getInfo( @PathVariable Long id){
        try{
            logger.info("#### Se la informacion de la persona ###");

            return new ResponseObject<>(usuarioService.obtenerInfo(id));
        }catch (Exception e){
            return new ResponseObject<>(ResponseCodes.REQUEST_ERROR_CODE, messageSource.getMessage(
                    exceptionMessage, null, LocaleContextHolder.getLocale()));
        }
    }

    @PostMapping(value = "/cambiarclave", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<String> recuperarClave(@RequestBody Long idUsuario,
                                                 @RequestBody String passwordConfig){
        try{
            return new ResponseObject<>(usuarioService.cambiarClave(idUsuario, passwordConfig));
        } catch (Exception e){
            logger.error(e.getMessage(), e);
            return new ResponseObject<>(
                    ResponseCodes.REQUEST_ERROR_CODE, messageSource.getMessage(
                    exceptionMessage,null, LocaleContextHolder.getLocale()));
        }
    }




}
