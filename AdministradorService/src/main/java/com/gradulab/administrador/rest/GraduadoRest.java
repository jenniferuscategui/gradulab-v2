
package com.gradulab.administrador.rest;

import com.gradulab.administrador.dto.GraduadoDTO;
import com.gradulab.administrador.dto.InfoAcademicaDTO;
import com.gradulab.administrador.dto.InfoLaboralDTO;
import com.gradulab.administrador.dto.PersonaDTO;
import com.gradulab.administrador.dto.ResgistroPersonaDTO;
import com.gradulab.administrador.general.constants.ResponseCodes;
import com.gradulab.administrador.general.exception.GradulabExceptionHandler;
import com.gradulab.administrador.general.utils.ResponseObject;
import com.gradulab.administrador.servicios.GraduadoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/gradudado")
public class GraduadoRest {

    @Autowired
    private GraduadoService graduadoService;

    @Autowired
    private MessageSource messageSource;

    private static final String EXCEPTION_MESSAGE = "Exception.message";

    private static final Logger logger = LoggerFactory.getLogger(GraduadoRest.class);

    @GetMapping(value = "/list", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<List<GraduadoDTO>> obtenerGraduados(
            @RequestParam(value = "nombre", required = false) String nombre,
            @RequestParam(value = "documento", required = false)String documento,
            @RequestParam(value = "codigo", required = false)String codigo){

        try {
            List<GraduadoDTO> personaDTOPage = graduadoService.buscarGraduados(documento,codigo );
            return new ResponseObject<>(personaDTOPage);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new ResponseObject<>(
                    ResponseCodes.REQUEST_ERROR_CODE, messageSource.getMessage(
                    EXCEPTION_MESSAGE,null, LocaleContextHolder.getLocale()));
        }
    }



    @PostMapping(value = "/registrarGraduado", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<GraduadoDTO> registrarGraduado (@RequestBody ResgistroPersonaDTO resgistroPersonaDto){
        try{
            return new ResponseObject<>(graduadoService.registrarGraduado(resgistroPersonaDto));
        } catch (GradulabExceptionHandler gradulabExceptionHandler){
            logger.error(gradulabExceptionHandler.getMessage());
            return new ResponseObject<>( ResponseCodes.EXCEPTION_CODE,
                    messageSource.getMessage(gradulabExceptionHandler.getMessage(), null, LocaleContextHolder.getLocale()));
        }
        catch (Exception e){
            logger.error(e.getMessage());
            return new ResponseObject<>( ResponseCodes.REQUEST_ERROR_CODE,
                    messageSource.getMessage(EXCEPTION_MESSAGE, null, LocaleContextHolder.getLocale()));
        }
    }

    @PostMapping(value = "/infoLaboral", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<InfoLaboralDTO> ingresarInfoLaboral(@RequestBody InfoLaboralDTO infoLaboralDTO){
        try{
            return new ResponseObject<>(graduadoService.ingresarInfoLaborar(infoLaboralDTO));
        } catch ( Exception exception) {
            logger.error(exception.getMessage());
            return new ResponseObject<>( ResponseCodes.REQUEST_ERROR_CODE,
                    messageSource.getMessage(EXCEPTION_MESSAGE, null, LocaleContextHolder.getLocale()));
        }
    }

    @PostMapping(value = "/infoAcademica", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<InfoAcademicaDTO> ingresarInfoAcademica(@RequestBody InfoAcademicaDTO infoAcademicaDTO){
        try{
            return new ResponseObject<>(graduadoService.ingresarInfoAcademica(infoAcademicaDTO));
        } catch ( Exception exception) {
            logger.error(exception.getMessage());
            return new ResponseObject<>( ResponseCodes.REQUEST_ERROR_CODE,
                    messageSource.getMessage(EXCEPTION_MESSAGE, null, LocaleContextHolder.getLocale()));
        }
    }

}
