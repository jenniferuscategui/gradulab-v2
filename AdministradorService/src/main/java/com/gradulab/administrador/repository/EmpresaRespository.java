package com.gradulab.administrador.repository;

import com.gradulab.administrador.entity.Empresa;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EmpresaRespository extends PagingAndSortingRepository<Empresa, Long>, JpaSpecificationExecutor<Empresa> {

  Optional <Empresa> findByEmail(String email);
  Optional <Empresa> findByNit(String nit);

}
