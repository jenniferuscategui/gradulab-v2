package com.gradulab.administrador.repository;


import com.gradulab.administrador.entity.Publicacion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PublicacionRepository extends PagingAndSortingRepository<Publicacion, Long>, JpaSpecificationExecutor<Publicacion> {
}
