package com.gradulab.administrador.repository;

import com.gradulab.administrador.entity.Empresa;
import com.gradulab.administrador.entity.OfertaLaboral;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface OfertaLaboralRespository extends CrudRepository<OfertaLaboral, Long> {
    List<OfertaLaboral> findAllByEmpresa(Empresa empresaOptional);
}
