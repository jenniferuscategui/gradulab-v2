package com.gradulab.administrador.repository;

import com.gradulab.administrador.entity.Persona;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PersonaRepository extends PagingAndSortingRepository<Persona, Long>, JpaSpecificationExecutor<Persona> {

    Optional<Persona> findByDocumento(String documento);

}


