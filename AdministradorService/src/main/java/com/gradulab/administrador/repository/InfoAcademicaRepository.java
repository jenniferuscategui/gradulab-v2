package com.gradulab.administrador.repository;

import com.gradulab.administrador.entity.Graduado;
import com.gradulab.administrador.entity.InfoAcademica;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface InfoAcademicaRepository  extends CrudRepository<InfoAcademica, Long> {
    Optional<InfoAcademica> findByGraduado(Graduado graduado);
}
