package com.gradulab.administrador.repository;


import com.gradulab.administrador.entity.Comentario;
import com.gradulab.administrador.entity.Publicacion;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface ComentarioRepository extends CrudRepository<Comentario, Long> {

    Long countComentarioByPublicacion(Publicacion publicacion);
}
