package com.gradulab.administrador.repository;


import com.gradulab.administrador.entity.Publicacion;
import com.gradulab.administrador.entity.Reaccion;
import com.gradulab.administrador.entity.ReaccionPublicacion;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public interface ReaccionPublicacionRepository extends CrudRepository<ReaccionPublicacion, Long> {
    Optional<ReaccionPublicacion> findByPublicacionAndReaccion(Publicacion publicacion,Reaccion reaccion);

}
