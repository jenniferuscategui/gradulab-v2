package com.gradulab.administrador.repository;

import com.gradulab.administrador.entity.Notificacion;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NoitificacionRepository extends CrudRepository<Notificacion, Long> {


}
