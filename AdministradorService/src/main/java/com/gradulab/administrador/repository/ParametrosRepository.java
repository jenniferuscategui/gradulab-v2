package com.gradulab.administrador.repository;

import com.gradulab.administrador.entity.Parametros;
import com.gradulab.administrador.general.enums.EnumEstadoSolicitud;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ParametrosRepository extends CrudRepository<Parametros, Long> {

    List<Parametros> findByDescripcion(EnumEstadoSolicitud pendiente);

}
