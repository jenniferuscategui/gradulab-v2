package com.gradulab.administrador.repository;

import com.gradulab.administrador.entity.Graduado;
import com.gradulab.administrador.entity.InfoLaboral;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface InfoLaboralRepository  extends CrudRepository<InfoLaboral,Long> {

    Optional<InfoLaboral> findByGraduado(Graduado graduado);
}
