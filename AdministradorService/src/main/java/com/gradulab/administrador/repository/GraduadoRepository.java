package com.gradulab.administrador.repository;

import com.gradulab.administrador.dto.GraduadoDTO;
import com.gradulab.administrador.entity.Graduado;
import com.gradulab.administrador.entity.Persona;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;


import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface GraduadoRepository extends  CrudRepository<Graduado, Long>{

    @Query("SELECT g FROM Graduado g JOIN g.persona p where g.persona not in ( select u.persona from Usuario u where u.persona = p)")
    List<Graduado> findAll();
    Optional<Graduado> findByPersona(Persona persona);

    List<Graduado> findByFechaGradoBetween(Date fecha1, Date fecha2);
    Optional<Graduado> findByCodigo(String codigo);
}
