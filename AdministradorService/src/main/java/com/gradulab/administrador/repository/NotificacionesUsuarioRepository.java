package com.gradulab.administrador.repository;

import com.gradulab.administrador.entity.NotificacionesUsuario;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NotificacionesUsuarioRepository extends CrudRepository<NotificacionesUsuario, Long> {
    List<NotificacionesUsuario> findFirst10ByEstadoOrderByFechaIngresoRegistroAsc(Integer estado);
}
