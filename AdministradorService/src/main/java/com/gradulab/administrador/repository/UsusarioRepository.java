package com.gradulab.administrador.repository;

import com.gradulab.administrador.entity.Persona;
import com.gradulab.administrador.entity.Usuario;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsusarioRepository  extends CrudRepository<Usuario,Long> {
    Usuario findByPersona(Persona persona);

    Usuario findByLogin(String username);

    List<Usuario> findByHabilitado(Boolean inactivo);
}
