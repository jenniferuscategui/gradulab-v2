package com.gradulab.encuestas.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/encuestas")
public class Hola {

    @GetMapping
    public String hola(){
        return "hola";
    }
}
