package com.gradulab.encuestas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class EncuestasServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(EncuestasServiceApplication.class, args);
	}

}
