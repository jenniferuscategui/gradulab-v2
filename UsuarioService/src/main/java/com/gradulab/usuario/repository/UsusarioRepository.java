package com.gradulab.usuario.repository;

import com.gradulab.usuario.entity.Persona;
import com.gradulab.usuario.entity.Usuario;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsusarioRepository extends CrudRepository<Usuario,Long> {
    Usuario findByPersona(Persona persona);

    public Usuario findByLogin(String login);
}
