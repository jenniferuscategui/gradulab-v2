package com.gradulab.usuario.entity;

import com.gradulab.usuario.dto.UsuarioDTO;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@Entity
@Table(name = "GDL_Usuario")
public class Usuario extends Model<UsuarioDTO> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "usua_id")
    private Long id;

    @Column(name = "usua_login")
    private String login;

    @Column(name = "usua_contraseña")
    private String contrasena;

    @Column(name = "usua_habilitado")
    private String habilitado;

    @Column(name = "usua_fecha_ingreso")
    private String fechaIngreso;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "usua_pers_id")
    private Persona persona;

    @Column(name = "usua_fecha_ingreso_reg")
    @Temporal(TemporalType.DATE)
    private Date fechaIngresoReg;

    @Column(name = "usua_fecha_modifica_reg")
    private Date fechaModificaReg;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getHabilitado() {
        return habilitado;
    }

    public void setHabilitado(String habilitado) {
        this.habilitado = habilitado;
    }

    public String getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(String fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public Date getFechaIngresoReg() {
        return fechaIngresoReg;
    }

    public void setFechaIngresoReg(Date fechaIngresoReg) {
        this.fechaIngresoReg = fechaIngresoReg;
    }

    public Date getFechaModificaReg() {
        return fechaModificaReg;
    }

    public void setFechaModificaReg(Date fechaModificaReg) {
        this.fechaModificaReg = fechaModificaReg;
    }

    @Override
    public UsuarioDTO getDto(){
        UsuarioDTO usuarioDto = super.getDto();
        if (persona != null){
            usuarioDto.setPersonaDto(this.persona.getDto());
        }
        return usuarioDto;
    }

    @Override
    public Class<UsuarioDTO> getDtoClass(){
        return UsuarioDTO.class;
    }

}
